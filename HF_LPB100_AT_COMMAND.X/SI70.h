#ifndef SI70_H
#define	SI70_H

#include "main.h"

#define SI70_DEVICE_ADD 0x80
#define SI70_CONVERT_HUMID 0xF5
#define SI70_CONVERT_TEMP 0xF3 //Only need to be use with temp only sensor (humid convert get temp automaticly)
#define SI70_TEMP_REG 0xE0

uint16_t getRawHumid();
float getHumid(uint16_t rawHumid);

uint16_t getRawTemp(uint8_t convert);
float getTemp(uint16_t rawTemp);
uint16_t conToRawTemp(float temp);

#endif

