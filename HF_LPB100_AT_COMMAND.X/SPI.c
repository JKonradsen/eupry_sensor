#include "SPI.h"

#ifdef SPIFLASH
void SPIinit(){
#ifndef SOFTSPI
    TRISCbits.RC3 = 0; //SCK TRIS = 0
    TRISCbits.RC4 = 1; //SDI TRIS = 1
    TRISCbits.RC5 = 0; //SDO TRIS = 0
    SSP1CON1 = 0b00100000;
    SSP1CON2 = 0b00000000;
    SSP1STAT = 0b01000000;
#endif
#ifdef SOFTSPI
    MOSITris = 0;
    MISOTris = 1;
    CLKTris = 0;
    CLK = 0;
    MOSI = 0;
#endif
}
uint8_t SPISendByte(uint8_t input){
#ifndef SOFTSPI
    SSP1BUF = input;
    while(SSP1STATbits.BF);
    return SSP1BUF;
#endif
#ifdef SOFTSPI
    uint8_t data = 0;
    for(char y = 0; y < 8; y++){
        if((input & 0x80)!=0){MOSI = 1;}else{MOSI = 0;}
        NOP();CLK = 1;NOP();
        data <<= 1;
        if(MISO){data |= 1;}
        NOP();CLK = 0;NOP();
        input <<= 1;
    }
    __delay_us(1);
    return data;
#endif
}
void SPISendByteDualMode(uint8_t input){
#ifdef SOFTSPI
    MISOTris = 0;
    uint8_t evenData = input & 0b01010101;
    uint8_t oddData = input & 0b10101010;
    for(char y = 0; y < 4; y++){
        if((evenData & 0x40)!=0){MOSI = 1;}
        else{MOSI = 0;}
        if((oddData & 0x80)!=0){MISO = 1;}
        else{MISO = 0;}
        NOP();CLK = 1;NOP();
        evenData <<= 2;
        oddData <<= 2;
        NOP();CLK = 0;NOP();
    }
    __delay_us(1);

    MISOTris = 1;
#endif
}
#endif


