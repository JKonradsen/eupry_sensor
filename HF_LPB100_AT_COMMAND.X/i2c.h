#ifndef I2C_H
#define	I2C_H

#include "main.h"

void I2Cinit(uint8_t bus);

void _I2CSendByte(uint8_t input, uint8_t bus);
uint8_t _I2CgetByte(uint8_t bus);
void _I2CSendStop(uint8_t bus);
void _I2CSendStart(uint8_t bus);
void _I2CSendRepeatedStart(uint8_t bus);
void _I2CSendACK(uint8_t NotACK, uint8_t bus);
uint8_t _I2CGetACK(uint8_t bus);

uint8_t I2CWriteByte(uint8_t device, uint8_t addres, uint8_t data, uint8_t bus);
uint8_t I2CReadByte(uint8_t device, uint8_t addres, uint8_t delay, uint8_t bus);
uint16_t I2CReadWord(uint8_t device, uint8_t addres, uint8_t delay, uint8_t bus);

#endif