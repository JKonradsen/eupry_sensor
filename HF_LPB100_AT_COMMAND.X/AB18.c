#include "AB18.h"

void AB18SetUnixTime(uint32_t unixTime){
    getTimeFields(unixTime);
    I2CWriteByte(AB18_ADD, 0x10, 0x17, 2);
    I2CWriteByte(AB18_ADD, 0x01, ((((globalTime.sec/10)<<4)&0xf0)|((globalTime.sec%10)&0x0f)), 2);
    I2CWriteByte(AB18_ADD, 0x02, ((((globalTime.min/10)<<4)&0xf0)|((globalTime.min%10)&0x0f)), 2);
    I2CWriteByte(AB18_ADD, 0x03, ((((globalTime.hour/10)<<4)&0xf0)|((globalTime.hour%10)&0x0f)), 2);
    I2CWriteByte(AB18_ADD, 0x04, ((((globalTime.day/10)<<4)&0xf0)|((globalTime.day%10)&0x0f)), 2);
    I2CWriteByte(AB18_ADD, 0x05, ((((globalTime.mon/10)<<4)&0xf0)|((globalTime.mon%10)&0x0f)), 2);
    globalTime.year -= 2000;
    I2CWriteByte(AB18_ADD, 0x06, ((((globalTime.year/10)<<4)&0xf0)|((globalTime.year%10)&0x0f)), 2);
//    sprintf(Debugbuf,"Saved to RTC:%d:%d:%d %lu/%lu/%lu\n", globalTime.hour,globalTime.min,globalTime.sec,globalTime.day,globalTime.mon,globalTime.year);
//    U2SendArray(Debugbuf);
}
unsigned long AB18GetUnixTime(){
    uint8_t a = I2CReadByte(AB18_ADD, 0x01, 0, 2);
    uint8_t b = I2CReadByte(AB18_ADD, 0x02, 0, 2);
    uint8_t c = I2CReadByte(AB18_ADD, 0x03, 0, 2);
    uint8_t d = I2CReadByte(AB18_ADD, 0x04, 0, 2);
    uint8_t e = I2CReadByte(AB18_ADD, 0x05, 0, 2);
    uint8_t f = I2CReadByte(AB18_ADD, 0x06, 0, 2);

    globalTime.sec = ((a&0x70)>>4)*10+(a&0x0f); //Convert to numbers format
    globalTime.min = ((b&0x70)>>4)*10+(b&0x0f);
    globalTime.hour = ((c&0x30)>>4)*10+(c&0x0f);
    globalTime.day = ((d&0x30)>>4)*10+(d&0x0f);
    globalTime.mon = ((e&0x10)>>4)*10+(e&0x0f);
    globalTime.year = (((f&0xf0)>>4)*10+(f&0x0f));

//    sprintf(Debugbuf,"OUTput:%d:%d:%d %lu/%lu/%lu\n", globalTime.hour,globalTime.min,globalTime.sec,globalTime.day,globalTime.mon,globalTime.year);
//    U2SendArray(Debugbuf);
    
    globalTime.year += 31; //To get to amount from 1970
    
    return getUnixTime(globalTime.year,globalTime.mon,globalTime.day,globalTime.hour,globalTime.min,globalTime.sec);
}
uint8_t AB18Init(){
    I2CWriteByte(AB18_ADD, 0x1c, 0x00, 2); //Set crystal osc, set autocalib, set osc fail int
    I2CWriteByte(AB18_ADD, 0x10, 0x17, 2); //Start RTC and set int clear and PWR pin
    I2CWriteByte(AB18_ADD, 0x11, 0x38, 2); //Set power and int pin control
    if(I2CReadByte(AB18_ADD, 0x10, 0, 2)&0x17){return 1;}
    else{return 0;}
}
void AB18SetTimer(uint16_t sec, uint8_t force){
    
    uint8_t timer = 0xa2;
    if(sec & 0xff00){
        sec /= 60;
        timer = 0xa3;
    }
#ifdef CE    
    timer = 0xa1;
#endif    
    if(sec > 0xff){sec = 0xff;}

    if((I2CReadByte(AB18_ADD, 0x1A, 0, 2) != sec)||(force == 1)){
        I2CWriteByte(AB18_ADD, 0x18, 0, 2);
        I2CWriteByte(AB18_ADD, 0x1A, (uint8_t)sec, 2); //Set timer init value
        I2CWriteByte(AB18_ADD, 0x19, (uint8_t)sec, 2); //Set timer value
    }
    
    if(I2CReadByte(AB18_ADD, 0x18, 0, 2) != timer){
       I2CWriteByte(AB18_ADD, 0x18, timer, 2); //Start timer and set counter Hz
    }
}
void AB18EnableInterrupt(uint8_t interrupts){
    I2CWriteByte(AB18_ADD, 0x12, (interrupts | 0xe0), 2);//0xe9); //Set timer and IO int
    I2CWriteByte(AB18_ADD, 0x17, 0, 2); //Set int on falling adge
}
void AB18DisableInterrupt(){
    if(I2CReadByte(AB18_ADD, 0x12, 0, 2) != 0xe0){
        I2CWriteByte(AB18_ADD, 0x12, 0xe0, 2); //Disable int
    }
}
uint8_t AB18Sleep(){
    I2CWriteByte(AB18_ADD, 0x17, 0b10000100, 2); //Enter sleep (cut power)
    if(I2CReadByte(AB18_ADD, 0x17, 0, 2)&0x80){
        I2CWriteByte(AB18_ADD, 0x1c, 0x04, 2);
        return 1;
    }else{
        return 0;
    }
}
uint8_t AB18GetInterrupt(){
    return I2CReadByte(AB18_ADD, 0x0f, 0, 2);
}
uint8_t AB18getButtonLevel(){
    if(I2CReadByte(AB18_ADD, 0x3f, 0, 2)&0x10){
        return 1;
    }else{
        return 0;
    }
}
uint8_t AB18getInterrupts(){
    return I2CReadByte(AB18_ADD, 0x12, 0, 2) & ~0xe0;
}
void AB18SWReaset(){
    I2CWriteByte(AB18_ADD, 0x1f, 0x3c, 2);
}
void AB18SetSCBackUp(){
    I2CWriteByte(AB18_ADD, 0x1f, 0x9d, 2);
    I2CWriteByte(AB18_ADD, 0x20, 0xa9, 2);
    I2CWriteByte(AB18_ADD, 0x1f, 0x9d, 2);
    I2CWriteByte(AB18_ADD, 0x27, 0x00, 2);
}