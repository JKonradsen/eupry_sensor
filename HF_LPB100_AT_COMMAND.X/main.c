#include "main.h"

#ifndef PRODUCTION_DEVICE
//Encryption key for AES encryption
unsigned char key[16] = {'B','i','r','k','e','d','o','m','m','e','r','v','e','j','2','9'};
unsigned char tempKey[16] = {'B','i','r','k','e','d','o','m','m','e','r','v','e','j','2','9'};
#endif

//General globals
int timeOffseet = 0; //Unix time error
uint16_t batteryVolt = 0; //Gobal battery voltage

//Interruper related veriables:
volatile uint8_t userLooking = 0; //if the user is looking on the device or not (for wifi connection leds)
volatile uint8_t GLOBALuserPushFlag = 0;

//Serial buffers
volatile uint16_t U1IndexInterupt = 0;
volatile uint16_t ModulusU1IndexInterupt = 0;
volatile char U1buf[513]; //Recorde UART
volatile uint8_t U1intFlag = 0;

volatile uint8_t U2InteruptIndex = 0;
volatile uint8_t U2ReadIndex = 0;
volatile char U2buf[256]; //Recorde UART
volatile uint8_t U2intFlag = 0;

volatile uint8_t intLeds = 0;
volatile uint8_t intTimerMult = 0;
volatile uint8_t intTimerMultValue = 0;
volatile uint16_t intTimer = 0;

char API[41];

char Debugbuf[100];
char Debugbuf2[257];

uint8_t state = 0; //The curent state the code is at

int minorSendTimeoutNumber;
int majorSendTimeoutNumber;
int overallSendTimeoutNumber;



void interrupt test(void){
    if(PIR1bits.RC1IF){ //RCIF - set high on fifth rising edge on uart
        while(PIR1bits.RC1IF){
            U1buf[ModulusU1IndexInterupt] = RCREG1; //Recorde UART
            U1buf[ModulusU1IndexInterupt+1] = 0;
            U1IndexInterupt++;
            ModulusU1IndexInterupt = U1IndexInterupt%512;
        }
        PIR1bits.RC1IF = 0;
        U1intFlag = 1;
    }else if(PIR3bits.RC2IF){ //RCIF - set high on fifth rising edge on uart
        while(PIR3bits.RC2IF){U2buf[U2InteruptIndex++] = RCREG2;} //Recorde UART
        PIR3bits.RC2IF = 0;
        U2intFlag = 1;
    }else if(INTCONbits.INT0IF){ // INT0IF - External interrupt on button press
        intTimerMult = 25; //Set LED timer low to delay timer led blink if activated
        
        //Turn off and disable LEDs when pushed
        LEFT_LED_TRIS = 0;
        RIGHT_LED_TRIS = 0;
        GREEN_LED_TRIS = 0;
        YELLOW_LED_TRIS = 0;
        RED_LED_TRIS = 0;
        LEFT_LED_LAT = 0;
        RIGHT_LED_LAT = 0;
        
        userLooking = 1; //Set userLooking to determind if LED should be on

        if(RTCbuseFlag == 0){INTuserPush(1);} //Only call this function if no RTC comunication is on
        else{RTCbuseFlag = 2;} //Other wise set flag so the fucntion is called after RTC comunication is done
        INTCONbits.INT0IF = 0; // Clear button press interrupt
    }else if(PIR1bits.TMR1IF){ //Timer for led blinking under process (now support only wifi led, as this is the only process)
        //Reset timer int
        PIR1bits.TMR1IF = 0;
        INTCONbits.GIE = 1;
        TMR1H = 0x3c;
        TMR1L = 0xb0;
        
        //Why needed?
        LEFT_LED_TRIS = 0;
        RIGHT_LED_TRIS = 0;
        GREEN_LED_TRIS = 0;
        YELLOW_LED_TRIS = 0;
        RED_LED_TRIS = 0;
        LEFT_LED_LAT = 0;
        RIGHT_LED_LAT = 0;
        
        if(intTimerMult == 0){
            RIGHT_LED_LAT = 1;
            intTimerMult = intTimerMultValue;
            lightLEDs(intLeds, LED_DELAY>>3,0);
        }else{
            intTimerMult--;
        }   
    }else{//Defult reset all int flags
        PIR1 = 0;
        PIR2 = 0;
        PIR3 = 0;
        PIR4 = 0;
        PIR5 = 0;
        INTCONbits.TMR0IF = 0;
        INTCONbits.INT0IF = 0;
        INTCONbits.RBIF = 0;
    }
    INTCONbits.GIE = 1;
}
void interupOn(){ //Anable intterupts+
    TRISBbits.RB0 = 1; //External int
    INTCON = 0b11011000; //INT0 interrupts on
    INTCON2 = 0b00000000; //INT0 on falling edge
    INTCON3 = 0;
    PIE1 = 0b00100000; //UART1 reciver int on
    PIE2 = 0;
    PIE3 = 0b00100000; //UART2 reciver int on
    RCON = 0; //No priorritys
    //Disable all int flags
    PIR1 = 0;
    PIR2 = 0;
    PIR3 = 0;
    PIR4 = 0;
    PIR5 = 0;
}
void interupOff(){ //Disable all int
    INTCON  = 0;
    INTCON2 = 0;
    INTCON3 = 0;
    PIE1 = 0;
    PIE2 = 0;
    PIE3 = 0;
    RCON = 0;
}
void delay_2ms(uint16_t delay){
    for(uint16_t y = 0; y < delay; y++){
        __delay_ms(2);
    }
}
void delay_10us(uint16_t delay){
    for(uint16_t t = 0; t < delay; t++){
        __delay_us(10);
    }
}
void IOinit(){
    //Set all pin as input (high impadence)
    TRISA = 0xff;
    TRISB = 0xff;
    TRISC = 0xff;

    //Disable all port b and c spetial features
    LATA = 0;
    IOCB = 0;
    WPUB = 0;
    LATB = 0;
    LATC = 0;

    //Disable all analog pins
    ANSELA = 0;
    ANSELB = 0;
    ANSELC = 0;
}
void initEEPROM(){
    //Set all EEPROM to start up value
    EEPROM_save8Bit(SPETIAL_LOG_FLAGS, 0);
    EEPROM_save8Bit(LOG_FLAGS_ADRESS, 0);
    EEPROM_save8Bit(CON_FLAGS_ADRESS, 0);
    EEPROM_save16Bit(SEND_INTERVAL_ADRESS, SEND_INTERVAL_DEFAULT);
    EEPROM_save32Bit(FLASH_INDEX_ADRESS, 1);
    EEPROM_save32Bit(FLASH_START_INDEX_ADRESS, 0);
    EEPROM_save16Bit(LOCAL_FLASH_INDEX_ADRESS, LOCAL_MEASUREMENTS_OFFSET);
    EEPROM_save32Bit(LAST_SEND_UNIXTIME_ADRESS, 0);
    EEPROM_save8Bit(ERROR_FLAGS_ADRESS, 0);
    EEPROM_save32Bit(NEXT_SEND_UNIXTIME_ADRESS, RTCgetUnixTime());
    EEPROM_save8Bit(MEASURMENT_INTERVAL_SEC_ADRESS, MEASURE_INTERVAL_DEFAULT);
    EEPROM_save32Bit(SEND_INDEX_ADRESS, 0);    
    EEPROM_save8Bit(MINOR_SEND_TIMEOUT_ADRESS, DEFAULT_minorSendTimeoutNumber);
    EEPROM_save8Bit(MAJOR_SEND_TIMEOUT_ADRESS, DEFAULT_majorSendTimeoutNumber);
    EEPROM_save8Bit(OVERALL_SEND_TIMEOUT_ADRESS, DEFAULT_overallSendTimeoutNumber);
    resetSENSORS();
}
void processU2Command(){
    U2SendDebug("C\n");
    while(U2ReadIndex != U2InteruptIndex){
        char temp = U2buf[U2ReadIndex++];
        switch (temp){
            case 'C': //init RTC
                U2SendDebug("c");
                if(!initRTC()){U2SendDebug("O");}
                else{U2SendDebug("B");}
                U2SendDebug("\n");
                break;
            case 'T': //init Sensors
                U2SendDebug("t");
                turnONSensors();
                if(!initSensors()){U2SendDebug("O");}
                else{U2SendDebug("B");}
                turnOFFSensors();
                U2SendDebug("\n");
                break;
            case 'A': //Set API
                U2SendDebug("a");
                uint8_t i = 0;
                while(U2ReadIndex != U2InteruptIndex){
                    API[i++] = U2buf[U2ReadIndex++];
                }
                API[i] = 0;
                U2SendDebug(API);
                U2SendDebug("\n");
                break;
            case 'S': //Save API
                U2SendDebug("s");
                EEPROMWriteArray(API_ADDRES, API);
                U2SendDebug("\n");
                break;
        }
    }
    U2Send('\n');
}

void Timer1ON(uint16_t HundmilSec){
    //Set timer with int
    intTimerMultValue = HundmilSec;
    intTimerMult = HundmilSec;
    T1CON = 0b00110001; //Set timer 1
    TMR1H = 0x3c; //Set timer 1 to count 100 milsec
    TMR1L = 0xb0;
    PIE1 = 0b00100001; //Set timer 1 intterupt
}
void Timer1OFF(){
    //Disable LEDs and stopping timer int
    GREEN_LED_LAT = 0;
    YELLOW_LED_LAT = 0;
    RED_LED_LAT = 0;
    T1CON = 0;
    TMR1H = 0;
    TMR1L = 0;
    PIE1 = 0b00100000;
}

void setBlink(uint8_t color, uint16_t HundmilSec){
    Timer1OFF();
    sprintf(Debugbuf,"setBlink %d\n", color);
    U2SendArray(Debugbuf);
    //Set a timer and color to blink
    intLeds = color;
    lightLEDs(intLeds, LED_DELAY>>3,1);
    Timer1ON(HundmilSec);
}
void waitForBlink(){
    //Not nesesery
   // while(YELLOW_LED_PORT || GREEN_LED_PORT || RED_LED_PORT);
}

uint8_t getWakeUpReason(){
    uint8_t userPush = userPushType();
#ifdef DEBUG_PRINT
    sprintf(Debugbuf,"userPush: %d\n", userPush);
    U2SendArray(Debugbuf);
#endif
//    if(PORTEbits.RE3 == 0){
//        RIGHT_LED_TRIS = 0;
//        LEFT_LED_TRIS = 0;
//        RIGHT_LED_LAT = 0;
//        LEFT_LED_LAT = 0;
//        GREEN_LED_TRIS = 0;
//        YELLOW_LED_TRIS = 0;
//        RED_LED_TRIS = 0;
//        GREEN_LED_LAT = 0;
//        YELLOW_LED_LAT = 0;
//        RED_LED_LAT = 0;
//        U2init(); //Init UART2 at 152000
//        U2ClearErrors();
//        delay_2ms(100);
//        U2SendDebug("DM\n");
//        interupOn();
//        while(PORTEbits.RE3 == 0){
//            /*
//            if(U2intFlag == 1){
//                delay_2ms(5);
//                U2intFlag = 0;
//                processU2Command();
//            }
//            delay_2ms(5);
//             */
//            testProgram();
//        }
//        return POWERON;
//    }else{
//        U2dis();
//    }
    
    //if(userPush == SUPER_LONG_PUSH){return POWERON;}
    /*else */if(userPush == LONG_PUSH){return SMART_LINK;}
    else if((!(getConFlags() & WIFI_WAS_INIT)) || (getLogFlags() == 0xff)){return POWERON;}
    else if(userPush == SHORT_PUSH){GLOBALuserPushFlag = SHORT_PUSH;return MEASURE;}
    else{
        if(getLogFlags() == NO_LOGGING){return CHECK_WIFI;}
        else{return MEASURE;}
    }
}
void updateNextSendTime(uint32_t sendInterval){
    EEPROM_save32Bit(LAST_SEND_UNIXTIME_ADRESS, RTCgetUnixTime());
    EEPROM_save32Bit(NEXT_SEND_UNIXTIME_ADRESS, RTCgetUnixTime() + sendInterval);
}
uint8_t getSendClearens(){
/*We send if:
 * I. We have not send in the last 10min:
 *      1. It is time to send (after 12 hours or so): This use the Unixtime from the RTC
 *      2. We dont have space on the flash
 *      3. We have increised send timer due to bad temperature (NOT IMPLEMENTED YET)
 *      4. User force send: (NOT IN THIS FUNCTION)
 *          a. Durring off mode
 *          b. Durring on mode
 * All of thouse, beside 4 are desidet at case 19.
 * 4a jump to case 8
 * 4b will have to be tested at case 26 and then turne the wifi on
 */
    uint8_t userPush = userPushType();
    uint32_t now = RTCgetUnixTime();
    uint32_t next = EEPROM_laod32Bit(NEXT_SEND_UNIXTIME_ADRESS);
    uint32_t last = EEPROM_laod32Bit(LAST_SEND_UNIXTIME_ADRESS);
#ifdef DEBUG_PRINT_EXTENDED
    sprintf(Debugbuf,"N:%lu S:%lu L:%lu %d-> ", now, next, last, userPush);
    U2SendArray(Debugbuf);
#endif
    if(userPush == LONG_PUSH){return LONG_PUSH;}
    if(userPush == SUPER_LONG_PUSH){return SUPER_LONG_PUSH;}
    if(now < 1000000000){ //time error, get time from server
        if(getBat() < TOO_LOW_VOLTAGE){return 0;}
#ifdef DEBUG_PRINT_EXTENDED        
        U2SendArray("RTC\n");
#endif
        uint8_t timeout = 10;
        while(!initRTC() && timeout > 0){
            timeout--;
            U2SendArray("RTC init bad\n");
        }
        RTCEnableInterrupt(USER_INT);
        return 1;
    }
    if(userPush == SHORT_PUSH){ //User push
        if(getBat() < TOO_LOW_VOLTAGE){return 0;}
        if((now > last + USER_PSUH_SEND_DELAY)||(getLogFlags()== NO_LOGGING)){
#ifdef DEBUG_PRINT_EXTENDED            
            U2SendArray("\nUser\n");
#endif      
            savePushEvent();
            if((getLogFlags() == TRANSPORT_LOGGING)&&(EEPROM_laod8Bit(SPETIAL_LOG_FLAGS) == 1)){
                EEPROM_save8Bit(SPETIAL_LOG_FLAGS, 2);
                return 0;
            }
            else{return 1;}
        }
    }
    if(sensorsArray.Flags & NEED_TO_SEND){//Alarm
        if(getBat() < TOO_LOW_VOLTAGE){return 0;}
        if(now > last + ALARM_SEND_DELAY){
            sensorsArray.Flags &= ~NEED_TO_SEND;
#ifdef DEBUG_PRINT_EXTENDED            
            U2SendArray("Alarm\n");
#endif            
            return 1;
        }
    }
    if(now > next){
        if(getBat() < TOO_LOW_VOLTAGE){return 0;}
#ifdef DEBUG_PRINT_EXTENDED 
        U2SendArray("Time\n");
#endif        
        return 1;
    }
    if(flashOverRunBit == 1){
        if(getBat() < TOO_LOW_VOLTAGE){return 0;}
#ifdef DEBUG_PRINT_EXTENDED        
        U2SendArray("FLASH\n");
#endif
        return 1;
    }
#ifdef DEBUG_PRINT_EXTENDED    
    U2SendArray("No\n");
#endif    
    return 0;
}

void INTuserPush(uint8_t forcePush){
    uint8_t RTCintmode = RTCreturnIntettupts();
    RTCDisableInterrupt();
    uint8_t RTCint = RTCgetInts();
    if(RTCint == USER_PUSH || RTCint == (USER_PUSH | TIMER_END) || forcePush || (!RTCgetButtonLevel())){
        uint16_t delay = 600;
        ClrWdt();
        while(!RTCgetButtonLevel() && (delay > 0)){__delay_ms(10);if(delay > 0){delay--;}}
        if(delay == 0){
            userLooking = 2;
            GLOBALuserPushFlag = LONG_PUSH;
        }else{
            if(userLooking == 0){userLooking = 1;}
            GLOBALuserPushFlag = SHORT_PUSH; //The user has pushed while the device is on... This need to be handled before sleeping
            showLight();
        }
    }else if(RTCint == TIMER_END){
        GLOBALuserPushFlag = TIMER;
    }else{
        GLOBALuserPushFlag = NO_PUSH;
    }
    RTCEnableInterrupt(RTCintmode);
}
uint8_t userPushType(){
    if(GLOBALuserPushFlag != 0 ){
        if(GLOBALuserPushFlag == TIMER){
            GLOBALuserPushFlag = 0;
#ifdef DEBUG_PRINT
            U2SendArray("TIMER EVENT\n");
#endif
            return TIMER;
        }else if(GLOBALuserPushFlag == SHORT_PUSH){
            GLOBALuserPushFlag = 0;
#ifdef DEBUG_PRINT
            U2SendArray("FORCE UPLOAD\n");
#endif
            return SHORT_PUSH;
        }else if(GLOBALuserPushFlag == LONG_PUSH){
            GLOBALuserPushFlag = 0;
#ifdef DEBUG_PRINT
            U2SendArray("RESETUP\n");
#endif
            return LONG_PUSH;
        }else if(GLOBALuserPushFlag == SUPER_LONG_PUSH){
            GLOBALuserPushFlag = 0;
#ifdef DEBUG_PRINT
            U2SendArray("REPOWERING\n");
#endif
            return SUPER_LONG_PUSH;
        }
    }
    return NO_PUSH;
}
void showLight(){
    if(state != SMART_LINK){
        uint8_t conFlags = getConFlags();
        uint8_t logFlags = getLogFlags();
        if(batteryVolt == 0){batteryVolt = getBat();}
        if(logFlags == UNKNOWEN_LOGGING){
            if(sensorsArray.Flags & (HIGH_ALARM_FLAG|LOW_ALARM_FLAG)){
                if(batteryVolt < TOO_LOW_VOLTAGE){
                    lightLEDs(RED_YELLOW, LED_DELAY,1);
                    lightLEDs(RED_OFF, LED_DELAY,1);
                }
                else if(conFlags & SERVER_CONNECTION){lightLEDs(GREEN_YELLOW, LED_DELAY,1);}
                else if(conFlags & WIFI_CONNECTION){lightLEDs(YELLOW_YELLOW, LED_DELAY,1);}
                else{lightLEDs(RED_YELLOW, LED_DELAY,1);}
            }else{
                if(batteryVolt < TOO_LOW_VOLTAGE){
                    lightLEDs(RED_GREEN, LED_DELAY,1);
                    lightLEDs(RED_OFF, LED_DELAY,1);
                }
                else if(conFlags & SERVER_CONNECTION){lightLEDs(GREEN_GREEN, LED_DELAY,1);}
                else if(conFlags & WIFI_CONNECTION){lightLEDs(YELLOW_GREEN, LED_DELAY,1);}
                else{lightLEDs(RED_GREEN, LED_DELAY,1);}
            }
        }else if(logFlags == TRANSPORT_LOGGING){
            if((EEPROM_laod8Bit(SPETIAL_LOG_FLAGS) == 0) && (conFlags & SERVER_CONNECTION)){
                EEPROM_save8Bit(SPETIAL_LOG_FLAGS, 1);
                unSetConFlag(WIFI_CONNECTION | SERVER_CONNECTION | CONNECTION);
                lightLEDs(OFF_GREEN, LED_DELAY,1);
            }
        }else{
            lightLEDs(GREEN_GREEN, LED_DELAY,1);
        }
    }
    RTCEnableInterrupt(USER_INT);
}

uint8_t Fletcher8(uint8_t* data, char terminator){
    uint16_t sum1 = 0;
    uint16_t sum2 = 0;
    uint8_t temp = 0;
    
    while(data[temp] != terminator){
        sum1 = (sum1 + (uint8_t)data[temp++]) % 15;
        sum2 = (sum1 + sum2) % 15;
    }
    return ((sum2 << 4) | sum1);
}

uint8_t getConFlags(){
    return EEPROM_laod8Bit(CON_FLAGS_ADRESS); //((getSystemFlags() >> 4)&0x0f);
}
void setConFlag(uint8_t flag){
    EEPROM_save8Bit(CON_FLAGS_ADRESS, EEPROM_laod8Bit(CON_FLAGS_ADRESS)|(flag));
}
void unSetConFlag(uint8_t flag){
    EEPROM_save8Bit(CON_FLAGS_ADRESS, EEPROM_laod8Bit(CON_FLAGS_ADRESS)&(~(flag)));
}

uint8_t getLogFlags(){
    return EEPROM_laod8Bit(LOG_FLAGS_ADRESS); //return (getSystemFlags()&0x0f);
}
void setLogFlag(uint8_t flag){
    EEPROM_save8Bit(LOG_FLAGS_ADRESS, (EEPROM_laod8Bit(LOG_FLAGS_ADRESS))|(flag));
}
void unSetLogFlag(uint8_t flag){
    EEPROM_save8Bit(LOG_FLAGS_ADRESS, EEPROM_laod8Bit(LOG_FLAGS_ADRESS)&(~(flag)));
}

uint8_t getError(){
    return EEPROM_laod8Bit(ERROR_FLAGS_ADRESS);
}
void setError(uint8_t error){
    EEPROM_save8Bit(ERROR_FLAGS_ADRESS, EEPROM_laod8Bit(ERROR_FLAGS_ADRESS)|error);
    EEPROM_save8Bit(ERROR_TO_SEND_ADRESS, EEPROM_laod8Bit(ERROR_TO_SEND_ADRESS)|error);
}
void unSetError(uint8_t error){
    EEPROM_save8Bit(ERROR_FLAGS_ADRESS, EEPROM_laod8Bit(ERROR_FLAGS_ADRESS)&(~error));
}

void initPoweronVariables(){
    EEPROM_save8Bit(MINOR_SEND_TIMEOUT_ADRESS, DEFAULT_minorSendTimeoutNumber);
    EEPROM_save8Bit(MAJOR_SEND_TIMEOUT_ADRESS, DEFAULT_majorSendTimeoutNumber);
    EEPROM_save8Bit(OVERALL_SEND_TIMEOUT_ADRESS, DEFAULT_overallSendTimeoutNumber);
}

uint16_t getBat(){
    VREFCON0 = 0b10010000;
    ADCON2 = 0b10111011;
    ADCON1 = 0; //Select refarance
    ADCON0 = 0b01111100; //Select channel
    ADCON0bits.ADON = 1; //Start collection
    __delay_us(20);
    ADCON0bits.GO = 1; //Start conertion
    while(ADCON0bits.GO); //Wait until done
    uint16_t volt = ADRESH;
    volt <<= 8;
    volt |= ADRESL; //Get data
    volt = 1048576/volt;
    return volt;
}

int main(){
// <editor-fold defaultstate="collapsed">    
    OSCCON = 0b01110010; //Set internal 16Mhz crystal
    IOinit(); //Init general IOs and remove analog
    
    TRISCbits.RC5 = 0;
    LATCbits.LATC5 = 0;
    
    U1buf[512] = 0; //Set last byte in arrays to zero (this byte it to prevent memory leak and should never be change
    I2Cinit(2);//Init I2C for RTC at ~400Khz
    
#ifdef JAKOB_WORKING
#endif
#ifdef NOAM_WORKING
    
//    long deviceTime = 1455944400;
//    while(1==1){
//        ClrWdt();
//        setTimeFromUnixTime(deviceTime);
//        long tempTime = RTCgetUnixTime();
//        sprintf(Debugbuf,"%lu = %lu -> %lu\n\n", deviceTime, tempTime, deviceTime - tempTime);
//        U2SendArray(Debugbuf);
//        deviceTime += 86400;
//        delay_2ms(10);
//    }
        EEPROM_save8Bit((0 + ENCRYPTION_KEY_ADDRES), 0x92);
        EEPROM_save8Bit((1 + ENCRYPTION_KEY_ADDRES), 0x65);
        EEPROM_save8Bit((2 + ENCRYPTION_KEY_ADDRES), 0xa4);
        EEPROM_save8Bit((3 + ENCRYPTION_KEY_ADDRES), 0x2a);
        EEPROM_save8Bit((4 + ENCRYPTION_KEY_ADDRES), 0x21);
        EEPROM_save8Bit((5 + ENCRYPTION_KEY_ADDRES), 0x4a);
        EEPROM_save8Bit((6 + ENCRYPTION_KEY_ADDRES), 0x10);
        EEPROM_save8Bit((7 + ENCRYPTION_KEY_ADDRES), 0x42);
        EEPROM_save8Bit((8 + ENCRYPTION_KEY_ADDRES), 0x2a);
        EEPROM_save8Bit((9 + ENCRYPTION_KEY_ADDRES), 0x57);
        EEPROM_save8Bit((10 + ENCRYPTION_KEY_ADDRES), 0xf4);
        EEPROM_save8Bit((11 + ENCRYPTION_KEY_ADDRES), 0x21);
        EEPROM_save8Bit((12 + ENCRYPTION_KEY_ADDRES), 0x86);
        EEPROM_save8Bit((13 + ENCRYPTION_KEY_ADDRES), 0x76);
        EEPROM_save8Bit((14 + ENCRYPTION_KEY_ADDRES), 0x42);
        EEPROM_save8Bit((15 + ENCRYPTION_KEY_ADDRES), 0xd9);
        

        EEPROM_save8Bit((0 + DEVICE_NUMBER_ADDRES), '0');
        EEPROM_save8Bit((1 + DEVICE_NUMBER_ADDRES), '0');
        EEPROM_save8Bit((2 + DEVICE_NUMBER_ADDRES), '0');
        EEPROM_save8Bit((3 + DEVICE_NUMBER_ADDRES), '0');
        EEPROM_save8Bit((4 + DEVICE_NUMBER_ADDRES), '0');
        EEPROM_save8Bit((5 + DEVICE_NUMBER_ADDRES), '0');
        EEPROM_save8Bit((6 + DEVICE_NUMBER_ADDRES), '3');
        EEPROM_save8Bit((7 + DEVICE_NUMBER_ADDRES), '1');
        
        EEPROM_save8Bit((0 + API_ADDRES), 'V');
        EEPROM_save8Bit((1 + API_ADDRES), 'v');
        EEPROM_save8Bit((2 + API_ADDRES), 'H');
        EEPROM_save8Bit((3 + API_ADDRES), 'l');
        EEPROM_save8Bit((4 + API_ADDRES), 'Q');
        EEPROM_save8Bit((5 + API_ADDRES), 'X');
        EEPROM_save8Bit((6 + API_ADDRES), 'L');
        EEPROM_save8Bit((7 + API_ADDRES), 'u');
        EEPROM_save8Bit((8 + API_ADDRES), 'v');
        EEPROM_save8Bit((9 + API_ADDRES), 'H');
        EEPROM_save8Bit((10 + API_ADDRES), 'A');
        EEPROM_save8Bit((11 + API_ADDRES), '5');
        EEPROM_save8Bit((12 + API_ADDRES), 'H');
        EEPROM_save8Bit((13 + API_ADDRES), 'i');
        EEPROM_save8Bit((14 + API_ADDRES), 'Y');
        EEPROM_save8Bit((15 + API_ADDRES), 'w');
        EEPROM_save8Bit((16 + API_ADDRES), 'n');
        EEPROM_save8Bit((17 + API_ADDRES), 'b');
        EEPROM_save8Bit((18 + API_ADDRES), 'n');
        EEPROM_save8Bit((19 + API_ADDRES), 'Y');
        EEPROM_save8Bit((20 + API_ADDRES), 'Z');
        EEPROM_save8Bit((21 + API_ADDRES), 'm');
        EEPROM_save8Bit((22 + API_ADDRES), 'W');
        EEPROM_save8Bit((23 + API_ADDRES), '0');
        EEPROM_save8Bit((24 + API_ADDRES), 'e');
        EEPROM_save8Bit((25 + API_ADDRES), 'j');
        EEPROM_save8Bit((26 + API_ADDRES), 'Q');
        EEPROM_save8Bit((27 + API_ADDRES), 'C');
        EEPROM_save8Bit((28 + API_ADDRES), '8');
        EEPROM_save8Bit((29 + API_ADDRES), 'B');
        EEPROM_save8Bit((30 + API_ADDRES), 'm');
        EEPROM_save8Bit((31 + API_ADDRES), '4');
    
    
#endif

    uint8_t timeout = 0;
    
    while(1==1){ //Main program start
        switch(state){
//************************************************************************************************//
//************************************* START UP *************************************************//
//************************************************************************************************//
            case NOTHING: //Start go into shipment mode
// <editor-fold defaultstate="collapsed">
#ifdef DEBUG_PRINT_STATES
                U2SendArray("State NOTHING reached\n");
                U2SendArray("Starting program\n");
                sprintf(Debugbuf,"Errors: %d\n", getError());
                U2SendArray(Debugbuf);
#endif
                if(RCONbits.TO == 0){saveWDTevent(1);}
                state = GET_WAKEUP;
                break;
// </editor-fold>
            case GET_WAKEUP: //Check Wakeup Reason
// <editor-fold defaultstate="collapsed">                
#ifdef DEBUG_PRINT_STATES
                U2SendArray("State GET_WAKEUP reached\n");
#endif
                loadSENSORS(); //Load the data from the EEPROM to sensorsArray
                INTuserPush(0);
                
                state = getWakeUpReason();//Together with RTC and last state calculate the wakeup reason
                
                RTCEnableInterrupt(USER_INT);
                interupOn(); //Init INT0 and UART1 & UART2 int
                
                break;
//</editor-fold>
            case POWERON: //Init system
// <editor-fold defaultstate="collapsed">                
//#ifdef DEBUG_PRINT_STATES
                U2SendArray("State POWERON reached\n");
//#endif
                
                if(getBat() < TOO_LOW_VOLTAGE){                        
                    RTCDisableInterrupt();
                    while(1==1){
                        lightLEDs(RED_RED, LED_DELAY >> 2, 0);
                        delay_2ms(250);
                    }
                }
                
                for(uint8_t u = 0; u < 16; u++){lightLEDs(u, LED_DELAY >> 3, 1);}//Test LEDs
                userLooking = 1;
                setBlink(RED_OFF, 10);
                
                turnONWifiModule();
                initEEPROM();
                if(getError()==0xff){unSetError(0xff);}
                
                turnONFLASH();
                AT25DFWriteArraySlow(0, "test");
                if(_AT25DFRead(0) != 't'){
                    setError(FLASH_ERROR_FLAG);
                    if(_AT25DFRead(1) != 'e'){
                        setError(FLASH_ERROR_FLAG);
                        if(_AT25DFRead(2) != 's'){
                            setError(FLASH_ERROR_FLAG);
                            if(_AT25DFRead(3) != 't'){
                                setError(FLASH_ERROR_FLAG);
                                if(_AT25DFRead(4) != 0xff){
                                    setError(FLASH_ERROR_FLAG);
                                }
                            }
                        }
                    }
                }
                initEEPROM();
                FLASHdeletPage(0); //1st time power on clear the 1st page on the flesh 
                saveGeneral("{\"99\":{\"X\":[99.99]}}"); //Save startup point
#ifdef hardware_version_V200
                turnOFFFLASH();
#endif
                EEPROM_save32Bit(FLASH_INDEX_ADRESS, 1);
                EEPROM_save32Bit(FLASH_START_INDEX_ADRESS, 0);
                
                //Init RTC and check for errors
                if(!initRTC()){setError(RTC_ERROR_FLAG);}
                else{unSetError(RTC_ERROR_FLAG);RTCEnableInterrupt(USER_INT);}

                //Init sensor and check for errors
                turnONSensors();
                if(!initSensors()){setError(SENSOR_ERROR_FLAG);}
                else{unSetError(SENSOR_ERROR_FLAG);}
#ifndef NO_WIFI
                //Init wifi and check for errors
                timeout = 100;
                while(NREADY && timeout > 0){delay_2ms(50);timeout--;}
                if(!initWifiModule()){setError(WIFI_ERROR_FLAG);}
                else{unSetError(WIFI_ERROR_FLAG);}
                state = INIT_WIFI;                
#endif
#ifdef NO_WIFI
                state = TEST_LOGFLAG;
#endif
//#ifdef DEBUG_PRINT
                sprintf(Debugbuf,"Errors: %d\n", getError());
                U2SendArray(Debugbuf);
//#endif
                break;
//</editor-fold>
//************************************************************************************************//
//************************************* MEASURING ************************************************//
//************************************************************************************************//
            case MEASURE: //Set measure mode
// <editor-fold defaultstate="collapsed">                
#ifdef DEBUG_PRINT_STATES
                U2SendArray("State MEASURE reached\n");
#endif
                turnONSensors();
                timeout = 5;
                while(timeout > 0){
                    if(saveMeasuremntPoint()){break;} //Save a measurment point in to the flash and save new data in to sensorArray
                    timeout--;
                }
                if(timeout == 0){setError(SENSOR_ERROR_FLAG);}
              
                uint8_t sendClearens = getSendClearens();    
                
                /*if(sendClearens == SUPER_LONG_PUSH){state = POWERON;break;}
                else */if(sendClearens == LONG_PUSH){state = SMART_LINK;}
                
                else if(sendClearens == 1){state = CHECK_WIFI;}
#ifdef FAST_MEASURE                
                else{state = MEASURE;delay_2ms(200);}
#else                
                else{state = TEST_USER_PUSH_BEFORE_SLEEP;}
#endif                

                break;
//</editor-fold>
//************************************************************************************************//
//************************************* WIFI INIT & SENDING **************************************//
//************************************************************************************************//
            case INIT_WIFI: //Init WiFi credentials
// <editor-fold defaultstate="collapsed">
#ifdef DEBUG_PRINT_STATES
                U2SendArray("State INIT_WIFI reached\n");
#endif
                if(initWifiCredentials()){
                    setConFlag(WIFI_WAS_INIT);
                    state = CHECK_WIFI;
                }else{
                    unSetConFlag(WIFI_WAS_INIT);
                    state = SLEEP_USER_PUSH;
                    lightLEDs(RED_RED, LED_DELAY,1);
                    lightLEDs(RED_RED, LED_DELAY,1);
                }
                break;
//</editor-fold>
            case SMART_LINK: //Search for Wifi Setup AP / STA
// <editor-fold defaultstate="collapsed">                
#ifdef DEBUG_PRINT_STATES
                U2SendArray("State SMART_LINK reached\n");
#endif
                if(getBat() > TOO_LOW_VOLTAGE){
                    if((!RTCgetButtonLevel())){
                        startupWifiModule();
                        echo(0);
                        delay_2ms(5);
                        clearOutputBuffer();
                        delay_2ms(5);
                        setBlink(GREEN_OFF, 2);
                        if(setSmartLink()){
                            waitForBlink();
                            setBlink(YELLOW_OFF, 10);
                            state = CHECK_WIFI;
                        }else{
                            turnOFFWifiModule();
                            waitForBlink();
                            Timer1OFF();
                            unSetConFlag(WIFI_CONNECTION | SERVER_CONNECTION | CONNECTION);
                            turnOFFWifiModule();
                            
                            if(userPushType() == LONG_PUSH){ //3 Long push for master reset
                                lightLEDs(YELLOW_YELLOW, LED_DELAY,1);
                                uint16_t delay = 500;
                                while((GLOBALuserPushFlag == 0) && (delay > 0)){__delay_ms(10);if(delay > 0){delay--;}} 
                                if(userPushType()==LONG_PUSH){state = POWERON;
                                }else{
                                    lightLEDs(RED_OFF, LED_DELAY,1);
                                    state = TEST_LOGFLAG;
                                }
                            }else{
                                lightLEDs(RED_OFF, LED_DELAY,1);
                                state = TEST_LOGFLAG;
                            }
                        }
                    }else{
                        state = TEST_LOGFLAG;
                    }
                }else{
                    lightLEDs(RED_OFF, LED_DELAY,1);
                    delay_2ms(100);
                    lightLEDs(RED_OFF, LED_DELAY,1);
                    state = TEST_LOGFLAG;
                }
                RTCsetTimer(50, 1);
                break;
//</editor-fold>                
            case CHECK_WIFI: //Check for wifi
// <editor-fold defaultstate="collapsed">                
#ifdef DEBUG_PRINT_STATES
                U2SendArray("State CHECK_WIFI reached\n");
#endif                
                if(EEPROM_laod8Bit(MAJOR_SEND_TIMEOUT_ADRESS) == 0){  //Major send timeout reached, should wait until next send time to try and send
                    EEPROM_save8Bit(MAJOR_SEND_TIMEOUT_ADRESS, DEFAULT_majorSendTimeoutNumber);
                    setError(DATASEND_TIMEOUT_ERROR_FLAG);
                    EEPROM_save8Bit(OVERALL_SEND_TIMEOUT_ADRESS, EEPROM_laod8Bit(OVERALL_SEND_TIMEOUT_ADRESS)-1);
                    if(EEPROM_laod8Bit(OVERALL_SEND_TIMEOUT_ADRESS) == 0){
                        EEPROM_save8Bit(OVERALL_SEND_TIMEOUT_ADRESS, DEFAULT_overallSendTimeoutNumber);
                        updateNextSendTime(EEPROM_laod16Bit(SEND_INTERVAL_ADRESS)*60*2);
                        U2SendArray("SL\n");
                    }else{
                        updateNextSendTime(EEPROM_laod16Bit(SEND_INTERVAL_ADRESS)*60);
                        U2SendArray("SI\n");
                    }
                    state = END_OF_TRANSMITION;
                    break;
                }else{
                    EEPROM_save8Bit(MAJOR_SEND_TIMEOUT_ADRESS,EEPROM_laod8Bit(MAJOR_SEND_TIMEOUT_ADRESS)-1);
                }
                
                turnONWifiModule();
                uint8_t conFlags = getConFlags();
                uint8_t logFlags = getLogFlags();
                
                if((userLooking == 1) && (!(conFlags & WIFI_CONNECTION)||(logFlags == NO_LOGGING))){setBlink(RED_OFF, 10); userLooking = 2;}
                
                timeout = 20;
                while((timeout > 0) && NLINK && (GLOBALuserPushFlag != LONG_PUSH)){
#ifdef DEBUG_PRINT_EXTENDED
                    U2SendArray("getting link!\n");
#endif
                    if((userLooking == 1) && (!(conFlags & WIFI_CONNECTION)||(logFlags == NO_LOGGING))){setBlink(RED_OFF, 10); userLooking = 2;}
                    delay_2ms(250);
                    timeout--;
                }
                if(!NLINK && (GLOBALuserPushFlag != LONG_PUSH)){ //Got the link
#ifdef DEBUG_PRINT_EXTENDED
                    U2SendArray("GOT LINK!\n");
#endif
                    if((userLooking != 0) && (!(conFlags & WIFI_CONNECTION)||!(conFlags & SERVER_CONNECTION)||(logFlags == NO_LOGGING))){setBlink(YELLOW_OFF, 10);}
                    setConFlag(WIFI_CONNECTION);
                    saveWifiStrength();
                    state = SEND_DATA;
                    break;
                }
#ifdef DEBUG_PRINT_EXTENDED                
                U2SendArray("NO LINK\n");
#endif           
                Timer1OFF();
                unSetConFlag(WIFI_CONNECTION | SERVER_CONNECTION | CONNECTION);
                setError(DATASEND_TIMEOUT_ERROR_FLAG | DATASEND_NOLINK2);
                //Change//setLogFlag(UNKNOWEN_LOGGING);
                
                
                if(userLooking && (!(conFlags & WIFI_CONNECTION)||(logFlags == NO_LOGGING)) && (GLOBALuserPushFlag != LONG_PUSH)){lightLEDs(RED_OFF, 50,1);}
                turnOFFWifiModule();
                RTCsetTimer(50, 1);
                state = END_OF_TRANSMITION;
                break;
//</editor-fold>
            case SEND_DATA: //Send data
// <editor-fold defaultstate="collapsed">                
#ifdef DEBUG_PRINT_STATES
                U2SendArray("State SEND_DATA reached\n");
#endif
                saveSensorFlags();
                long sendIndex = EEPROM_laod32Bit(SEND_INDEX_ADRESS);
                int localIndex = (int)EEPROM_laod16Bit(LOCAL_FLASH_INDEX_ADRESS)-LOCAL_MEASUREMENTS_OFFSET;
                long startIndex = EEPROM_laod32Bit(FLASH_START_INDEX_ADRESS);
                long FLASHindex = EEPROM_laod32Bit(FLASH_INDEX_ADRESS);
                long amountToSend = 0;

                if(startIndex == FLASHindex){amountToSend = FLASH_MAX_INDEX + localIndex;}
                else if(startIndex > FLASHindex){amountToSend = (FLASH_MAX_INDEX - startIndex) + FLASHindex + localIndex;}
                else{amountToSend = (FLASHindex - startIndex) + localIndex;}
                
                uint8_t tryes = EEPROM_laod8Bit(MINOR_SEND_TIMEOUT_ADRESS);

//                uint32_t deviceNumber = 0;
//                for(uint8_t y = 0; y < 8; y++){
//                    deviceNumber *= 10;
//                    deviceNumber += (EEPROM_laod8Bit(y + DEVICE_NUMBER_ADDRES)-48);
//                }
//                if(deviceNumber < 2000000000){
//                    //Load encription key
//                    for(uint8_t u = 0; u < 16; u++){
//                        key[u] = EEPROM_laod8Bit(ENCRYPTION_KEY_ADDRES + u);
//                        tempKey[u] = key[u];
//                    }
//                }                
#ifdef DEBUG_PRINT
                sprintf(Debugbuf,"sendIndex: %lu\n", sendIndex);
                U2SendArray(Debugbuf);
                sprintf(Debugbuf,"localIndex: %lu\n", localIndex);
                U2SendArray(Debugbuf);
                sprintf(Debugbuf,"startIndex: %lu\n", startIndex);
                U2SendArray(Debugbuf);
                sprintf(Debugbuf,"FLASHindex: %lu\n", FLASHindex);
                U2SendArray(Debugbuf);
                sprintf(Debugbuf,"amountToSend: %lu\n", amountToSend);
                U2SendArray(Debugbuf);
#endif
                char dataSuccess;

                long maxSend = MIN_SEND_LENGTH;
                if(EEPROM_laod8Bit(MAJOR_SEND_TIMEOUT_ADRESS) == DEFAULT_majorSendTimeoutNumber - 1){maxSend = MAX_SEND_LENGTH;}
                uint8_t conFlags = getConFlags();
#ifdef CE
                while(1==1){
#else                
                while(sendIndex <= amountToSend && tryes > 0 && (GLOBALuserPushFlag != LONG_PUSH)){
#endif                                     
                    if(state != SEND_DATA){break;}
                    U1ClearErrors();
                    
                    //Send maximum
                    if((sendIndex + maxSend) <= amountToSend){dataSuccess = sendDataThroughputMode(sendIndex, maxSend, 0);}
                    //Send left over and pad data for incryption
                    else if(amountToSend > 0){dataSuccess = sendDataThroughputMode(sendIndex, (amountToSend - sendIndex)+(ENCRYPTION_LENGTH-((amountToSend - sendIndex)%ENCRYPTION_LENGTH)), 1);}
                    //Other wise send zero (using send function to get confige and unixtime)
                    else{dataSuccess = sendDataThroughputMode(0, 0, 1);}
                    if(dataSuccess == DATASEND_OK){
                        //clearSendsErrors();
                        U2SendArray("DATASEND OK\n");
#ifndef CE                        
                        sendIndex = EEPROM_laod32Bit(SEND_INDEX_ADRESS); //Re load sendIndex as it has been change in sendDataThroughputMode
#endif                        
                        tryes = maximumSendNumber; //Reset tryes
                        unSetError(DATASEND_TIMEOUT_ERROR_FLAG);
                        
                        waitForBlink();
                        Timer1OFF();
                        if(userLooking && (!(conFlags & SERVER_CONNECTION)||(logFlags == NO_LOGGING))){
                            userLooking = 0;
                            lightLEDs(GREEN_OFF, LED_DELAY,1);
                            lightLEDs(GREEN_OFF, LED_DELAY,1);
                            lightLEDs(GREEN_OFF, LED_DELAY,1);
                        }
                        setConFlag(SERVER_CONNECTION | CONNECTION);
                        //unSetConFlag(CONNECTION);
                        
                        if(sendIndex >= amountToSend){  //All data has been sent

                            //Clear errors connevted to sending data
                            unSetError(DATASEND_TIMEOUT_ERROR_FLAG | DATASEND_CONNECTERROR | DATASEND_NOLINK2);
                            //unSetError(DATASEND_CONNECTERROR);
                            //unSetError(DATASEND_NOLINK2);


                            for(uint8_t y = 0; y < 5; y++){
                                if(updateConfigFromResponse()==0){dataSuccess = SEVER_SEND_ERROR; continue;}
                                else{dataSuccess = DATASEND_OK;}
                                break;
                            }
                            uint8_t timeout = 10;
                            while((dataSuccess == SEVER_SEND_ERROR)&&(timeout > 0 )){
                                unSetConFlag(SERVER_CONNECTION);
                                dataSuccess = sendDataThroughputMode(0, 0, 1);
                                timeout--;
                                for(uint8_t y = 0; y < 5; y++){
                                    delay_2ms(200);
                                    if(updateConfigFromResponse()==0){continue;}
                                    else{setConFlag(SERVER_CONNECTION);}
                                    break;
                                }
                            }
                            
                            //Get WiFi singal strength
                            for(uint8_t y = 0; y < 10; y++){
                                if(setCommandMode()==1){break;}
                                delay_2ms(200);
                            }
                            int strength = getSignalStrengthSTA();
                            if(strength > 0){
                                saveWifiStrengthToEeprom(strength);
                                reqLinkStatus();
                            }else{
                                saveWifiStrengthToEeprom(255);
                            }

                            break;
                        }else{
                            ClrWdt();
                            U2SendArray("Prepering to send more\n");
                            U1ClearErrors();
                            delay_2ms(100);
                            U1ClearErrors();
                            U2SendArray("Sending more\n");
                        }
                    }else if(dataSuccess == DATASEND_NOLINK){
                        U2SendArray("NOLINK\n");
                        delay_2ms(noLinkDelay);
                        tryes--;
                        setError(DATASEND_NOLINK2);
                        clearSerialBuffer();
                    }else if(dataSuccess == DATASEND_ERROR){  //Uneaxplainable error
                       U2SendArray("DATASEND ERROR\n");
                       tryes--;
                       setError(DATASEND_TIMEOUT_ERROR_FLAG);
                       clearSerialBuffer();
                    }else{
                       tryes--;
                    }
                }
                turnOFFWifiModule();
                RTCsetTimer(50, 1);
                state = END_OF_TRANSMITION;
                break;
//</editor-fold>
            case END_OF_TRANSMITION: //Clear sent number
// <editor-fold defaultstate="collapsed">                
#ifdef DEBUG_PRINT_STATES
                U2SendArray("State END_OF_TRANSMITION reached\n");
#endif
                NOP();
                uint8_t errors = getError();

                if(!(errors & DATASEND_TIMEOUT_ERROR_FLAG) && !(errors & DATASEND_NOLINK2)){//If send was ok
                    FLASHclear();
                    saveTimeDiff(timeOffseet);
                    saveSendErrors();
                    saveSENSORStoEEPROM();
                    updateNextSendTime((uint32_t)EEPROM_laod16Bit(SEND_INTERVAL_ADRESS)*60);
                    initPoweronVariables();  //Restart all error
                    if(getLogFlags() == TRANSPORT_LOGGING){EEPROM_save8Bit(SPETIAL_LOG_FLAGS, 0);}
                }else{  //Data was not sent ok
                    saveSendErrors();
                    U2SendArray("Problems with sending data\n");
                    unSetConFlag(SERVER_CONNECTION);
#ifdef DEBUG_PRINT_EXTENDED
                    sprintf(Debugbuf,"Mi:%d/%d,Ma:%d/%d,O:%d/%d\n", EEPROM_laod8Bit(MINOR_SEND_TIMEOUT_ADRESS), DEFAULT_minorSendTimeoutNumber, EEPROM_laod8Bit(MAJOR_SEND_TIMEOUT_ADRESS),DEFAULT_majorSendTimeoutNumber, EEPROM_laod8Bit(OVERALL_SEND_TIMEOUT_ADRESS), DEFAULT_overallSendTimeoutNumber);
                    U2SendArray(Debugbuf);
#endif
                }
                state = TEST_USER_PUSH_BEFORE_SLEEP;
                break;
//</editor-fold>
//************************************************************************************************//
//************************************* SLEEPING *************************************************//
//************************************************************************************************//
            case TEST_USER_PUSH_BEFORE_SLEEP: //Set sleep parameters in Power RTC
// <editor-fold defaultstate="collapsed">                
#ifdef DEBUG_PRINT
                U2SendArray("State TEST_USER_PUSH_BEFORE_SLEEP reached\n");
#endif
                NOP();
                uint8_t userPush = userPushType();
                if(userPush == LONG_PUSH){state = SMART_LINK;}
                else{state = TEST_LOGFLAG;}                
                break;
//</editor-fold>
            case TEST_LOGFLAG: //Nothing yet
// <editor-fold defaultstate="collapsed">
#ifdef DEBUG_PRINT
                U2SendArray("State TEST_LOGFLAG reached\n");
#endif
                NOP();
                uint8_t logFlags = getLogFlags();
                if(logFlags == NO_LOGGING){state = SLEEP_USER_PUSH;}
                else{state = SLEEP_TIMER;}
                
                if(logFlags == TRANSPORT_LOGGING){if(EEPROM_laod8Bit(SPETIAL_LOG_FLAGS) == 1){EEPROM_save8Bit(SPETIAL_LOG_FLAGS, 2);}}
                
                break;
//</editor-fold>                
            case SLEEP_TIMER: //Sleep with timer and user
// <editor-fold defaultstate="collapsed">                
#ifdef DEBUG_PRINT_STATES
                U2SendArray("State SLEEP_TIMER reached\n");
#endif
                NOP();
                if(LATCbits.LATC5 == 1){
                    TRISCbits.RC5 = 0;
                    LATCbits.LATC5 = 0;
                    delay_2ms(50);
                    saveBatteryStatuse();
                }else{
                    TRISCbits.RC5 = 0;
                    LATCbits.LATC5 = 0;
                }
                
                uint16_t sleepSec = 0xffff;// Init sleepSec ver
                //Test for alarm state to measure faster
                if(sensorsArray.Flags & (HIGH_ALARM_FLAG | LOW_ALARM_FLAG | HIGH_FLAG | LOW_FLAG)){
                    //Load sleeping sec when in alarm state
                    sleepSec = EEPROM_laod16Bit(MEASURMENT_INTERVAL_FAST_ADRESS);
                    if(sleepSec == 0xffff || sleepSec < 10){
                        sleepSec = FAST_MEASURE_INTERVAL_DEFAULT;
                        EEPROM_save16Bit(MEASURMENT_INTERVAL_FAST_ADRESS, FAST_MEASURE_INTERVAL_DEFAULT);
                    }
                }else{
                    //Load sleeping sec when in normal state
                    sleepSec = EEPROM_laod16Bit(MEASURMENT_INTERVAL_SEC_ADRESS);
                    if(sleepSec == 0xffff || sleepSec < 10){
                        sleepSec = MEASURE_INTERVAL_DEFAULT;
                        EEPROM_save16Bit(MEASURMENT_INTERVAL_SEC_ADRESS, MEASURE_INTERVAL_DEFAULT);
                    }
                }
                RTCsetTimer(sleepSec, 0);
                
                timeout = 10;
                while(!RTCEnterOffMode(USER_INT | TIMER_INT) && timeout > 0){
                    timeout--;
                    if(!initRTC()){U2SendArray("RTC init bad\n");}
                    U2SendArray("RTC off mode bad\n");
                    RTCsetTimer(sleepSec, 0);
                }
                state = GOING_TO_SLEEP;
                
                break;
//</editor-fold>
            case SLEEP_USER_PUSH: //Sleep with only user wake up
// <editor-fold defaultstate="collapsed">                
#ifdef DEBUG_PRINT_STATES
                U2SendArray("State SLEEP_USER_PUSH reached\n");
#endif
                NOP();
                timeout = 10;
                while(!RTCEnterOffMode(USER_INT) && timeout > 0){
                    timeout--;
                    if(!initRTC()){U2SendArray("RTC init bad\n");}
                    U2SendArray("RTC off mode bad\n");
                }
                state = GOING_TO_SLEEP;
                break;
//</editor-fold>
            case GOING_TO_SLEEP: //sleep mode / off mode
// <editor-fold defaultstate="collapsed">                
#ifdef DEBUG_PRINT_STATES
                U2SendArray("State GOING_TO_SLEEP reached\n");
#endif
                if(getError() & RTC_ERROR_FLAG){unSetError(RTC_ERROR_FLAG);}
                delay_2ms(1000);
#ifdef DEBUG_PRINT_EXTENDED
                U2SendArray("RTC OFF not effective!\n"); //If the device is not off here there is an error
#endif
                if(!initRTC()){U2SendArray("RTC init bad\n");}
                setError(RTC_ERROR_FLAG);
                state = NOTHING; //Go to the start if some thing is wrong
                break;
            default:
                state = NOTHING; //Go to the start if some thing is wrong
                break;
//</editor-fold>
        }
    }
}
//</editor-fold>