#ifndef PWM_H
#define	PWM_H

#include "main.h"

void PWMinit();
void PWM1set(uint16_t input);
void PWM2set(uint16_t input);

#endif
