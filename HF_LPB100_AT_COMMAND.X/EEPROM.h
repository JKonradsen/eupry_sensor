#ifndef EEPROM_H
#define	EEPROM_H

#include "main.h"

uint8_t ReadEEByte(uint16_t address);
uint8_t WriteEEByte(uint16_t address, uint8_t data);

#endif

