#include "wifi.h"
#include "sensor.h"

#ifndef PRODUCTION_DEVICE
#if USE_ENCRYPTION == 0
    #ifdef DEVICE_1
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v2/store/1/ek8MER86TlaHyflWArS4eFsEMrLvtudc HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_2
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v2/store/2/HcDMfdDKRZDDJ12YWWRIoyrQoqS2pJK4 HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_3
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v2/store/3/kIB6iRknNIEzYp1ifWovdxgqmOXVifwH HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_4
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v2/store/4/8l3FCslSZFWqXaSOctfPECHJ4Pe4ub25 HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_5
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v2/store/5/gML3S9sbaSK5OrYm6k4AYDLeiP4S3E8j HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_6
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v2/store/6/5kEQ6CY8slB9jXlO5dIZ8hjs5XLyOeyv HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_7
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v2/store/7/ZEwaSemmNB0gLpViR6kLwOpshEq5POdv HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_8
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v2/store/8/7j6nyy2zDoLRaNKQnismd1nzI3S5QesO HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_9
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v2/store/9/Q9S9qQqBd3e6qxrh2LbDSn97WHBFMWKR HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_10
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v2/store/10/IlD0SU0fSAflFQQUbljRXe4a8jh89SQc HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_11
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v2/store/11/8A8MyIJ59SmvCSYsCq9CH1WXMzUN6jEP HTTP/1.1\r\n";
    #endif
#endif
#if USE_ENCRYPTION == 1
    #ifdef DEVICE_0
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/0/00000000000000000000000000000000 HTTP/1.1\r\n";
    #endif    
    #ifdef DEVICE_1
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/1/ek8MER86TlaHyflWArS4eFsEMrLvtudc HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_2
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/2/HcDMfdDKRZDDJ12YWWRIoyrQoqS2pJK4 HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_3
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/3/kIB6iRknNIEzYp1ifWovdxgqmOXVifwH HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_4
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/4/8l3FCslSZFWqXaSOctfPECHJ4Pe4ub25 HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_5
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/5/gML3S9sbaSK5OrYm6k4AYDLeiP4S3E8j HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_6
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/6/5kEQ6CY8slB9jXlO5dIZ8hjs5XLyOeyv HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_7
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/7/ZEwaSemmNB0gLpViR6kLwOpshEq5POdv HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_8
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/8/7j6nyy2zDoLRaNKQnismd1nzI3S5QesO HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_9
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/9/Q9S9qQqBd3e6qxrh2LbDSn97WHBFMWKR HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_10
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/10/IlD0SU0fSAflFQQUbljRXe4a8jh89SQc HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_11
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/11/8A8MyIJ59SmvCSYsCq9CH1WXMzUN6jEP HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_12
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/12/gFAz8qv86XnvKdW1ePsebNsadXi2wZ1R HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_13
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/13/ukOAfP2Of4KezCkIyub2YFWuTGhM12YI HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_14
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/14/Xm4najzx66VQKMtQWeFOtqJb5ZZ1yMQR HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_15
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/15/TGvrSPuGD9Lqj1maOXevoxyQjJSEhBi6 HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_16
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/16/gzn1G64FUOu3ufHky4qBtWzBkbgP1sRc HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_17
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/17/kYLv2OAAwwZ1EEBcKyY7nrryL2AYQpUA HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_18
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/18/68ijxACfsA1b3EsoUfVfXjBM4OONZGhs HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_19
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/19/E30M8GRszGQk4rC6ZAKWRI8QfihKhBw6 HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_20
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/20/Th2Kg1Q5cB3rkFItZk35MDXdnjsbzzsN HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_21
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/21/zdQAarpHQj922HeIo7gf3ImvT3foViVu HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_22
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/22/uMx5I5kn3UHJC3ilixb1YhfsuvAPdlU1 HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_23
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/23/PymhZndVF14wQ1raaTmXCvRd6ZlCE4SU HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_24
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/24/lRy3xUnfA4czsYVcJ2SrH9MdA6Q3SjCR HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_25
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/25/viPZ7p42vdlf7jFWF2T70cLy7QS2fPjq HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_26
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/26/iMdBfNLUKDbTN6srzYMm9tUv5N9vYswG HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_27
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/27/OF7ykksgMMkWwt7VREaKpDAyDdcWfNqU HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_28
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/28/V8Iu0YrKInJG6U4SAWxxAgvKG4W2bt7v HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_29
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/29/7QmJSJXd0nD5CRtH05Cz5Rmk4qLbWDgP HTTP/1.1\r\n";
    #endif
    #ifdef DEVICE_30
    const char ConstChar_HTTP_HEADER_POST[] = "POST /d/v3/save/30/S6exPZVRjYotvHh5eplpaxhdTOmD2JcC HTTP/1.1\r\n";
    #endif

#endif
#endif

char STAauth[] = "WPA2PSK";
char STAencry[] = "AES";
char STAkey[] = "g5b+yaea_cxuG^s";//"ballgame";//"EUPRYTEST";//"4gINTERNET@Eupry!";//"PASSWORD";//
char SSID_NAME[] = "JJPS!xBSZ99^oth";//"4G Wi-Fi 3Danmark-53DF";//"TEST_WIFI";//"Hansi";//"EUPRY_TEST_WIFI";//

//Server config
const char reqType[] = "POST";
const char headerPath[] = "/d/v2/debug";//"/";//
const char connectionArea[] = "close";
const char userAgent[] = "HF_LPB_100_TEST";

const char RESPONSE[] = "RESPONSE:";

//Send parameters
char dataSendTimeout = 3;
char noLinkDelay = 10;
char maximumSendNumber = 10;
uint8_t wifiOnFlag = 0;

char startupWifi(int timeout){  //start up the wifi module and set in command mode
    //Turn on chip
    int i = 0;
    clearSerialBuffer();
    if(!reqMode()){
        while(!setCommandMode() && (i < (timeout))){
            U1ClearErrors();
            i++;
        }
        if(i >= timeout){  //Error due to timeout
            return 0;
        }
    }
    return 1;
}
char initAPI(){  //Sets up the modem for data with applied server paths
    //clrwdt();
    if(!startupWifi(4)){
        resetDevice();
        if(!startupWifi(5)){
            return 0;
        }
    }
    echo(1);
    if(!setWorkMode(2)){return 0;}
    if(!setHTTPURL(URL, PORT)){return 0;}
    if(!setHTTPReqType(reqType)){return 0;}
    if(!setHTTPHeaderPath(headerPath)){return 0;}
    if(!setHTTPConnectionArea(connectionArea)){return 0;}
    if(!setHTTPUserAgent(userAgent)){return 0;}
    return 1;
}
char sendData(const char data[], uint32_t index){
    char dataSend = setHTTPSendData(data, index);
    
    if(dataSend == 1){
        return DATASEND_OK;
    }else if(dataSend == 2){
        return DATASEND_SENDERROR;
    } else if(NLINK){
        return DATASEND_NOLINK;
    }else{
        if(compareStrings("Connet error", U1buf)){
            return DATASEND_CONNECTERROR;
        }else{
            return DATASEND_ERROR;
        }
    }
    return 0; 
}
char sendDataFlash(){
    if(setHTTPSendData(NULL, 0)){
        return DATASEND_OK;
    }else if(NLINK){
        return DATASEND_NOLINK;
    }else{
        return DATASEND_ERROR;
    }
}
void restartWifi(){
    NRESET = 0;
    __delay_ms(20);
    NRESET = 1;
}
char isConnected(){
    if(!NLINK){
        return 1;
    }else{
        return 0;
    }
}
char connectSmartLink(){  //0 = SmartLink initiate failure, 1 = Success, 2 = Timeout failure
    int time = 0;
    int timeout = 100;
    setCommandMode();
    if(!setSmartLink()){
        return 0; //SmartLink init failure
    }
    delay_2ms(30);
    while(time < timeout){
        if(!NREADY){
            __delay_ms(10);
            if(!NREADY){   //Extra safety
                return 1;  //Success
            }
        }
        delay_2ms(130);
        time++;
    }
    restartWifi();
    return 2; //Timeout failure
}
/*
 * Here follows all functions which connect directly to main.c
 */
char initWifiModule(){
    int i;
    for(i = 0; i < 10; i++){
        if(initAPI()){
            return 1;
        }
    }
    return 0;
}
char gotWifiCredentials(){
#ifdef BORN_WITH_WIFI_CRED
    return 1;
#else
    return 0;
#endif
}
char initWifiCredentials(){
    clearSerialBuffer();
    if(!setSTAKey(STAauth, STAencry, STAkey)){return 0;}
    if(!setSTASSID(SSID_NAME)){return 0;}
    if(!setNETP()){return 0;}
    //restartWifi(); 
    resetDevice();
    return 1;
}
void turnOFFWifiModule(){

    //Terminate any connection with server
    U1init();
    U1ClearErrors();
    clearSerialBuffer();

    U1SendArray("\r\r\r\n");
    delay_2ms(100);
    
    //Set all pins asusiated with wifi to input and there value to 0
    LATA = 0;
    TRISAbits.RA4 = 1; //RTS from wifi , 0 = output, 1 = input
    TRISAbits.RA3 = 1; //CTS from wifi
    TRISAbits.RA0 = 1; //NRESET
    TRISAbits.RA1 = 1; //NREADY
    TRISAbits.RA2 = 1; //NLINK
    
    //globalSend = 0;
    
    TRISBbits.RB3 = 0; //Wifi control Power

//    //Turn off DCDC convertor
//    LATCbits.LATC5 = 0;
//    TRISCbits.RC5 = 1;
//    delay_2ms(100);
//    
//#ifdef DEBUG_PRINT    
//    U2SendArray("DCDC off\n");
//#endif    
    
// #ifdef hardware_version_V200
    WIFI_S = 1;
// #endif
// #ifdef hardware_version_V110
    // WIFI_S = 0;
// #endif
    
#ifdef DEBUG_PRINT    
    U2SendArray("WIFI off\n");
#endif        
    wifiOnFlag = 0;
    //return;
}
void turnONWifiModule(){
    if(wifiOnFlag == 1){return;}
    if(LATCbits.LATC5 == 0){
        if(saveBatteryStatuse() < LOW_VOLTAGE){
            //Set all pins asusiated with wifi to input and there value to 0 before DCDC is on
            TRISAbits.RA4 = 1; //RTS from wifi , 0 = output, 1 = input
            TRISAbits.RA3 = 1; //CTS from wifi
            TRISAbits.RA0 = 1; //NRESET
            TRISAbits.RA1 = 1; //NREADY
            TRISAbits.RA2 = 1; //NLINK
            LATA = 0;

            //Aslo UART pin to input and low
            TRISCbits.RC6 = 1;
            LATCbits.LATC6 = 0;
            TRISCbits.RC7 = 1;
            LATCbits.LATC7 = 0;
            delay_2ms(20);

            //After all pins are ready turn on DCDC
            TRISCbits.RC5 = 0;
            LATCbits.LATC5 = 1;
            delay_2ms(100);
        }
    }
    
    TRISBbits.RB3 = 0; //Wifi control Power
// #ifdef hardware_version_V200
    WIFI_S = 0;
// #endif
// #ifdef hardware_version_V110
    // WIFI_S = 1;
// #endif
    delay_2ms(250);
    U2SendArray("Wifi on\n");
   
    wifiOnFlag = 1;
    
    //WiFi
    TRISAbits.RA3 = 1; //RTS from wifi , 0 = output, 1 = input
    TRISAbits.RA4 = 0; //CTS from wifi
    TRISAbits.RA0 = 0; //NRESET
    TRISAbits.RA1 = 1; //NREADY
    TRISAbits.RA2 = 1; //NLINK
    
    delay_2ms(10);
    CTS = 0; //CTS low at all times
    NRESET = 1; //Reset
    U1init();
    U1ClearErrors();
    clearSerialBuffer();
}
void startupWifiModule(){
    turnONWifiModule();
    U1ClearErrors();
    if(!reqMode()){
        while(NREADY);
        startupWifi(10);
        U1Send('\r');
        delay_2ms(1);
    }
}
char sendDataThroughputMode(long index, long length, uint8_t dataPadding){

    if((index < 0)||(length < 0)){return 0;}
    //TODO: find out why the data 1st send is not working, maybe due to noise on the UART

    delay_2ms(10);
    
    U1ClearErrors();
    clearSerialBuffer();
    
//    uint32_t deviceNumber = 0;
//    clearSerialBuffer();
//    for(uint8_t y = 0; y < 8; y++){
//        deviceNumber *= 10;
//        deviceNumber += (EEPROM_laod8Bit(y + DEVICE_NUMBER_ADDRES)-48);
//    }
//    if(deviceNumber < 2000000000){
//        sprintf(Debugbuf,"POST /d/v3/save/%lu/", deviceNumber);
//        U1SendArray(Debugbuf);
//        for(uint8_t y = 0; y < 32; y++){U1Send(EEPROM_laod8Bit(y + API_ADDRES));}
//        U1SendArray(" HTTP/1.1\r\n");
//    }else{
        U1SendArray(ConstChar_HTTP_HEADER_POST);
//    }

    U1SendArray("Connection: close\r\nUser-Agent: HF_LPB_100_TEST\r\nHost: api.eupry.com\r\n");//Host information
    if(length > 0){
        if(USE_ENCRYPTION){
            U1SendArray("Content-Type: multipart/form-data; boundary=-----------------------------KROLLEBOLLE\r\n");

            sprintf(Debugbuf,"Content-Length: %lu\r\n\r\n", length+201);//Content length
            U1SendArray(Debugbuf);

        }else{
            sprintf(Debugbuf,"Content-Length: %lu\r\n\r\n", length);//Content length
            U1SendArray(Debugbuf);
        }


        if(USE_ENCRYPTION){
            U1SendArray("-------------------------------KROLLEBOLLE\r\n");
            U1SendArray("Content-Disposition: form-data; name=\"DATA\"; filename=\"data.bin\"\r\nContent-Type: application/octet-stream\r\n\r\n");
        }
        
        if(!dataPadding){
            index += length - FLASHStreamUpToToU1(index, length, USE_ENCRYPTION);
        }else{
            FLASHStreamUpToToU1(index, length, USE_ENCRYPTION);
            index += length;
        }

        if(USE_ENCRYPTION){
            U1SendArray("\r\n-------------------------------KROLLEBOLLE--\r\n");
        }

    }else{
        sprintf(Debugbuf,"Content-Length: %d\r\n\r\n", 10);//Content length
        U1SendArray(Debugbuf);
        U1SendArray("0123456789");
    }

    delay_2ms(1);
    U1Send(13); //Send end characters
    U1Send(10);
    U1Send(13);
    U1Send(10);
    U1ClearErrors();
    clearSerialBuffer();
    //if(serialEventWait(200, "UNIXTIME")){
    if(serialEventWait(100, "RESPONSE")){
        EEPROM_save32Bit(SEND_INDEX_ADRESS, index);
        return DATASEND_OK;
    }else if(NLINK){
        return DATASEND_NOLINK;
    }else{
        return DATASEND_ERROR;
    } 
}
char updateConfigFromResponse(){
    uint8_t checkSumB = 0;
    
    for(uint8_t i = 0; i < 100; i++){Debugbuf[i] = 0; }

    char *point = strstr(U1buf, RESPONSE);
    for(uint8_t i = 0; i < 64; i++){Debugbuf[i] = *point++; }
    Debugbuf[64] = 0;
#ifdef DEBUG_PRINT
    U2SendArray(Debugbuf);
#endif    
    checkSumB = *(strstr(Debugbuf, "|")+1);
    if(!strstr(Debugbuf, "|")){
#ifdef DEBUG_PRINT_EXTENDED
        U2SendArray("BC!\n");
#endif
        return 0;
    }else{
#ifdef DEBUG_PRINT
        U2SendArray("OK!\n");
#endif        
    }
        
    uint8_t checkSum = Fletcher8(Debugbuf, '|');

    if(checkSum != checkSumB){
#ifdef DEBUG_PRINT_EXTENDED      
        sprintf(Debugbuf2,"CS: %d %d\n", checkSum, checkSumB);
        U2SendArray(Debugbuf2);
#endif        
        return 0;
    }else{
#ifdef DEBUG_PRINT
        U2SendArray("OK!\n");
#endif        
    }
    
    long serverTime = 0;
    float temp = 0;
    uint8_t minus = 0;
    char *point = Debugbuf;
    
    //Get to after "RESPONCE:"
    while(*point++ != ':');
    
    //UNIXTIME
    while(*point != ';'){
        serverTime *= 10;
        serverTime += ((*point++) - 48);
    }
    long deviceTime = RTCgetUnixTime();
    timeOffseet = serverTime - deviceTime;
    setTimeFromUnixTime(serverTime);
    
    //ALARM HIGH
    *point++;
    while(*point != ','){
        if(*point == '-'){minus = 1;}
        else{
            temp *= 10;
            temp += (*point - 48);
        }
        *point++;
    }
    if(minus){temp /= -10;}
    else{temp /= 10;}
    minus = 0;
    sensorsArray.AlarmHigh = conToRawTemp(temp);
    temp = 0;
    
    //ALARM LOW
    *point++;
    while(*point != ','){
        if(*point == '-'){minus = 1;}
        else{
            temp *= 10;
            temp += (*point - 48);
        }
        *point++;
    }
    if(minus){temp /= -10;}
    else{temp /= 10;}
    minus = 0;
    sensorsArray.AlarmLow = conToRawTemp(temp);
    temp = 0;
    
    //ALARM CRIT HIGH
    *point++;
    while(*point != ','){
        if(*point == '-'){minus = 1;}
        else{
            temp *= 10;
            temp += (*point - 48);
        }
        *point++;
    }
    if(minus){temp /= -10;}
    else{temp /= 10;}
    minus = 0;
    sensorsArray.AlarmCritHigh = conToRawTemp(temp);
    temp = 0;
    
    //ALARM CRIT LOW
    *point++;
    while(*point != ','){
        if(*point == '-'){minus = 1;}
        else{
            temp *= 10;
            temp += (*point - 48);
        }
        *point++;
    }
    if(minus){temp /= -10;}
    else{temp /= 10;}
    minus = 0;
    sensorsArray.AlarmCritLow = conToRawTemp(temp);
    temp = 0;
    
    //MEASURMENT INTERVAL STANDART
    *point++;
    while(*point != ','){
        temp *= 10;
        temp += ((*point++) - 48);
    }
    EEPROM_save16Bit(MEASURMENT_INTERVAL_SEC_ADRESS, (uint16_t)temp);
    temp = 0;
    
    //MEASURMENT INTERVAL FAST
    *point++;
    while(*point != ','){
        temp *= 10;
        temp += ((*point++) - 48);
    }
    EEPROM_save16Bit(MEASURMENT_INTERVAL_FAST_ADRESS, (uint16_t)temp);
    temp = 0;
    
    //SEND INTERVAL STANDART
    *point++;
    while(*point != ','){
        temp *= 10;
        temp += ((*point++) - 48);
    }        
    EEPROM_save16Bit(SEND_INTERVAL_ADRESS, (uint16_t)temp);
    temp = 0;
    
    //SEND INTERVAL FAST
    *point++;
    while(*point != ','){
        temp *= 10;
        temp += ((*point++) - 48);
    }
    EEPROM_save8Bit(FAST_SEND_INTERVAL_ADDRES, (uint8_t)temp);
    temp = 0;
    
    //LOGGING FLAG
    *point++;
    unSetLogFlag(0xff);
    setLogFlag(((*point)-48)/*&LOG_FLAG*/);
    
#ifdef DEBUG_PRINT_EXTENDED
        sprintf(Debugbuf,"C:%2.1f %2.1f %2.1f %2.1f\n", getTemp(sensorsArray.AlarmHigh), getTemp(sensorsArray.AlarmLow), getTemp(sensorsArray.AlarmCritHigh), getTemp(sensorsArray.AlarmCritLow));
        U2SendArray(Debugbuf);
        sprintf(Debugbuf,"LF:%d\n", getLogFlags());
        U2SendArray(Debugbuf);
        sprintf(Debugbuf,"NM:%d FM:%d NS:%d FS:%d\n", 
                EEPROM_laod16Bit(MEASURMENT_INTERVAL_SEC_ADRESS), 
                EEPROM_laod16Bit(MEASURMENT_INTERVAL_FAST_ADRESS), 
                EEPROM_laod16Bit(SEND_INTERVAL_ADRESS), 
                EEPROM_laod8Bit(FAST_SEND_INTERVAL_ADDRES)
                
                );
        U2SendArray(Debugbuf);
#endif

    return 1;
}
void saveWifiStrengthToEeprom(int strength){
    EEPROM_save8Bit(WIFI_STRENGTH_ADRESS, strength);
}
int getSignalStrengthFromEeprom(){
    return EEPROM_laod8Bit(WIFI_STRENGTH_ADRESS);
}