/* 
 * File:   testProgram.h
 * Author: jakobkonradsen
 *
 * Created on 14. april 2015, 09:41
 */

#ifndef TESTPROGRAM_H
#define	TESTPROGRAM_H

#include "main.h"

void testProgram();
void clearU2Buf();
void runConnectionTest();
void runGeneralTest();
void runBurst(char num);

#endif	/* TESTPROGRAM_H */

