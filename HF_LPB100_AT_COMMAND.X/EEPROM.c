#include "EEPROM.h"

uint8_t ReadEEByte(uint16_t address){
    EEADRH = (address&0xff00)>>8;
    EEADR = address&0x00ff; // load address of EEPROM to read
    EECON1bits.EEPGD = 0; // access EEPROM data memory
    EECON1bits.CFGS =0; // do not access configuration registers
    EECON1bits.RD = 1; // initiate read 
    return EEDATA; // return EEPROM byte
}

// Write Byte to internal EEPROM
uint8_t WriteEEByte(uint16_t address, uint8_t data){
    EECON1bits.WREN=1; // allow EEPROM writes
    EEADRH = (address&0xff00)>>8;
    EEADR = address&0x00ff; // load address of EEPROM to read
    EEDATA=data; // load data to write to EEPROM
    EECON1bits.EEPGD=0;// access EEPROM data memory
    EECON1bits.CFGS=0; // do not access configuration registers
    INTCONbits.GIE=0; // disable interrupts for critical EEPROM write sequence
    //===============//
    EECON2=0x55;
    EECON2=0xAA;
    EECON1bits.WR=1;
    //==============//
    INTCONbits.GIE=1; // enable interrupts, critical sequence complete
    while (EECON1bits.WR==1); // wait for write to complete
    EECON1bits.WREN=0; // do not allow EEPROM writes

    //Verify write operation
    if (ReadEEByte(address)==data) // read the byte we just wrote to EEPROM
    return 1; // write was successful
    else
    return 0; // write failed
}
