#include "rtc.h"

uint8_t volatile RTCbuseFlag = 0;

void getTimeStempAsString(char *buffer){
    RTCbuseFlag = 1;
    sprintf(buffer,"{\"%lu\":",AB18GetUnixTime());
    if(RTCbuseFlag == 2){INTuserPush(0);}
    RTCbuseFlag = 0;
}
uint32_t RTCgetUnixTime(){
    RTCbuseFlag = 1;
    uint32_t output = AB18GetUnixTime();
    if(RTCbuseFlag == 2){INTuserPush(0);}
    RTCbuseFlag = 0;
    return output;
}
void setTimeFromUnixTime(uint32_t unixTime){
    RTCbuseFlag = 1;
    AB18SetUnixTime(unixTime);
    if(RTCbuseFlag == 2){INTuserPush(0);}
    RTCbuseFlag = 0;
}
void RTCsetTimer(uint16_t sec, uint8_t force){
    RTCbuseFlag = 1;
    AB18SetTimer(sec, force);
    if(RTCbuseFlag == 2){INTuserPush(0);}
    RTCbuseFlag = 0;
}
uint8_t initRTC(){
    RTCbuseFlag = 1;
    uint32_t tempTime = RTCgetUnixTime();
    RTCreset();
    uint8_t output =  AB18Init();
    setTimeFromUnixTime(tempTime);
    U2SendArray("Setting SUPERCAP\n");
    AB18SetSCBackUp();
    if(RTCbuseFlag == 2){INTuserPush(0);}
    RTCbuseFlag = 0;
    return output;
}
uint8_t RTCEnterOffMode(uint8_t interrupts){
    RTCbuseFlag = 1;
    AB18EnableInterrupt(interrupts);
    uint8_t output = AB18Sleep();
    if(RTCbuseFlag == 2){INTuserPush(0);}
    RTCbuseFlag = 0;
    return output;
}

uint8_t RTCgetInts(){
    return AB18GetInterrupt();
}
void RTCDisableInterrupt(){
    AB18DisableInterrupt();
}
void RTCEnableInterrupt(uint8_t interrupts){
    AB18EnableInterrupt(interrupts);
}
uint8_t RTCgetButtonLevel(){
    return AB18getButtonLevel();
}
uint8_t RTCreturnIntettupts(){
    return AB18getInterrupts();
}
void RTCreset(){
    U2SendArray("R\n");
    delay_2ms(10);
    AB18SWReaset();
    delay_2ms(10);
}