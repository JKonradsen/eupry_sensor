#ifndef SENSOR_H
#define	SENSOR_H

#include "main.h"
#include "DS18B20.h"
#include "SI70.h"

#ifdef sensorONOFF
    #define SENS_SWITCH_PORT LATBbits.LB4
    #define SENS_SWITCH_TRIS TRISBbits.RB4
#endif

//#define HUMID_AND_TEMP
#define TEMP_ONLY
//#define AVRIG_AND_SLOP

#define HIGH_ALARM_FLAG 1
#define LOW_ALARM_FLAG 2
#define HIGH_FLAG 4
#define LOW_FLAG 8
#define ALARM_SEND 16
#define NOT_ALARM_SEND 32
#define NEED_TO_SEND 64

#define BAD_SENSOR_READING 250

typedef struct{
    uint8_t Flags, grayZoonTimer; //Alarm, Alarm type,
    uint16_t Current, Max, Min, Avrige;
    uint16_t AlarmHigh, AlarmLow, AlarmCritHigh, AlarmCritLow;
    float Slope;
}SENSORS;

extern SENSORS sensorsArray;

// Get all sensors a a string
uint8_t getSensorsAsString(char *buffer);
// Calculate and save slop and avrige
void calculateSlopAndAvrige();
//Update sensorArray and save it
void updateSensorArray(uint16_t rawTemp);
// reset the global sensor ver
void resetSENSORS();
//Load global sensor var from EEPROM
void loadSENSORS();
//Save global sensor to EEPROM
void saveSENSORStoEEPROM();
// Set MOSFET off
void turnOFFSensors();
// Set MOSFET on
void turnONSensors();
// init Sensor and see that thay are working
uint8_t initSensors();
//Save Battery value
uint16_t saveBatteryStatuse();
//Save a measurment point
uint8_t saveMeasuremntPoint();
//Save last measured wifi strength
void saveWifiStrength();
//Save time diff from servers
void saveTimeDiff(int timeDiff);
//Save push event
void savePushEvent();
//Save minor, major and overall verb
void saveSendErrors();

void saveSensorFlags();

void saveWDTevent(uint8_t Akn);
//Save what ever input
void saveGeneral(const char *input);

#endif

