#ifndef LEDS_H
#define	LEDS_H

#include "main.h"
#include "PWM.h"

#define LED_DELAY 10

#define OFF_OFF 0
#define OFF_GREEN 1
#define OFF_YELLOW 2
#define OFF_RED 3
#define GREEN_OFF 4
#define GREEN_GREEN 5
#define GREEN_YELLOW 6
#define GREEN_RED 7
#define YELLOW_OFF 8
#define YELLOW_GREEN 9
#define YELLOW_YELLOW 10
#define YELLOW_RED 11
#define RED_OFF 12
#define RED_GREEN 13
#define RED_YELLOW 14
#define RED_RED 15

#define YELLOW_LED_TRIS TRISCbits.RC2
#define GREEN_LED_TRIS TRISCbits.RC1
#define RED_LED_TRIS TRISBbits.RB4

#define YELLOW_LED_LAT LATCbits.LATC2
#define GREEN_LED_LAT LATCbits.LATC1
#define RED_LED_LAT LATBbits.LATB4

#define YELLOW_LED_PORT PORTCbits.RC2
#define GREEN_LED_PORT PORTCbits.RC1
#define RED_LED_PORT PORTBbits.RB4

#define RIGHT_LED_TRIS TRISBbits.RB6
#define LEFT_LED_TRIS TRISBbits.RB7
#define RIGHT_LED_LAT LATBbits.LATB6
#define LEFT_LED_LAT LATBbits.LATB7

void lightALED(uint16_t red, uint16_t yellow, uint16_t green, uint8_t side);
void lightLEDs(uint8_t input, uint8_t delay, uint8_t DCDC);

#endif

