/* 
 * File:   wifi.h
 * Author: jakobkonradsen
 *
 * Created on 5. marts 2015, 17:05
 */

#ifndef WIFI_H
#define	WIFI_H

#include "main.h"
#include "HFLBP100_AT.h"

//IO from Wifi module
//#define NRESET PORTAbits.RA2
//#define LINK PORTAbits.RA0
//#define READY PORTAbits.RA1

//Define if the module is made with premade wifi credentials
#define WIFICRED    

//Wifi Module port bits
// #if defined(hardware_version_V110) || defined(hardware_version_V200)
#define NRESET LATAbits.LA0 //LATAbits.LA2 
#define NREADY PORTAbits.RA1 //PORTAbits.RA3
#define NLINK PORTAbits.RA2 //PORTAbits.RA4
#define CTS PORTAbits.RA4 //PORTAbits.RA1
#define RTS PORTAbits.RA3 //PORTAbits.RA0
#define WIFI_S PORTBbits.RB3
// #endif

// #ifdef hardware_version_V100
//Wifi Module port bits, old hardware version
// #define RTS PORTAbits.RA0
// #define CTS PORTAbits.RA1
// #define NRESET LATAbits.LA2
// #define NREADY PORTAbits.RA3
// #define NLINK PORTAbits.RA4
// #define WIFI_S PORTBbits.RB3
// #endif

#define URL "api.eupry.com"
#define PORT "80"

#define MAX_SEND_LENGTH 32768
#define MIN_SEND_LENGTH 8192

//Born wifi credentials
#define BORN_WITH_WIFI_CRED

//Send data error messages
#define DATASEND_OK 2
#define DATASEND_NOLINK 3
#define DATASEND_ERROR 4  //General unexplainable error
#define DATASEND_CONNECTERROR 5
#define DATASEND_SENDERROR 6

//Send parameters
extern char dataSendTimeout;
extern char noLinkDelay;
extern char maximumSendNumber;

extern uint8_t wifiOnFlag;

uint8_t getDataFromBackEnd();

char startupWifi(int timeout);
char sendData(const char data[], uint32_t index);
char initAPI();
void restartWifi();
char isConnected();

//Functions directly linked to main.c
char initWifiModule();
char gotWifiCredentials();
char initWifiCredentials();
char sendDataFlash();
void startupWifiModule();
char sendDataThroughputMode(long index, long length, uint8_t dataPadding);
char updateConfigFromResponse();
void saveWifiStrengthToEeprom(int strength);
int getSignalStrengthFromEeprom();

#endif	/* WIFI_H */