#include "SI70.h"

uint16_t getRawHumid(){
    return I2CReadWord(SI70_DEVICE_ADD, SI70_CONVERT_HUMID, 10, 1);
}
float getHumid(uint16_t rawHumid){
    if(rawHumid == 0){
        return BAD_SENSOR_READING;
    }else{
        return (125.0*(float)rawHumid/65536.0)-6.0;
    }
}

uint16_t getRawTemp(uint8_t convert){
    if(convert == 1){
        return I2CReadWord(SI70_DEVICE_ADD, SI70_CONVERT_TEMP, 6, 1);
    }else{
        return I2CReadWord(SI70_DEVICE_ADD, SI70_TEMP_REG, 0, 1);
    }
}
float getTemp(uint16_t rawTemp){
    if(rawTemp == 0){
        return BAD_SENSOR_READING;
    }else{
        return (((175.72f*(float)rawTemp)/65536.0f)-46.85f);
    }
}
uint16_t conToRawTemp(float temp){
    if(temp > 128){temp = 128;}
    if(temp < -46){temp = -46;}
    return (uint16_t)((65536.0f*(temp+46.85f))/175.72f);
}