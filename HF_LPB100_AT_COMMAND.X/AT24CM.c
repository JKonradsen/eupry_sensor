#include "AT24CM.h"

uint32_t AT24CMReadStreamUpToToU1(uint32_t address, uint32_t amount, char *outputString) {
    if(amount == 0){return 0;}
    if (address > FLASH_MAX_INDEX){address = address - FLASH_MAX_INDEX;}
    uint8_t data = 0;
    uint8_t ffCount = 0;
    uint32_t output = 0;
    uint8_t P0 = (address>>15)&0x00000002;
    _I2CSendStart(AT24_BUS);
    _I2CSendByte(AT24_ADD|P0, AT24_BUS);
    if(_I2CGetACK(AT24_BUS) == 0){
        _I2CSendByte((address >> 8)&0x00ff, AT24_BUS);
        if(_I2CGetACK(AT24_BUS) == 0){
            _I2CSendByte(address, AT24_BUS);
            if(_I2CGetACK(AT24_BUS) == 0){
                _I2CSendRepeatedStart(AT24_BUS);
                _I2CSendByte(AT24_ADD+1, AT24_BUS);
                if(_I2CGetACK(AT24_BUS) == 0){
                    if(amount > 0){
                        for(ffCount = 0; ffCount < 5; ffCount++){
                            while((data != 0xff) && (amount > 0)){
                                data = _I2CgetByte(AT24_BUS);
                                _I2CSendACK(0, AT24_BUS);
                                if(data == '\n'){
                                    output = amount;
                                }
                                *outputString++ = data;
                                amount--;
                            }
                        }
                    }
//                    else{
//                        for(ffCount = 0; ffCount < 10; ffCount++){
//                            while(data != 0xff){
//                                data = _I2CgetByte(AT24_BUS);
//                                _I2CSendACK(0, AT24_BUS);
//                                U1Send(data);
//                                output++;
//                            }
//                        }
//                    }
                    _I2CgetByte(AT24_BUS);
                    _I2CSendACK(1, AT24_BUS);
                    _I2CSendStop(AT24_BUS);
                    *outputString = 0;
                    return output;
                }else{
                    _I2CSendStop(AT24_BUS);
                    *outputString = 0;
                    return output;
                }
            }else{
                _I2CSendStop(AT24_BUS);
                *outputString = 0;
                return output;
            }
        }
    }else{
        _I2CSendStop(AT24_BUS);
        *outputString = 0;
        return output;
    }
    return 0;
}
uint32_t AT24CMReadStreamUpToToU2(uint32_t address, uint32_t amount) {
    if (address > FLASH_MAX_INDEX){address = address - FLASH_MAX_INDEX;}
    uint8_t data = 0;
    uint8_t ffCount = 0;
    uint8_t output = (uint8_t)amount;
    uint8_t P0 = (address>>15)&0x00000002;
    _I2CSendStart(AT24_BUS);
    _I2CSendByte(AT24_ADD|P0, AT24_BUS);
    if(_I2CGetACK(AT24_BUS) == 0){
        _I2CSendByte((address >> 8)&0x000000ff, AT24_BUS);
        if(_I2CGetACK(AT24_BUS) == 0){
            _I2CSendByte(address&0x000000ff, AT24_BUS);
            if(_I2CGetACK(AT24_BUS) == 0){
                _I2CSendRepeatedStart(AT24_BUS);
                _I2CSendByte(AT24_ADD|1, AT24_BUS);
                if(_I2CGetACK(AT24_BUS) == 0){
                    if(amount > 0){
                        while((amount > 0)){
                            data = _I2CgetByte(AT24_BUS);
                            _I2CSendACK(0, AT24_BUS);
                            if(data == '\n'){
                                output = amount;
                            }
                            U2Send(data);
                            amount--;
                        }
                    }else{
                        for(ffCount = 0; ffCount < 10; ffCount++){
                            while(data != 0xff){
                                data = _I2CgetByte(AT24_BUS);
                                _I2CSendACK(0, AT24_BUS);
                                U2Send(data);
                                output++;
                            }
                            U2Send(data);
                        }
                    }
                    _I2CgetByte(AT24_BUS);
                    _I2CSendACK(1, AT24_BUS);
                    _I2CSendStop(AT24_BUS);
                    return output;
                }else{
                    _I2CSendStop(AT24_BUS);
                    return output;
                }
            }else{
                _I2CSendStop(AT24_BUS);
                return output;
            }
        }
    }else{
        _I2CSendStop(AT24_BUS);
        return output;
    }
    return 0;
}
uint32_t AT24CMWriteArray(uint32_t address, char *input) {
    if (address > FLASH_MAX_INDEX){address = address - FLASH_MAX_INDEX;}
    int pageOffset = (PAGE_SIZE - ((address) % PAGE_SIZE));
    uint8_t P0 = (address>>15)&0x00000002;

    _I2CSendStart(AT24_BUS);
    _I2CSendByte(AT24_ADD|P0, AT24_BUS);
    if(_I2CGetACK(AT24_BUS) == 0){
        _I2CSendByte((address>>8)&0x000000ff, AT24_BUS);
        if(_I2CGetACK(AT24_BUS) == 0){
            _I2CSendByte(address&0x000000ff, AT24_BUS);
            if(_I2CGetACK(AT24_BUS) == 0){
                while(*input && pageOffset > 0){
                    _I2CSendByte(*input++, AT24_BUS);
                    if(_I2CGetACK(AT24_BUS) == 0){
                        address++;
                        pageOffset--;
                    }else{
                        _I2CSendStop(AT24_BUS);
                        return 0;
                    }
                }
                _I2CGetACK(AT24_BUS);
                _I2CSendStop(AT24_BUS);

                if(*input != 0){
                    delay_2ms(10);
                    P0 = (address>>15)&0x00000002;
                    _I2CSendStart(AT24_BUS);
                    _I2CSendByte(AT24_ADD|P0, AT24_BUS);
                    if(_I2CGetACK(AT24_BUS) == 0){
                        _I2CSendByte((address>>8)&0x000000ff, AT24_BUS);
                        if(_I2CGetACK(AT24_BUS) == 0){
                            _I2CSendByte(address&0x000000ff, AT24_BUS);
                            if(_I2CGetACK(AT24_BUS) == 0){
                                while(*input){
                                    _I2CSendByte(*input++, AT24_BUS);
                                    if(_I2CGetACK(AT24_BUS) == 0){
                                        address++;
                                    }else{
                                        _I2CSendStop(AT24_BUS);
                                        return 0;
                                    }
                                }
                                _I2CGetACK(AT24_BUS);
                                _I2CSendStop(AT24_BUS);
                                return address;
                            }else{
                                _I2CSendStop(AT24_BUS);
                                return 0;
                            }
                        }
                    }else{
                        _I2CSendStop(AT24_BUS);
                        return 0;
                    }
                }
                return address;
            }else{
                _I2CSendStop(AT24_BUS);
                return 0;
            }
        }
    }else{
        _I2CSendStop(AT24_BUS);
        return 0;
    }
    return 0;
}
