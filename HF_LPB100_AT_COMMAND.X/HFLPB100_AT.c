#include "HFLBP100_AT.h"

/*****************************
 * AT commands and responses *
 *****************************/

//Genereal
const char OK[] =  "ok";
#define ERR 0
#define SUCCESS 1
//Command mode
const char CMD_OK[] = "ok";
const char CMD_PLUS[] = "+++";
//Workmode
#define AP 1
#define STA 2
#define APSTA 3
const char AT_WMODE[] =  "AT+WMODE";
//Reset device
const char AT_RESET[] =  "AT+Z\r";
//Set WiFi key
const char AT_WSKEY[] = "AT+WSKEY";
//Set BSSID
const char AT_WSSSID[] =  "AT+WSSSID";
//Query Link status
const char AT_WSLK[] =  "AT+WSLK";
const char DISCONNECTED_S[] = "Disconnected";
#define DISCONNECTED 0
#define CONNECTED 1
//Query signal strength
const char AT_WSLQ[] =  "AT+WSLQ";
//Scan for acccesspoints
const char AT_WSCAN[] =  "AT+WSCAN";
//Ping service
const char AT_PING[] =  "AT+PING";
//Set/query AP parameters
const char AT_WAP[] =  "AT+WAP";
//Set/query AP security parameters
const char AT_WAKEY[] =  "AT+WAKEY";
const char OPEN[] =  "OPEN";
const char SHARED[] =  "SHARED";
const char WPAPSK[] =  "WPAPSK";
const char WPA2PSK[] =  "WPA2PSK";
const char NONE[] =  "NONE";
const char WEP[] =  "WEP";
const char TKIP[] =  "TKIP";
const char AES[] =  "AES";
//Querry MAC adress of connected STA
const char AT_WALK[] = "AT+WALK";
//Querry own MAC adress
const char AT_WSMAC[] = "AT+WSMAC";
//Test if in command mode
const char AT_PLUS[] = "AT+\r";
//Turn on or off echo
const char AT_E[] = "AT+E";
//Set/query deepsleep/standby mode
const char AT_MSLP[] = "AT+MSLP";
const char STANDBY[] = "standby";
const char NORMAL[] = "normal";
//Set HTTP URL adress
const char AT_HTTPURL[] = "AT+HTTPURL";
//Set HTTP request type
const char AT_HTTPTP[] = "AT+HTTPTP";
//Set HTTP protocol header path
const char AT_HTTPPH[] = "AT+HTTPPH";
//Set HTTP Connection area
const char AT_HTTPCN[] = "AT+HTTPCN";
//Set HTTP User-Agent area
const char AT_HTTPUA[] =  "AT+HTTPUA";
//Send HTTP Data
const char AT_HTTPDT[] = "AT+HTTPDT";
//Req or set Wifi chip ON/OFF
const char AT_WIFI[] =  "AT+WIFI";
const char UP[] =  "UP";
const char DOWN[] =  "DOWN";
const char NETP[] = "AT+NETP=tcp,client,80,newapi.eupry.com\r";
/********************
 * TYPE DEFINITIONS *
 ********************/
#define True 1
#define False 0

char compareStrings(const char cmd[], const char in[]){
    
    __delay_ms(5);

    if(strstr(in, cmd) != NULL) {
        return SUCCESS;
    }else{
        return ERR;
    }
}
char setCommandMode(){  //WORK OK
    int i;
    clearSerialBuffer();
    U1ClearErrors();
    U2ClearErrors();
    U1SendArray("+++");
    
    if(!serialEventWait(300, "a")){/*return 0;*/}
    //delay_2ms(3);
    U1SendArray("a");
    U1IndexInterupt = 0;
    if(serialEventWait(50, CMD_OK)){
        U1SendArray("\r");
        return 1;
    }else if(compareStrings("A", U1buf)){
        U1SendArray("\r");
        return 1;
    }
    return 0;
}
int string_length(const char s[]) {
   int c = 0;

   while (s[c] != '\0'){
      c++;
   }

   return c;
}
char serialEventWait(long timeout, const char cmd[]){
  long n = 0;
  clearSerialEvent();

  if(cmd == NULL){  //Not want to check with command
    while(!U1intFlag && n < timeout){
        serialEvent();
        n++;
        __delay_ms(1);
        
    }
    if(n > timeout-2){
        return 0;
    }else{
        return 1;
    }
  }else{  //Constantly check with command and buffer to quickly return if it has responded right
      timeout = timeout*2000;
      while(n < timeout){
          __delay_us(10);
          if(U1intFlag){
              U1intFlag = 0;
              __delay_ms(10);
              if(compareStrings(cmd, U1buf)){
                  return 1;
              }
          }
          n++;
      }
      return 0;
  }
}
char serialEventWaitDouble(long timeout, const char cmd[],const char cmd2[]){
  long n = 0;
  int i;
  clearSerialEvent();

  if(cmd == NULL){  //Not want to check with command
    while(!U1intFlag && n < timeout){
        serialEvent();
        n++;
        __delay_ms(1);

    }
    if(n > timeout-2){
        return 0;
    }else{
        return 1;
    }
  }else{  //Constantly check with command and buffer to quickly return if it has responded right
      timeout = timeout*2000;
      while(n < timeout){
          __delay_us(10);
          if(U1intFlag){
              if(compareStrings(cmd, U1buf)){
                  return 1;
              }else if(compareStrings(cmd2, U1buf)){
                  return 2;
              }else{
                  U1intFlag = 0;
                  //TODO: only used in test program, and missing delay here compeared to serialEventWait()
              }
          }
          n++;
      }
      return 0;
  }
}
void serialEvent() {

  if(U1intFlag){
    delay_2ms(80);
    #ifdef DEBUG_ENABLE
    printDebug("");
    printDebug("-------------------- DEBUG: -------------------------");
    printDebug(U1buf);
    printDebug("-----------------------------------------------------");
    #endif

    U1intFlag = 0;
  }
}
void clearSerialEvent(){
    U1intFlag = 0;
}
void clearSerialBuffer(){
    int i;
    for(i = 0; i < U1bufferSize-1; i++){
        U1buf[i] = NULL;
    }
    U1buf[0] = '\0';
    U1IndexInterupt = 0;
    ModulusU1IndexInterupt = 0;
}
char setWorkMode(char workmode){ //WORK OK
 char cmd[40];
 cmd[0] = '\0';
 strcat(cmd, AT_WMODE);
 clearSerialBuffer();
 clearSerialEvent();

 switch(workmode){

  case AP:
     strcat(cmd,"=AP\r");
     break;
  case STA:
     strcat(cmd,"=STA\r");
     break;
  case APSTA:
    strcat(cmd,"=APSTA\r");
    break;
 }
 clearSerialBuffer();
 U1SendArray(cmd);
 return serialEventWait(2000, CMD_OK);
}
char reqWorkMode(){  //WORK OK

  clearSerialBuffer();
  U1SendArray(AT_WMODE);
  U1SendArray("\r");
  serialEventWait(100, NULL);

  if(compareStrings("AP", U1buf)){
    return AP;
  }else if(compareStrings("STA", U1buf)){
    return STA;
  }else if(compareStrings("APSTA", U1buf)){
    return APSTA;
  }else{
    return ERR;
  }

}
void resetDevice(){  //WORK OK

    NRESET = 0;
    __delay_ms(10);
    __delay_ms(10);
    NRESET = 1;
    clearSerialBuffer();  
    uint8_t timeout = 100;
    while(NREADY && timeout > 0){
        //clrwdt();
        delay_2ms(50);
        timeout--;
    }
  
  //serialEventWait(800, OK);
}
char setSTAKey(const char auth[], const char encry[], const char key[]){  //WORK OK
   char cmd[100];
   cmd[0] = '\0';
   strcat(cmd,AT_WSKEY);
   strcat(cmd,"=");
   strcat(cmd,auth);
   strcat(cmd,",");
   strcat(cmd,encry);
   strcat(cmd,",");
   strcat(cmd,key);
   strcat(cmd,"\r");

  #ifdef COMMAND_DELAY
    delay_2ms(100);
  #endif
   clearSerialBuffer();
   U1SendArray(cmd);
   return serialEventWait(200, OK);;

}
char reqMode(){  //WORK OK
  clearOutputBuffer();
  //delay_2ms(1);
  clearSerialBuffer();
  U1SendArray(AT_PLUS);
  return serialEventWait(20, OK);
}
void clearOutputBuffer(){

  U1SendArray("\r");
  serialEventWait(100, NULL);
  clearSerialBuffer();
}
void echo(char e){  //WORK OK
  char cmd[40];
  cmd[0] = '\0';
  strcat(cmd, AT_E);
  strcat(cmd, "=");
  if(e){
    strcat(cmd,"on\r");
  }else{
    strcat(cmd, "off\r");
  }
  U1SendArray(cmd);
  serialEventWait(100, OK);
}
char pingSite(const char site[]){  //WORK OK
   char cmd[40];
   cmd[0] = '\0';
   strcat(cmd, AT_PING);
   strcat(cmd, "=");
   strcat(cmd, site);
   strcat(cmd, "\r");

   clearSerialBuffer();
   U1SendArray(cmd);
   return serialEventWait(600, OK);;
}
char setSTASSID(const char SSID_name[]){ //WORK OK
  char cmd[40];
  cmd[0] = '\0';
  strcat(cmd, AT_WSSSID);
  strcat(cmd,"=");
  strcat(cmd,SSID_name);
  strcat(cmd,"\r");

  clearSerialBuffer();
  U1SendArray(cmd);
  return serialEventWait(2000, OK);
}
int getSignalStrengthSTA(){  //WORK OK
  char strength_S[] = "   ";
  int i;
  char keys[] = "%";
  char cmd[40];
  cmd[0] = '\0';
  strcat(cmd, AT_WSLQ);
  strcat(cmd,"\r");

  clearOutputBuffer();
  delay_2ms(1);
  clearSerialBuffer();
  U1SendArray(cmd);

  if(!serialEventWait(100, "%")){
#ifdef DEBUG_PRINT
      U2SendArray("\rLOST  % \r\r\r");
#endif
      return 0;
  }
  for(i = 0; i < U1IndexInterupt; i++){
      if(U1buf[i] == '%'){
          break;
      }
  }
  if(i < U1IndexInterupt){
#ifdef DEBUG_ENABLE
    UART2Send(U1buf[i-2]);
    UART2Send(U1buf[i-1]);
    UART2Send(U1buf[i]);
#endif
    strength_S[0] = U1buf[i-3];
    strength_S[1] = U1buf[i-2];
    strength_S[2] = U1buf[i-1];
  }

  return atoi(strength_S);

}
char reqLinkStatus(){ //WORK OK

  char cmd[40];
  clearOutputBuffer();
  cmd[0] = '\0';
  strcat(cmd, AT_WSLK);
  strcat(cmd, "\r");

  clearSerialBuffer();
  U1SendArray(cmd);
  serialEventWait(200, OK);
#ifdef DEBUG_ENABLE
  UART2SendString(U1buf);
#endif

  if(compareStrings(DISCONNECTED_S, U1buf)){return DISCONNECTED;}
  else{return CONNECTED;}
}
void setDeepSleep(){  //Not impemented yet
  char cmd[40];
  cmd[0] = '\0';
  strcat(cmd, AT_MSLP);
  strcat(cmd, "=");
  strcat(cmd, STANDBY);
  strcat(cmd, "\r");

  clearSerialBuffer();
  U1SendArray(cmd);
  serialEventWait(100, NULL);

}
char setHTTPURL(const char url[], const char port[]){  //WORK OK
  char cmd[40];
  cmd[0] = '\0';
  strcat(cmd, AT_HTTPURL);
  strcat(cmd, "=");
  strcat(cmd, url);
  strcat(cmd, ",");
  strcat(cmd, port);
  strcat(cmd, "\r");

  clearSerialBuffer();
  U1SendArray(cmd);
  return serialEventWait(1000, OK);

}
char setHTTPReqType(const char type[]){  //WORK OK
  char cmd[40];
  cmd[0] = '\0';
  strcat(cmd, AT_HTTPTP);
  strcat(cmd, "=");
  strcat(cmd, type);
  strcat(cmd, "\r");

  clearSerialBuffer();
  U1SendArray(cmd);
  return serialEventWait(1000, OK);
}
char setHTTPHeaderPath(const char header[]){ //WORK OK
  char cmd[40];
  cmd[0] = '\0';
  strcat(cmd,  AT_HTTPPH);
  strcat(cmd, "=");
  strcat(cmd, header);
  strcat(cmd, "\r");

  clearSerialBuffer();
  U1SendArray(cmd);
  return serialEventWait(1000, OK);
}
char setHTTPConnectionArea(const char area[]){  //WORK OK
  char cmd[60];
  cmd[0] = '\0';
  strcat(cmd, AT_HTTPCN);
  strcat(cmd, "=");
  strcat(cmd, area);
  strcat(cmd, "\r");

  clearSerialBuffer();
  U1SendArray(cmd);
  return serialEventWait(1000, OK);

}
char setHTTPUserAgent(const char agent[]){  //WORK OK
  char cmd[60];
  cmd[0] = '\0';
  strcat(cmd, AT_HTTPUA);
  strcat(cmd,  "=");
  strcat(cmd, agent);
  strcat(cmd, "\r");

  clearSerialBuffer();
  U1SendArray(cmd);
  return serialEventWait(1000, OK);

}
char setHTTPSendData(const char data[], uint32_t index){
    char cmd[80];
    cmd[0] = '\0';
    strcat(cmd,AT_HTTPDT);
    strcat(cmd, "=");
    U1SendArray(cmd);

    if(data != NULL){
        U1SendArray(data);
    }else{
        FLASHStreamUpToToU1(index, MAX_SEND_LENGTH, 0);
    }

    clearSerialBuffer();
    U1Send('\r');

    return serialEventWaitDouble(sendDataWaitTime, "200 OK", "Send error");
    //TODO: Make dependent on response from server
}
char reqWifi(){
  char cmd[40];
  cmd[0] = '\0';
  strcat(cmd, AT_WIFI);
  strcat(cmd, "\r");

  clearSerialBuffer();
  U1SendArray(cmd);
  serialEventWait(100, OK);

  if(compareStrings(UP, U1buf)){
     return True;
  }else{
     return False;
  } 
}
char setSmartLink(){
#ifdef DEBUG_PRINT
  U2SendArray("SmartLink\n");
#endif
  int i = 0;
  U1SendArray("AT+SMTLK\r");
  clearSerialBuffer();
  delay_2ms(200);
  while((i < SMARTLINK_TIMEOUT)&&(GLOBALuserPushFlag == 0)){
#ifdef DEBUG_PRINT
        U2SendArray("Waiting SmartLink\n");
#endif
        delay_2ms(100);
        ClrWdt();
        if(NREADY == 0){
            delay_2ms(100);
            setCommandMode();
            if(reqLinkStatus()){
                U2SendArray("Link status ok\n");
                delay_2ms(2000);
                resetDevice();
                return 1;
            }else{
                U2SendArray("Link status failure\n");
                return 0;
            }
        }
        i++;
  }
  return 0;
}
void getFirmwareVersion(){
  char version_S[] = "       ";
  int i;
  char keys[] = "%";
  char cmd[40];
  cmd[0] = '\0';
  strcat(cmd, "AT+VER");
  strcat(cmd,"\r");

  clearSerialBuffer();
  U1SendArray(cmd);
  
  if(!serialEventWait(5500, "=")){
      //U2SendArray("\rLOST  % \r\r\r");    
  }
  
  for(i = 0; i < U1IndexInterupt; i++){
      if(U1buf[i] == '='){
          break;
      }
  }
  if(i < U1IndexInterupt){
      i++;
      for(int l = 0; l < 7; l++){
          U2Send(U1buf[i+l]);
      }
      U2Send('$');
  }else{
      U2SendArray("NAN$");
  }
  //return atoi(strength_S);

}
char reqMACAdress(){
  int i;
  char adress_s[] = "";
  clearSerialBuffer();
  U1SendArray(AT_WSMAC);
  U1SendArray("\r");
  if(!serialEventWait(200, OK)){return 0;}
  __delay_ms(10);
  strcat(adress_s, strstr(U1buf, "ok"));

#ifdef DEBUG_PRINT
  U2SendArray(adress_s);
#endif

  for(i = 0; i < strlen(adress_s); i++){
    EEPROM_save8Bit(MAC_ADRESS_START_ADRESS+i, adress_s[3+i]);
  }
  
  return 1;
}
char setNETP(){
  char cmd[60];
  cmd[0] = '\0';
  //strcat(cmd, "AT+NETP=tcp,client,80,newapi.eupry.com\r");
  strcat(cmd, NETP);
  clearSerialBuffer();
  U1SendArray(cmd);
  return serialEventWait(1000, OK);
}