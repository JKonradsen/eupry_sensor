#include "DS18B20.h"


float DS18B20GetTemp(uint8_t *scratchpad){

    uint16_t temp = (scratchpad[1]<<8)|scratchpad[0];
    float out = 0;

    // Conversion based on Table 1. Temperature/Data Relationship [Page 4]
    if(temp&0x8000){
        temp ^= 0x7fff;
        temp += 1;
    }
    if(temp&0x0400){out += 64.0f;}
    if(temp&0x0200){out += 32.0f;}
    if(temp&0x0100){out += 16.0f;}
    if(temp&0x0080){out +=  8.0f;}
    if(temp&0x0040){out +=  4.0f;}
    if(temp&0x0020){out +=  2.0f;}
    if(temp&0x0010){out +=  1.0f;}
    if(temp&0x0008){out +=  0.5f;} // Last bit when using: 9 bit resolution
    if(temp&0x0004){out +=  0.25f;} // Last bit when using: 10 bit resolution
    if(temp&0x0002){out +=  0.125f;} // Last bit when using: 11 bit resolution
    if(temp&0x0001){out +=  0.0625f;} // Last bit when using: 12 bit resolution
    if(temp&0x8000){out *= -1.0f;}

    return out;
}
void DS18B20SetResulution(uint8_t res){
  // TODO: ?
  // Lowering precision will greatly reduce conversion time. [Page 8]
  // Resolution | Max conversion time   | Precision
  //   9 bits   |    93.75 ms (tCONV/8) | 0.5 °C
  //  10 bits   |   187.5_ ms (tCONV/4) | 0.25 °C   <<-- Enough for us?
  //  11 bits   |   375.__ ms (tCONV/2) | 0.125 °C
  //  12 bits   |   750.__ ms (tCONV)   | 0.0625 °C
}
