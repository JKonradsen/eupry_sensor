#ifndef SD18_H
#define	SD18_H

#include "main.h"

float DS18B20GetTemp(uint8_t *scratchpad);
void DS18B20SetResulution(uint8_t res);

#endif

