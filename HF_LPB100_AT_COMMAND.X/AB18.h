#ifndef AB18_H
#define	AB18_H

#define AB18_ADD 0xD2

#define USER_PUSH 0x01
#define TIMER_END 0x08
#define USER_INT 0x01
#define TIMER_INT 0x08
#define BAT_INT 0x18

#include "main.h"

//Set time in RTC from unixtime
void AB18SetUnixTime(uint32_t unixTime);
//Get time as unixtime from RTC
unsigned long AB18GetUnixTime();
//Start RTC and set osc up
uint8_t AB18Init();
//Set Timer
void AB18SetTimer(uint16_t sec, uint8_t force);
//Enable timer interrupt
void AB18EnableInterrupt(uint8_t interrupts);
//Disable timer interrupt
void AB18DisableInterrupt();
//Cut power from main IC
uint8_t AB18Sleep();
//Get interrupts register
uint8_t AB18GetInterrupt();
//Get EXTI buttem level
uint8_t AB18getButtonLevel();
//Get the activated intterups
uint8_t AB18getInterrupts();
//Reset the IC
void AB18SWReaset();
//Set BackUp capacitor        
void AB18SetSCBackUp();

#endif

