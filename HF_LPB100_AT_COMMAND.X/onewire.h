#ifndef ONEWIRE_H
#define	ONEWIRE_H

#define ONEWIRE_SKIP_ROM 0xCC
#define ONEWIRE_SEARCH_ROM 0xF0
#define ONEWIRE_CONVERT_TEMP 0x44
#define ONEWIRE_CONVERT_VOLT 0xB4
#define ONEWIRE_READ_SCRATCHPAD 0xBE
#define ONEWIRE_ALARM_SEARCH 0xEC
#define ONEWIRE_MATCH_ROM 0x55
#define ONEWIRE_WRITE_SCRATCHPAD 0x4E
#define ONEWIRE_COPY_SCRATCHPAD 0x48

#include "main.h"

#define ONEWIRE_TRIS TRISBbits.TRISB1
#define ONEWIRE_PIN PORTBbits.RB1

void delay480();
uint8_t CRConebyte(uint8_t currentByte, uint8_t inputByte);
void StartConvertion();
uint8_t OneWireReset();
void OneWireWrite (uint8_t Cmd);
uint8_t OneWireRead ();
void OneWireResetSearch();
uint8_t OneWireSearch(uint8_t *newAddr);
void OneWireSelect(uint8_t add[8]);
void OneWireWritebit(uint8_t data);
uint8_t ReadScratchpad(uint8_t *dataout, uint8_t add[8]);
uint8_t getDoorStat(uint8_t addr[8]);
uint8_t getBatVolt(uint8_t addr[8]);

#endif

