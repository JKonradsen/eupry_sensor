/* 
 * File:   HFLBP100_AT.h
 * Author: jakobkonradsen
 *
 * Created on 5. februar 2015, 19:12
 */
#ifndef HFLBP100_AT_H
#define	HFLBP100_AT_H

#include "main.h"

/******************
 *  Timeout values*
 ******************/
#define SMARTLINK_TIMEOUT 100
int sendDataWaitTime = 200;  //Time to wait for response from server when sending data

/***************
 * DEBUG FLAGS *
 ***************/
//#define DEBUG_ENABLE
/***********************
 * FUNCTION PROTOTYPES *
 ***********************/

char serialEventWait(long timeout, const char cmd[]);
void serialEvent();
void clearSerialEvent();
void clearSerialBuffer();
char compareStrings(const char cmd[], const char in[]);
char setCommandMode();
char setWorkMode(char workmode);
char reqWorkMode();
void resetDevice();
char setSTAKey(const char auth[], const char encry[], const char key[]);
char reqMode();
void clearOutputBuffer();
void echo(char e);
char pingSite(const char site[]);
char setSTASSID(const char SSID_name[]);
int  getSignalStrengthSTA();
char reqLinkStatus();
void setDeepSleep();
char setHTTPURL(const char url[], const char port[]);
char setHTTPReqType(const char type[]);
char setHTTPHeaderPath(const char header[]);
char setHTTPConnectionArea(const char area[]);
char setHTTPUserAgent(const char agent[]);
char setHTTPSendData(const char data[], uint32_t index);
char reqWifi();
char setSmartLink();
void getFirmwareVersion();
char reqMACAdress();
char serialEventWaitDouble(long timeout, const char cmd[],const char cmd2[]);
char setNETP();

#endif	/* HFLBP100_AT_H */

