#ifndef UNIXTIME_H
#define	UNIXTIME_H

#include "main.h"

typedef struct{
    uint8_t hour, min, sec;
    uint32_t year, mon, day, unixTime, oldunixTime;
}TIME;

extern TIME globalTime;

#define YEAR_TO_DAYS(y) ((y)*365 + (y)/4 - (y)/100 + (y)/400)

const int DaysToMonth365[] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };
const int DaysToMonth366[] = { 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366 };
const long TicksInMillisecond = 10000L;
const long TicksInSecond = 1L;

// Converts year, month, day, hour, minute and second into unixtime
unsigned long getUnixTime(int year, int month, int day, int hour, int minute, int second);
// Converts UnixTime to clock
void getTimeFields(uint32_t unixtime);

// Even with 4 years leap, drift still occurs, and must be corrected using a 100 year non-leap and 400 year leap
uint8_t IsLeapYear(int year);
long DateToTicks(int year, int month, int day);
long TimeToTicks(int hour, int minute, int second);

#endif
