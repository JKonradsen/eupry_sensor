#ifndef FLASH_H
#define	FLASH_H

#include "main.h"
#include "AT25DF.h"
#include "AT24CM.h"
#include "EEPROM.h"

#define FLASH_SWITCH_PORT LATBbits.LATB5
#define FLASH_SWITCH_TRIS TRISBbits.RB5

#define PAGE_SIZE 256
#define FLASH_MAX_INDEX 524287

#define EEPROM_MAX_INDEX 1024

//Stream from flash to UART1
uint32_t FLASHStreamUpToToU1(long address, long amount, uint8_t encrupted);
//Stream from flash to UART2
uint32_t FLASHStreamUpToToU2(uint32_t address, uint32_t amount);
//Read one byte from flash
uint8_t FLASHReadByte(uint32_t address);
//Write array into flash
uint32_t FLASHWriteArray(char *input);
//Clear flash
void FLASHclear();
//Delete page in flash
void FLASHdeletPage(uint32_t address);
// Turn off flash
void turnOFFFLASH();
// Turn on flash
void turnONFLASH();

uint8_t EEPROM_laod8Bit(uint16_t address);
void EEPROM_save8Bit(uint16_t address, uint8_t input);
uint16_t EEPROM_laod16Bit(uint16_t LowAdd);
void EEPROM_save16Bit(uint16_t LowAdd, uint16_t input);
uint32_t EEPROM_laod32Bit(uint16_t LowAdd);
void EEPROM_save32Bit(uint16_t LowAdd, uint32_t input);

uint16_t EEPROMWriteArray(uint16_t address, char *input);
uint32_t EEPROMReadStreamUpToToU1(uint16_t address, uint16_t amount, char *outputString);
uint32_t EEPROMReadStreamUpToToU2(uint16_t address, uint16_t amount);
char *EEPROMReadToString(uint16_t address, char *input, uint16_t length);

void moveDataFromEEPROMtoFlash(char *input);

#endif

