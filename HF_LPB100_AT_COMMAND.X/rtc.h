#ifndef RTC_H
#define	RTC_H

#include "main.h"
#include "AB18.h"
#include "unixtime.h"

//extern TIME globalTime;
extern uint8_t volatile RTCbuseFlag;
// Get time stamp as string
void getTimeStempAsString(char *buffer);

uint32_t RTCgetUnixTime();
void setTimeFromUnixTime(uint32_t unixTime);

void RTCsetTimer(uint16_t sec, uint8_t force);
uint8_t initRTC();
uint8_t RTCEnterOffMode(uint8_t interrupts);

uint8_t RTCgetInts();
void RTCDisableInterrupt();
void RTCEnableInterrupt(uint8_t interrupts);
uint8_t RTCgetButtonLevel();
uint8_t RTCreturnIntettupts();
void RTCreset();

#endif

