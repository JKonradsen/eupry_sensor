void AESEncode(unsigned char* block, unsigned char* key);
void AESDecode(unsigned char* block, unsigned char* key);
void AESCalcDecodeKey(unsigned char* key);
void encryptBlock(char* plaintext);
void decryptBlock(char* plaintext);
void reInitKey();
