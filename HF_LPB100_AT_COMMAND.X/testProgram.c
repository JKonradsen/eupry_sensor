#include "testProgram.h"



void testProgram(){
    char dataSuccess;
    int i;
    char testBuf[5];
    testBuf[4] = 0;
    
    U2SendArray("Reached test program\r");
    
    turnONSensors();
    if(initSensors() > 0){              //Check of sensors
        testBuf[0] = '1';
    }else{
        testBuf[0] = '0';
    }
    turnOFFSensors();
     
    if(initRTC()){                  //Check of RTC
        testBuf[1] = '1';
    }else{
        testBuf[1] = '0';
    }
    
    if(/*initFlash()*/0){           //Check of FLASH, to be implemented
        testBuf[2] = '1';
    }else{
        testBuf[2] = '0';
    }
   
    turnONWifiModule();             //Check wifi module
    if(startupWifi(800)){
        testBuf[3] = '1';
    }else{
        testBuf[3] = '0';
    }
    turnOFFWifiModule();
    
    U2SendArray("\n"); 
    U2SendArray(testBuf);
    U2SendArray("\n");              //End of transmission
}

void runBurst(char num){
    int i = 0;

    startupWifiModule();
    for(i = 0; i < num; i++){
        char dataSuccess = sendData("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 0);
        U1ClearErrors();
        
        if(dataSuccess == DATASEND_OK){
            U2SendArray("OK\n");
        }else if(dataSuccess == DATASEND_NOLINK2){
            U2SendArray("NOLINK\n");
        }else if(dataSuccess == DATASEND_CONNECTERROR){
            U2SendArray("CERROR\n");
        }else if(dataSuccess == DATASEND_ERROR){  //Uneaxplainable error
           U2SendArray("DERROR\n");
        }else if(dataSuccess == DATASEND_SENDERROR){
           U2SendArray("SENDERROR\n");
        }
    }

    U2SendArray("END\n");
}

void runConnectionTest(){
    //U2SendArray("Running connection test\r");
    char dataSuccess;
    char buf[15];
    buf[0] = '\0';
    turnONWifiModule();
        if(!reqMode()){
            while(NREADY);
            delay_2ms(50);
            startupWifi(20);
            U1Send('\r');
            delay_2ms(5);  //TODO: update to do more intelligent
        }
        echo(0);

        int sendCounter;
        for(sendCounter = 0; sendCounter < dataSendTimeout; sendCounter++){

            dataSuccess = sendData("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 0);
            U1ClearErrors();
            if(dataSuccess == DATASEND_OK){
                U2SendArray("OKF\r");
                break;
            }else if(dataSuccess == DATASEND_NOLINK2){
                U2SendArray("EL\r");
                delay_2ms(5*noLinkDelay);
            }else if(dataSuccess == DATASEND_CONNECTERROR){
                U2SendArray("EC\r");
                
                delay_2ms(5);
            }else if(dataSuccess == DATASEND_SENDERROR){
               U2SendArray("ES\r");
            }else if(dataSuccess == DATASEND_ERROR){

               U2SendArray("EL\r");
               
               delay_2ms(5);
             }
            sprintf(Debugbuf, "Data success: %d", dataSuccess);
            U2SendArray(Debugbuf); //TODO: Get a fix for this, right now it only works when sprintf is on
            }

        if(dataSuccess == DATASEND_OK){
            U2SendArray("DOK\r");
        }else{
            U2SendArray("DER\r");
        }

        sprintf(buf,"STREN :%d: \r", getSignalStrengthSTA());
        U2SendArray(buf);
        turnOFFWifiModule();
        delay_2ms(1500);

}

void runGeneralTest(){
    turnONWifiModule();
    if(!reqMode()){
        while(NREADY);
        delay_2ms(50);
        startupWifi(20);
        U1Send('\r');
        delay_2ms(5);  
    }
    echo(0);
    getFirmwareVersion();           //Firmware version

    U2SendArray(firmware_version);  //PIC firmware version
    U2Send('$');

    if(initSensors()){              //Check of sensors
        U2SendArray("Sensors OK");
    }else{
        U2SendArray("Sensors FAIL");
    }
    U2Send('$');

    if(initRTC()){                  //Check of RTC
        U2SendArray("RTC OK");
    }else{
        U2SendArray("RTC FAIL");
    }
    U2Send('$');

    
    if(/*initFlash()*/0){                  //Check of FLASH, to be implemented
        U2SendArray("Flash OK");
    }else{
        U2SendArray("Flash FAIL");
    }
    U2Send('$');

    
}

void clearU2Buf(){
    int i;

    for(i = 0; i < 255; ){
        U2buf[i] = NULL;
    }
    U2buf[0] = '\0';
}