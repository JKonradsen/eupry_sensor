#ifndef MAIN_H
#define	MAIN_H

#define _XTAL_FREQ 16000000

//FIRMWARE version
#define firmware_version "PIC V1.0.0"
//#define hardware_version_V100
//#define firmware_version "PIC V1.1.0"
//#define hardware_version_V110
#define hardware_version_V200
#define newSPI
//#define sensorONOFF
#endif

//#define FAST_MEASURE
//#define CE

//Chose device:
//#define INSTALL_DEVICE
#define PRODUCTION_DEVICE
//#define DEVICE_0
//#define DEVICE_1
//#define DEVICE_2
//#define DEVICE_3
//#define DEVICE_4
//#define DEVICE_5
//#define DEVICE_6
//#define DEVICE_7
//#define DEVICE_8
//#define DEVICE_9
//#define DEVICE_10
//#define DEVICE_11
//#define DEVICE_12
//#define DEVICE_13
//#define DEVICE_14
//#define DEVICE_15
//#define DEVICE_16
//#define DEVICE_17
//#define DEVICE_18
//#define DEVICE_19
//#define DEVICE_20
//#define DEVICE_21
//#define DEVICE_22
//#define DEVICE_23
//#define DEVICE_24
//#define DEVICE_25
//#define DEVICE_26
//#define DEVICE_27
//#define DEVICE_28
//#define DEVICE_29
//#define DEVICE_30

//Need to be here so other files (like sensors.c) can use it.
//#define DEBUG_STATE
//#define NO_WIFI
//#define DEBUG_SENSORS
//#define DEBUG_PRINT
//#define DEBUG_PRINT_FLASH
#define DEBUG_PRINT_EXTENDED
//#define DEBUG_PRINT_STATES

//#define JAKOB_WORKING
//#define NOAM_WORKING
//#define TEST 0

// #ifdef hardware_version_V200
    #define SPIFLASH
    #define SOFTSPI
// #endif
//// #ifdef hardware_version_V110
    // #define I2CFLASH
// #endif

//IC lib
#include <p18f26k22.h>

//Compiler lib
#include <xc.h>

#include "DeviceConfig.h"

//Encryption
#include "AES.h"

//Help lib
#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

//High level HW lib
#include "flash.h"
#include "sensor.h"
#include "rtc.h"
#include "leds.h"

//Wifi
#include "wifi.h"

//Communication lib
#include "I2C.h"
#include "ONEWIRE.h"
//#include "PWM.h"
#include "SPI.h"
#include "UART.h"

//Test program
#include "testProgram.h"

// CONFIG1H
#pragma config FOSC = INTIO67   // Oscillator Selection bits (Internal oscillator block)
#pragma config PLLCFG = OFF     // 4X PLL Enable (Oscillator used directly)
#pragma config PRICLKEN = OFF   // Primary clock enable bit (Primary clock can be disabled by software)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Oscillator Switchover mode disabled)

// CONFIG2L
#pragma config PWRTEN = ON     // Power-up Timer Enable bit (Power up timer disabled)
#pragma config BOREN = ON      // Brown-out Reset Enable bits (Brown-out Reset disabled in hardware and software)
#pragma config BORV = 190       // Brown Out Reset Voltage bits (VBOR set to 1.90 V nominal)

// CONFIG2H
#pragma config WDTEN = ON       // Watchdog Timer Enable bits (Watch dog timer is always disabled. SWDTEN has no effect.)
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3H
#pragma config CCP2MX = PORTC1  // CCP2 MUX bit (CCP2 input/output is multiplexed with RC1)
#pragma config PBADEN = OFF     // PORTB A/D Enable bit (PORTB<5:0> pins are configured as digital I/O on Reset)
#pragma config CCP3MX = PORTB5  // P3A/CCP3 Mux bit (P3A/CCP3 input/output is multiplexed with RB5)
#pragma config HFOFST = OFF     // HFINTOSC Fast Start-up (HFINTOSC output and ready status are delayed by the oscillator stable status)
#pragma config T3CMX = PORTC0   // Timer3 Clock input mux bit (T3CKI is on RC0)
#pragma config P2BMX = PORTB5   // ECCP2 B output mux bit (P2B is on RB5)
#pragma config MCLRE = INTMCLR  // MCLR Pin Enable bit (MCLR pin enabled, RE3 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON     // Stack Full/Underflow Reset Enable bit (Stack full/underflow will not cause Reset)
#pragma config LVP = OFF        // Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection Block 0 (Block 0 (000800-003FFFh) not code-protected)
#pragma config CP1 = OFF        // Code Protection Block 1 (Block 1 (004000-007FFFh) not code-protected)
#pragma config CP2 = OFF        // Code Protection Block 2 (Block 2 (008000-00BFFFh) not code-protected)
#pragma config CP3 = OFF        // Code Protection Block 3 (Block 3 (00C000-00FFFFh) not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot block (000000-0007FFh) not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection Block 0 (Block 0 (000800-003FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection Block 1 (Block 1 (004000-007FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection Block 2 (Block 2 (008000-00BFFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection Block 3 (Block 3 (00C000-00FFFFh) not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-0007FFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection Block 0 (Block 0 (000800-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection Block 1 (Block 1 (004000-007FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection Block 2 (Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection Block 3 (Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-0007FFh) not protected from table reads executed in other blocks)

extern int timeOffseet;
extern uint16_t batteryVolt;

//Serial buffers
extern volatile uint16_t U1IndexInterupt;
extern volatile uint16_t ModulusU1IndexInterupt;
extern volatile char U1buf[513]; //Recorde UART
extern volatile uint8_t U1intFlag;


extern volatile uint8_t U2InteruptIndex;
extern volatile char U2buf[256]; //Recorde UART
extern volatile uint8_t U2intFlag;

extern char API[41];

extern char Debugbuf[100];
extern char Debugbuf2[257];


extern volatile uint8_t GLOBALuserPushFlag;

extern unsigned char key[16];
extern unsigned char tempKey[16];

//Function prototypes
void interupOn();
void interupOff();
void delay_2ms(uint16_t delay);
void delay_10us(uint16_t delay);

uint16_t getBat();

char getWakeUpReason();
void saveCurentMode(uint8_t mode);
void updateNextSendTime(uint32_t sendInterval);
uint8_t getSendClearens();
uint8_t getError();
void setError(uint8_t error);
void unSetError(uint8_t error);
void testProgram();

void initEEPROM();

void Timer1ON(uint16_t HundmilSec);
void Timer1OFF();
void setBlink(uint8_t color, uint16_t HundmilSec);
void waitForBlink();

void INTuserPush(uint8_t forcePush);
uint8_t userPushType();
void initPoweronVariables();

uint8_t getConFlags();
uint8_t getLogFlags();
void setLogFlag(uint8_t flag);
void unSetLogFlag(uint8_t flag);
void setConFlag(uint8_t flag);
void unSetConFlag(uint8_t flag);

void showLight();

uint8_t Fletcher8( uint8_t* data, char terminator);

//Flags
//Log flags
#define NO_LOGGING 0
#define UNKNOWEN_LOGGING 1
#define TRANSPORT_LOGGING 2

//Connection flags
#define CONNECTION 1
#define WIFI_CONNECTION 2
#define SERVER_CONNECTION 4
#define WIFI_WAS_INIT 8

//Temp flags
#define TEMP_OK 0
#define TEMP_LOW 1
#define TEMP_HIGH 2

//States
#define NOTHING 0
#define GET_WAKEUP 1
#define POWERON 2
#define MEASURE 3
#define INIT_WIFI 4
#define SMART_LINK 5
#define FORCE_UPLOAD 6
#define GET_BAT 7
#define CHECK_WIFI 8
#define SEND_DATA 9
#define END_OF_TRANSMITION 10
#define TEST_LOGFLAG 11
#define TEST_USER_PUSH_BEFORE_SLEEP 12
#define SLEEP_TIMER 13
#define SLEEP_USER_PUSH 14
#define GOING_TO_SLEEP 15

//EEPROM adresses
#define SPETIAL_LOG_FLAGS 1
#define SEND_INTERVAL_ADRESS 2
#define SEVERITY_VALUE_ADRESS 4
#define SEND_INDEX_ADRESS 8
#define FLASH_INDEX_ADRESS 12
#define MEASURMENT_INTERVAL_SEC_ADRESS 16
#define MEASURMENT_INTERVAL_FAST_ADRESS 18
#define FAST_SEND_INTERVAL_ADDRES 20
#define MEASURMENT_INDEX_ADRESS 22
#define TEMPBUF_1ST_ADRESS 24
#define TEMPBUF_LAST_ADRESS 79
#define SENSOR_ARRAY_ADRESS 80
#define NEXT_SEND_UNIXTIME_ADRESS 100
#define LAST_SEND_UNIXTIME_ADRESS 104
#define DATA_SEND_TIMEOUT_ADRESS 108
//#define DATA_SEND_ERROR_ADRESS 110 //TODO: in use?
#define ERROR_TO_SEND_ADRESS 111
#define ERROR_FLAGS_ADRESS 112
#define MINOR_SEND_TIMEOUT_ADRESS 113
#define MAJOR_SEND_TIMEOUT_ADRESS 114
#define OVERALL_SEND_TIMEOUT_ADRESS 115
#define WIFI_STRENGTH_ADRESS 116
#define FLASH_START_INDEX_ADRESS 117
#define MAC_ADRESS_START_ADRESS 121
#define DEVICE_NUMBER_ADDRES 133
#define API_ADDRES 141
#define LOG_FLAGS_ADRESS 174
#define CON_FLAGS_ADRESS 175
#define ENCRYPTION_KEY_ADDRES 176
#define LOCAL_FLASH_INDEX_ADRESS 192

//
#define NO_PUSH 0
#define TIMER 1
#define SHORT_PUSH 2
#define LONG_PUSH 3
#define SUPER_LONG_PUSH 4

//Default values
#define GRAY_ZONE_ALARM_TIMER_DEFAULT 30
#define MEASURE_INTERVAL_DEFAULT 180 //sec
#define FAST_MEASURE_INTERVAL_DEFAULT 30 //sec
#define SEND_INTERVAL_DEFAULT 720
#define SEND_LONG_WAIT 10080 //Wait for a week
#define USER_PSUH_SEND_DELAY 0 //Minimum send delay between use push
#define ALARM_SEND_DELAY 60 //Minimum send delay between alarm send

//Error flags with hardware
#define STARTUP_ERROR_FLAG 1
#define RTC_ERROR_FLAG 2
#define SENSOR_ERROR_FLAG 4
#define WIFI_ERROR_FLAG 8
#define FLASH_ERROR_FLAG 16

//Error codes for server
#define DATASEND_TIMEOUT_ERROR_FLAG 16
#define DATASEND_NOLINK2 32
#define SEVER_SEND_ERROR 64
#define NO_WIFI_FLAG 128

//Power on send sequencu variables
#define DEFAULT_minorSendTimeoutNumber 10
#define DEFAULT_majorSendTimeoutNumber 3
#define DEFAULT_overallSendTimeoutNumber 5

#define LOCAL_MEASUREMENTS_OFFSET 200
#define LOCAL_MEASUREMENTS_MAX_INDEX 780
#define MEASUREMNT_POINT_SIZE 40

#define ENCRYPTION_LENGTH 16
#define USE_ENCRYPTION 1  //1 for ON, 0 for OFF

#define DEFULTE_HIGH_ALARM 25
#define DEFULTE_LOW_ALARM 0
#define DEFULTE_HIGH_CRIT_ALARM 30
#define DEFULTE_LOW_CRIT_ALARM -5

#define LOW_VOLTAGE 3200
#define TOO_LOW_VOLTAGE 2300