#include "sensor.h"

SENSORS sensorsArray;

void turnOFFSensors(){
#ifdef DEBUG_PRINT
    U2SendArray("Sensor off\n");
#endif
#ifdef sensorONOFF    
    LATCbits.LC3 = 0;
    LATCbits.LC4 = 0;
    TRISCbits.RC3 = 1; //SCL TRIS = 0
    TRISCbits.RC4 = 1; //SDA TRIS = 0
    SENS_SWITCH_TRIS = 0;
    SENS_SWITCH_PORT = 0;
#endif    
}
void turnONSensors(){
#ifdef DEBUG_PRINT
    U2SendArray("Sensor on\n");
#endif
#ifdef sensorONOFF 
    SENS_SWITCH_TRIS = 0;
    SENS_SWITCH_PORT = 1;
#endif    
    I2Cinit(1);
}
uint8_t initSensors(){
    uint8_t ouput = getSensorsAsString(Debugbuf);
    return ouput;
}
uint8_t getSensorsAsString(char *buffer){
#ifdef ONE_WIRE
    char tempBuf[50];
#endif
    uint8_t output = 0;
#ifdef AVRIG_AND_SLOP
    uint8_t tempIndex = EEPROM_laod8Bit(MEASURMENT_INDEX_ADRESS);
#endif    
#ifdef HUMID_AND_TEMP // if humid & temp
    uint16_t tempRawHumi = getRawHumid();
    uint16_t tempRawTemp = getRawTemp(0);
    if(tempRawHumi != 0 && tempRawTemp != 0){
        output = 1;
        //Change
        if(tempIndex > TEMPBUF_LAST_ADRESS){tempIndex = TEMPBUF_1ST_ADRESS;}
        EEPROM_save16Bit(tempIndex, tempRawTemp);
        tempIndex += 2;        
        sprintf(buffer,"{\"1\":[%2.2f,%2.2f]", getTemp(tempRawTemp),getHumid(tempRawHumi));
#ifdef DEBUG_SENSORS
        U2SendArray(buffer);
        U2SendArray("\n");
#endif
    }
#endif
#ifdef TEMP_ONLY // if only temp
    uint16_t tempRawTemp = getRawTemp(1);
    if(tempRawTemp != 0){
        output = 1;
#ifdef AVRIG_AND_SLOP       
        if(tempIndex > TEMPBUF_LAST_ADRESS){tempIndex = TEMPBUF_1ST_ADRESS;}
        EEPROM_save16Bit(tempIndex, tempRawTemp);
        tempIndex += 2;
#endif        
        sprintf(buffer,"{\"1\":%2.2f", getTemp(tempRawTemp));
    }
#endif
#ifdef ONE_WIRE //There is one wire support
    if(OneWireReset()){//if one wire
        output |= 2;
        uint8_t addr[8], scratchpad[9];
        StartConvertion(); // Convert temperatures on DS18B20 sensors
        OneWireResetSearch(); //Reset one wire serch
        while(OneWireSearch(addr)!=0){ //As long as there are devices on the bus
            ReadScratchpad(scratchpad, addr);
            sprintf(tempBuf,",%2.2f",DS18B20GetTemp(scratchpad));
            strcat(buffer,tempBuf);
        }
    }
#endif
#ifdef AVRIG_AND_SLOP    
    EEPROM_save8Bit(MEASURMENT_INDEX_ADRESS, tempIndex);
#endif    
    strcat(buffer,"}}\n");
    
    updateSensorArray(tempRawTemp);
    return output;
}

void calculateSlopAndAvrige(){
    uint32_t avrige = 0;
    uint16_t temp = 0;
    uint8_t avriges = 0;
    for(uint8_t t = TEMPBUF_1ST_ADRESS; t < TEMPBUF_LAST_ADRESS; t++){
        temp = EEPROM_laod16Bit(t);
        t++;
        if(temp != 0xffff){
            avriges++;
            avrige += temp;
#ifdef DEBUG_SENSORS
            sprintf(Debugbuf2,"%d:%2.2f[%lu], ", t, getTemp(temp), avrige);
            U2SendArray(Debugbuf2);
#endif
        }
    }
    sensorsArray.Avrige = (uint16_t)(avrige/avriges);
    sensorsArray.Slope = getTemp(sensorsArray.Current) - getTemp(EEPROM_laod16Bit(EEPROM_laod8Bit(MEASURMENT_INDEX_ADRESS)));
}
void updateSensorArray(uint16_t rawTemp){
    sensorsArray.Current = rawTemp; //update current
    if((rawTemp > sensorsArray.Max) || (sensorsArray.Max == 0xffff)){sensorsArray.Max = rawTemp;}//update min
    if((rawTemp < sensorsArray.Min) || (sensorsArray.Max == 0xffff)){sensorsArray.Min = rawTemp;}//update max
    
    if((rawTemp > sensorsArray.AlarmHigh)||(rawTemp < sensorsArray.AlarmLow)){
        if(rawTemp > sensorsArray.AlarmHigh){sensorsArray.Flags |= HIGH_FLAG;}
        if(rawTemp < sensorsArray.AlarmLow){sensorsArray.Flags |= LOW_FLAG;}
        if((rawTemp > sensorsArray.AlarmCritHigh)||(rawTemp < sensorsArray.AlarmCritLow)){
        
            if(!(sensorsArray.Flags & ALARM_SEND)){
                sensorsArray.Flags |= ALARM_SEND;
                sensorsArray.Flags |= NEED_TO_SEND;
            }//Toggle alarm send
            sensorsArray.Flags &= ~NOT_ALARM_SEND;
            
            if(rawTemp > sensorsArray.AlarmCritHigh){sensorsArray.Flags |= HIGH_ALARM_FLAG;}
            if(rawTemp < sensorsArray.AlarmCritLow){sensorsArray.Flags |= LOW_ALARM_FLAG;}
        }else{
            if(sensorsArray.grayZoonTimer >= EEPROM_laod8Bit(FAST_SEND_INTERVAL_ADDRES)){
                
                if(!(sensorsArray.Flags & ALARM_SEND)){
                    sensorsArray.Flags |= ALARM_SEND;
                    sensorsArray.Flags |= NEED_TO_SEND;
                }//Toggle alarm send
                sensorsArray.Flags &= ~NOT_ALARM_SEND;
                
                if(rawTemp > sensorsArray.AlarmHigh){sensorsArray.Flags |= HIGH_ALARM_FLAG;}
                if(rawTemp < sensorsArray.AlarmLow){sensorsArray.Flags |= LOW_ALARM_FLAG;}
            }else{
                sensorsArray.grayZoonTimer++;
            }
        }
    }else{
        if(!(sensorsArray.Flags & NOT_ALARM_SEND)){
            sensorsArray.Flags |= NOT_ALARM_SEND;
            sensorsArray.Flags |= NEED_TO_SEND;
        }
        sensorsArray.Flags &= ~ALARM_SEND;
        
        sensorsArray.Flags &= ~HIGH_ALARM_FLAG;
        sensorsArray.Flags &= ~LOW_ALARM_FLAG;
        sensorsArray.Flags &= ~HIGH_FLAG;
        sensorsArray.Flags &= ~LOW_FLAG;
        sensorsArray.grayZoonTimer = 0;
    }
#ifdef AVRIG_AND_SLOP    
    calculateSlopAndAvrige();
#endif    
    saveSENSORStoEEPROM();
#ifdef DEBUG_SENSORS
    sprintf(Debugbuf2,"AH:%2.2f AL:%2.2f AV:%2.2f CU:%2.2f FL:%02x MA:%2.2f MI:%2.2f SL:%2.2f\n", getTemp(sensorsArray.AlarmHigh), getTemp(sensorsArray.AlarmLow), getTemp(sensorsArray.Avrige), getTemp(sensorsArray.Current), sensorsArray.Flags, getTemp(sensorsArray.Max), getTemp(sensorsArray.Min), sensorsArray.Slope);
    U2SendArray(Debugbuf2);
#endif
}
void resetSENSORS(){
    sensorsArray.Flags = 0;
    sensorsArray.grayZoonTimer = 0;
    sensorsArray.AlarmHigh = conToRawTemp(DEFULTE_HIGH_ALARM);
    sensorsArray.AlarmLow = conToRawTemp(DEFULTE_LOW_ALARM);
    sensorsArray.AlarmCritHigh = conToRawTemp(DEFULTE_HIGH_CRIT_ALARM);
    sensorsArray.AlarmCritLow = conToRawTemp(DEFULTE_LOW_CRIT_ALARM);
    sensorsArray.Avrige = 0xffff;
    sensorsArray.Current = 0xffff;
    sensorsArray.Max = 0xffff;
    sensorsArray.Min = 0xffff;
    sensorsArray.Slope = 0xffff;
    saveSENSORStoEEPROM();
}
void loadSENSORS(){
    sensorsArray.Flags = EEPROM_laod8Bit(SENSOR_ARRAY_ADRESS);
    sensorsArray.grayZoonTimer = EEPROM_laod8Bit(SENSOR_ARRAY_ADRESS + 1);
    sensorsArray.AlarmHigh =  EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 2);
    sensorsArray.AlarmLow = EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 4);
    sensorsArray.AlarmCritHigh =  EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 6);
    sensorsArray.AlarmCritLow = EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 8);
    sensorsArray.Avrige = EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 10);
    sensorsArray.Current = EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 12);
    sensorsArray.Max = EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 14);
    sensorsArray.Min = EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 16);
    sensorsArray.Slope = EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 18);
#ifdef DEBUG_SENSORS
    sprintf(Debugbuf2,"AH:%2.2f AL:%2.2f AV:%2.2f CU:%2.2f FL:%02x MA:%2.2f MI:%2.2f SL:%2.2f\n", getTemp(sensorsArray.AlarmHigh), getTemp(sensorsArray.AlarmLow), getTemp(sensorsArray.Avrige), getTemp(sensorsArray.Current), sensorsArray.Flags, getTemp(sensorsArray.Max), getTemp(sensorsArray.Min), getTemp(sensorsArray.Slope));
    U2SendArray(Debugbuf2);
#endif
}
void saveSENSORStoEEPROM(){
    if(EEPROM_laod8Bit(SENSOR_ARRAY_ADRESS)!= sensorsArray.Flags){EEPROM_save8Bit(SENSOR_ARRAY_ADRESS, sensorsArray.Flags);}
    if(EEPROM_laod8Bit(SENSOR_ARRAY_ADRESS + 1)!= sensorsArray.grayZoonTimer){EEPROM_save8Bit(SENSOR_ARRAY_ADRESS + 1, sensorsArray.grayZoonTimer);}
    if(EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 2)!= sensorsArray.AlarmHigh){EEPROM_save16Bit(SENSOR_ARRAY_ADRESS + 2, sensorsArray.AlarmHigh);}
    if(EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 4)!= sensorsArray.AlarmLow){EEPROM_save16Bit(SENSOR_ARRAY_ADRESS + 4, sensorsArray.AlarmLow);}
    if(EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 6)!= sensorsArray.AlarmCritHigh){EEPROM_save16Bit(SENSOR_ARRAY_ADRESS + 6, sensorsArray.AlarmCritHigh);}
    if(EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 8)!= sensorsArray.AlarmCritLow){EEPROM_save16Bit(SENSOR_ARRAY_ADRESS + 8, sensorsArray.AlarmCritLow);}
    if(EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 10)!= sensorsArray.Avrige){EEPROM_save16Bit(SENSOR_ARRAY_ADRESS + 10, sensorsArray.Avrige);}
    if(EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 12)!= sensorsArray.Current){EEPROM_save16Bit(SENSOR_ARRAY_ADRESS + 12, sensorsArray.Current);}
    if(EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 14)!= sensorsArray.Max){EEPROM_save16Bit(SENSOR_ARRAY_ADRESS + 14, sensorsArray.Max);}
    if(EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 16)!= sensorsArray.Min){EEPROM_save16Bit(SENSOR_ARRAY_ADRESS + 16, sensorsArray.Min);}
    if(EEPROM_laod16Bit(SENSOR_ARRAY_ADRESS + 18)!= sensorsArray.Slope){EEPROM_save16Bit(SENSOR_ARRAY_ADRESS + 18, sensorsArray.Slope);}
#ifdef DEBUG_SENSORS
    sprintf(Debugbuf2,"AH:%2.2f AL:%2.2f AV:%2.2f CU:%2.2f FL:%02x MA:%2.2f MI:%2.2f SL:%2.2f\n", getTemp(sensorsArray.AlarmHigh), getTemp(sensorsArray.AlarmLow), getTemp(sensorsArray.Avrige), getTemp(sensorsArray.Current), sensorsArray.Flags, getTemp(sensorsArray.Max), getTemp(sensorsArray.Min), getTemp(sensorsArray.Slope));
    U2SendArray(Debugbuf2);
#endif
}

uint16_t saveBatteryStatuse(){
    char temp[15];
    if(batteryVolt == 0){batteryVolt = getBat();}
    getTimeStempAsString(Debugbuf);
    sprintf(temp,"{\"0\":%1.3f}}\n", (float)batteryVolt/1000.0);
    strcat(Debugbuf, temp);
    FLASHWriteArray(Debugbuf);    
#ifdef DEBUG_SENSORS
    U2SendArray(Debugbuf);
#endif
    return batteryVolt;
}
uint8_t saveMeasuremntPoint(){
    char temp[50];
    char temp2[50];
    getTimeStempAsString(temp);
    uint8_t output = getSensorsAsString(temp2);
    turnOFFSensors();
    strcat(temp, temp2);
    FLASHWriteArray(temp);
#ifdef DEBUG_SENSORS
    U2SendArray(temp);
#endif
    return output;
}
void saveWifiStrength(){
    char temp[25];
    int strength = getSignalStrengthFromEeprom();
    if((strength <= 100)&&(strength >= 0)){
		getTimeStempAsString(Debugbuf);
		sprintf(temp,"{\"2\":%d}}\n", (/*(float)*/strength)/*/10.0*/);
		strcat(Debugbuf, temp);
		FLASHWriteArray(Debugbuf);
#ifdef DEBUG_SENSORS
		U2SendArray(Debugbuf);
#endif
    }else{
#ifdef DEBUG_PRINT
        U2SendArray("No valid ssignal strength value\n");
#endif
    }
}
void saveTimeDiff(int timeDiff){
    char temp[25];
    FLASHWriteArray("\n");
    getTimeStempAsString(Debugbuf);
    sprintf(temp,"{\"3\":%d}}\n", timeDiff);
    strcat(Debugbuf, temp);
    FLASHWriteArray(Debugbuf);
#ifdef DEBUG_SENSORS
    U2SendArray(Debugbuf);
#endif
}
void savePushEvent(){
    char temp[25];
    getTimeStempAsString(Debugbuf);
    sprintf(temp,"{\"4\":%d}}\n", EEPROM_laod8Bit(SPETIAL_LOG_FLAGS));
    strcat(Debugbuf, temp);
    FLASHWriteArray(Debugbuf);
#ifdef DEBUG_SENSORS
    U2SendArray(Debugbuf);
#endif
}
void saveSendErrors(){
    char temp[25];
    getTimeStempAsString(Debugbuf);
    sprintf(temp,"{\"5\":[%d,%d,%d,%d]}}\n", EEPROM_laod8Bit(MINOR_SEND_TIMEOUT_ADRESS),EEPROM_laod8Bit(MAJOR_SEND_TIMEOUT_ADRESS),EEPROM_laod8Bit(OVERALL_SEND_TIMEOUT_ADRESS),getError());
    strcat(Debugbuf, temp);
    FLASHWriteArray(Debugbuf);
#ifdef DEBUG_SENSORS
    U2SendArray(Debugbuf);
#endif
}
void saveWDTevent(uint8_t Akn){
    char temp[25];
    getTimeStempAsString(Debugbuf);
    sprintf(temp,"{\"6\":%d}}\n", Akn);
    strcat(Debugbuf, temp);
    FLASHWriteArray(Debugbuf);
#ifdef DEBUG_SENSORS
    U2SendArray(Debugbuf);
#endif
}
void saveSensorFlags(){
    char temp[25];
    getTimeStempAsString(Debugbuf);
    sprintf(temp,"{\"7\":[%d,%d]}}\n", sensorsArray.Flags, sensorsArray.grayZoonTimer);
    strcat(Debugbuf, temp);
    FLASHWriteArray(Debugbuf);
#ifdef DEBUG_SENSORS
    U2SendArray(Debugbuf);
#endif
}
void saveGeneral(const char *input){
    getTimeStempAsString(Debugbuf2);
    strcat(Debugbuf2, input);
    strcat(Debugbuf2, "}\n");
    FLASHWriteArray(Debugbuf2);
#ifdef DEBUG_SENSORS
    U2SendArray("General sen: ");
    U2SendArray(input);
    U2SendArray("\n");
#endif
}