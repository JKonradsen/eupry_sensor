#include "flash.h"

//FLASH functions
uint32_t FLASHStreamUpToToU1(long address, long amount, uint8_t encrypted){
    if((amount < 0)||(address < 0)){return 0;}
    long flashIndex = EEPROM_laod32Bit(FLASH_INDEX_ADRESS);
    long startIndex = EEPROM_laod32Bit(FLASH_START_INDEX_ADRESS);
    
    address += startIndex; //Returne from virtual addres to actual address on flash
    
    uint8_t output = 0;

#ifdef DEBUG_PRINT
    sprintf(Debugbuf2,"flashIndex %lu\n", flashIndex);
    U2SendArray(Debugbuf2);
    sprintf(Debugbuf2,"address %lu\n", address);
    U2SendArray(Debugbuf2);
    sprintf(Debugbuf2,"amount %lu\n", amount);
    U2SendArray(Debugbuf2);
    sprintf(Debugbuf2,"startIndex %lu\n", startIndex);
    U2SendArray(Debugbuf2);
#endif
    
    if(startIndex > flashIndex){flashIndex += FLASH_MAX_INDEX;} //To handle overflow of flash cases

    if(address + amount <= flashIndex){  //Everything is in the flash
        turnONFLASH();
#ifdef I2CFLASH
            uint8_t tempOutput = AT24CMReadStreamUpToToU1(tempAddres, ENCRYPTION_LENGTH ,Debugbuf);
            if(tempOutput == 0){output += ENCRYPTION_LENGTH;
            }else{output = tempOutput;}
#endif
#ifdef SPIFLASH
        output = AT25DFReadStreamUpToToU1(address, amount, 0, Debugbuf);
#endif
        turnOFFFLASH();
        amount = 0;
    }else if(address < flashIndex){  //A part is in EEPROM and a part is in Flash
        long leftOnFlash = flashIndex - address;
        int endOfFlash = leftOnFlash % ENCRYPTION_LENGTH;
        turnONFLASH();
#ifdef I2CFLASH
            uint8_t tempOutput = AT24CMReadStreamUpToToU1(tempAddres, ENCRYPTION_LENGTH, Debugbuf);
            if(tempOutput == 0){output += ENCRYPTION_LENGTH;
            }else{output = tempOutput;}
#endif
#ifdef SPIFLASH
        if(leftOnFlash >= ENCRYPTION_LENGTH){
            output = AT25DFReadStreamUpToToU1(address, leftOnFlash - endOfFlash, 0, Debugbuf);
        }
#endif
        address = 0; //reset for EEPROM start
        if(endOfFlash > 0){
#ifdef I2CFLASH
            AT24CMReadStreamUpToToU1(tempAddres, ENCRYPTION_LENGTH, Debugbuf);
#endif
#ifdef SPIFLASH
            AT25DFReadStreamUpToToU1(flashIndex - endOfFlash, endOfFlash, 1, Debugbuf);
#endif
            turnOFFFLASH();

            output = EEPROMReadStreamUpToToU1(address + LOCAL_MEASUREMENTS_OFFSET, ENCRYPTION_LENGTH - endOfFlash, Debugbuf2);
            if(output == 0){output = ENCRYPTION_LENGTH - endOfFlash;}
            address += ENCRYPTION_LENGTH - endOfFlash;

            strcat(Debugbuf, Debugbuf2);

            if(encrypted){encryptBlock(Debugbuf);}//Encrypts the 16 byte data string if necessary
            for(uint8_t t = 0; t < ENCRYPTION_LENGTH; t++){U1Send(Debugbuf[t]);}
            amount -= leftOnFlash + (ENCRYPTION_LENGTH - endOfFlash);
        }else{
            turnOFFFLASH();
            amount -= leftOnFlash;
        }

        while(amount > 0){
            uint8_t tempOutput = EEPROMReadStreamUpToToU1(address + LOCAL_MEASUREMENTS_OFFSET, ENCRYPTION_LENGTH, Debugbuf);
            if(tempOutput == 0){output += ENCRYPTION_LENGTH;
            }else{output = tempOutput;}

            if(encrypted == 1){encryptBlock(Debugbuf);}//Encrypts the 16 byte data string if necessary

            for(uint8_t t = 0; t < ENCRYPTION_LENGTH; t++){U1Send(Debugbuf[t]);}
            address += ENCRYPTION_LENGTH;
            amount -= ENCRYPTION_LENGTH;
        }

    }else{
        while(amount > 0){
            uint8_t tempOutput = EEPROMReadStreamUpToToU1((address - flashIndex) + LOCAL_MEASUREMENTS_OFFSET, ENCRYPTION_LENGTH, Debugbuf);
            if(tempOutput == 0){output += ENCRYPTION_LENGTH;
            }else{output = tempOutput;}

            if(encrypted == 1){encryptBlock(Debugbuf);}//Encrypts the 16 byte data string if necessary
            for(uint8_t t = 0; t < ENCRYPTION_LENGTH; t++){U1Send(Debugbuf[t]);}
            address += ENCRYPTION_LENGTH;
            amount -= ENCRYPTION_LENGTH;
        }
    }
    return output;
}
uint32_t FLASHStreamUpToToU2(uint32_t address, uint32_t amount){
    U2init();
    if(address > FLASH_MAX_INDEX){return 0;}
    if(amount == 0){return 0;}
    U2init();
#ifdef SPIFLASH
    return AT25DFReadStreamUpToToU2(address, amount);
#endif
#ifdef I2CFLASH
    return AT24CMReadStreamUpToToU2(address, amount);
#endif
}
uint8_t FLASHReadByte(uint32_t address) {
    if(address > FLASH_MAX_INDEX){return 0;}
    return _AT25DFRead(address);
}
uint32_t FLASHWriteArray(char *input) {
    uint16_t localIndex = EEPROM_laod16Bit(LOCAL_FLASH_INDEX_ADRESS);
    if((localIndex + MEASUREMNT_POINT_SIZE) < LOCAL_MEASUREMENTS_MAX_INDEX){
        EEPROM_save16Bit(LOCAL_FLASH_INDEX_ADRESS, EEPROMWriteArray(localIndex, input));
    }else{
        turnONFLASH();
        moveDataFromEEPROMtoFlash(input);
    }
    return 0;
}
void FLASHclear(){ // Reset all cells in flash memory
#ifdef DEBUG_PRINT
    U2SendArray("Clearing FLASH\n");
#endif
    EEPROM_save32Bit(FLASH_START_INDEX_ADRESS, EEPROM_laod32Bit(FLASH_INDEX_ADRESS));
    EEPROM_save32Bit(FLASH_INDEX_ADRESS, EEPROM_laod32Bit(FLASH_INDEX_ADRESS)+1);
    EEPROM_save32Bit(SEND_INDEX_ADRESS, 0);
    EEPROM_save16Bit(LOCAL_FLASH_INDEX_ADRESS, LOCAL_MEASUREMENTS_OFFSET);
}
void FLASHdeletPage(uint32_t address){
    AT25DFpageErase(address);
}
void turnOFFFLASH(){
#ifdef DEBUG_PRINT
    U2SendArray("FLASH off\n");
#endif
#ifdef newSPI
    FLASH_SWITCH_TRIS = 0;
    FLASH_SWITCH_PORT = 1;
#else
    FLASH_SWITCH_TRIS = 0;
    FLASH_SWITCH_PORT = 0;
#endif
}
void turnONFLASH(){
#ifdef DEBUG_PRINT
    U2SendArray("FLASH on\n");
#endif
#ifdef SPIFLASH
        SPIinit();
#endif
#ifdef I2CFLASH
        I2Cinit(1);
#endif
#ifdef newSPI
    FLASH_SWITCH_TRIS = 0;
    FLASH_SWITCH_PORT = 0;
#else
    FLASH_SWITCH_TRIS = 0;
    FLASH_SWITCH_PORT = 1;
#endif
    delay_2ms(20);
}

//Internal EEPROM functions
uint8_t EEPROM_laod8Bit(uint16_t address){ //Return 32bit from eeprom
    return ReadEEByte(address); //Uplaod flash index to write after it
}
void EEPROM_save8Bit(uint16_t address, uint8_t input){ //Save 32bit in eeprom        
    if(EEPROM_laod8Bit(address) != input){
        while(!WriteEEByte(address, input)){
            __delay_ms(5);
        }
    }
}
uint16_t EEPROM_laod16Bit(uint16_t LowAdd){ //Return 32bit from eeprom
    uint16_t Output = EEPROM_laod8Bit(LowAdd); //Uplaod flash index to write after it
    Output <<= 8;
    Output |= EEPROM_laod8Bit(LowAdd + 1);
    return Output;
}
void EEPROM_save16Bit(uint16_t LowAdd, uint16_t input){ //Save 32bit in eeprom
    EEPROM_save8Bit(LowAdd + 1,(input&0xff));//Save new flash index
    input >>= 8;
    EEPROM_save8Bit(LowAdd,(input&0xff));
}
uint32_t EEPROM_laod32Bit(uint16_t LowAdd){ //Return 32bit from eeprom
    uint32_t Output = 0;
    Output = EEPROM_laod8Bit(LowAdd); //Uplaod flash index to write after it
    Output <<= 8;
    Output |= EEPROM_laod8Bit(LowAdd + 1);
    Output <<= 8;
    Output |= EEPROM_laod8Bit(LowAdd + 2);
    Output <<= 8;
    Output |= EEPROM_laod8Bit(LowAdd +3);
    return Output;
}
void EEPROM_save32Bit(uint16_t LowAdd, uint32_t input){ //Save 32bit in eeprom
    EEPROM_save8Bit(LowAdd + 3,(input&0xff));//Save new flash index
    input >>= 8;
    EEPROM_save8Bit(LowAdd + 2,(input&0xff));
    input >>= 8;
    EEPROM_save8Bit(LowAdd + 1,(input&0xff));
    input >>= 8;
    EEPROM_save8Bit(LowAdd,(input&0xff));
}

uint16_t EEPROMWriteArray(uint16_t address, char *input){
    uint8_t maxLength = 50;
    while (*input && (maxLength-- != 0)) {
        EEPROM_save8Bit(address++,*input++);
    }
    return address;
}
uint32_t EEPROMReadStreamUpToToU1(uint16_t address, uint16_t amount, char *outputString) {
    if(amount == 0){return 0;}

    uint8_t ffCount = 0;
    uint32_t output = 0;
    uint8_t data = 0;

    if(amount > 0){
        for(ffCount = 0; ffCount < 5; ffCount++){
            data = EEPROM_laod8Bit(address++);
            while((data != 0xff) && (amount > 0)){
                if(data == '\n'){
                    output = amount;
                }
                //U1Send(data);
                *outputString++ = data;
                data = EEPROM_laod8Bit(address++);
                amount--;
                //clrwdt();
            }
            *outputString = 0;
        }
    }
    SPICSPORT = 1;
    return output;
}
uint32_t EEPROMReadStreamUpToToU2(uint16_t address, uint16_t amount) {
    uint8_t ffCount = 0;
    uint32_t output = 0;
    uint8_t data = 0;

    if(amount > 0){
        for(ffCount = 0; ffCount < 5; ffCount++){
            data = EEPROM_laod8Bit(address++);
            while((data != 0xff) && (amount > 0)){
                if((data == '\n')||(data == '\r')){
                    output = amount;
                }
                U2Send(data);
                data = EEPROM_laod8Bit(address++);
                amount--;
            }
        }
    }else{
        for(ffCount = 0; ffCount < 5; ffCount++){
            data = EEPROM_laod8Bit(address++);
            while(data != 0xff){
                if((data != '\n')&&(data != '\r')){
                    U2Send(data);
                }
                data = EEPROM_laod8Bit(address++);
            }
        }
    }
    SPICSPORT = 1;
    return output;
}
char *EEPROMReadToString(uint16_t address, char *input, uint16_t length){
    uint16_t temp = 0;
    while(temp < length){
        input[temp++] = EEPROM_laod8Bit(address++);
    }
    input[temp] = 0;
    return input;
}

void moveDataFromEEPROMtoFlash(char *input){
    uint16_t tempIndex = LOCAL_MEASUREMENTS_OFFSET;
    uint16_t localIndex = EEPROM_laod16Bit(LOCAL_FLASH_INDEX_ADRESS);

    while(tempIndex + 100 <= localIndex){

#ifdef SPIFLASH
        EEPROM_save32Bit(FLASH_INDEX_ADRESS,AT25DFWriteArraySlow(EEPROM_laod32Bit(FLASH_INDEX_ADRESS), EEPROMReadToString(tempIndex, Debugbuf2, 100)));
#endif
#ifdef I2CFLASH
        EEPROM_save32Bit(FLASH_INDEX_ADRESS,AT24CMWriteArray(EEPROM_laod32Bit(FLASH_INDEX_ADRESS), EEPROMReadToString(tempIndex, Debugbuf, 250)));
#endif
        tempIndex += 100;
    }
#ifdef SPIFLASH
    EEPROM_save32Bit(FLASH_INDEX_ADRESS,AT25DFWriteArraySlow(EEPROM_laod32Bit(FLASH_INDEX_ADRESS), EEPROMReadToString(tempIndex, Debugbuf2, localIndex - tempIndex)));
#endif
#ifdef I2CFLASH
    EEPROM_save32Bit(FLASH_INDEX_ADRESS,AT24CMWriteArray(EEPROM_laod32Bit(FLASH_INDEX_ADRESS), EEPROMReadToString(tempIndex, Debugbuf, localIndex - tempIndex)));
#endif

#ifdef SPIFLASH
    EEPROM_save32Bit(FLASH_INDEX_ADRESS,AT25DFWriteArraySlow(EEPROM_laod32Bit(FLASH_INDEX_ADRESS), input));
#endif
#ifdef I2CFLASH
        EEPROM_save32Bit(FLASH_INDEX_ADRESS,AT24CMWriteArray(EEPROM_laod32Bit(FLASH_INDEX_ADRESS), input));
#endif


#ifdef DEBUG_PRINT_FLASH
    U2SendArray("In flash now: \n");
    FLASHStreamUpToToU2(0, EEPROM_laod32Bit(FLASH_INDEX_ADRESS)-EEPROM_laod32Bit(FLASH_START_INDEX_ADRESS));
    if(EEPROM_laod32Bit(FLASH_INDEX_ADRESS) >= EEPROM_laod32Bit(FLASH_START_INDEX_ADRESS)){
        FLASHStreamUpToToU2(EEPROM_laod32Bit(FLASH_START_INDEX_ADRESS), EEPROM_laod32Bit(FLASH_INDEX_ADRESS)-EEPROM_laod32Bit(FLASH_START_INDEX_ADRESS));
    }else{
        FLASHStreamUpToToU2(EEPROM_laod32Bit(FLASH_START_INDEX_ADRESS), (FLASH_MAX_INDEX-EEPROM_laod32Bit(FLASH_START_INDEX_ADRESS))+EEPROM_laod32Bit(FLASH_INDEX_ADRESS));
    }
    U2SendArray("\n");
#endif
    EEPROM_save16Bit(LOCAL_FLASH_INDEX_ADRESS, LOCAL_MEASUREMENTS_OFFSET);
    
    //uint32_t sendIndex = EEPROM_laod32Bit(SEND_INDEX_ADRESS);
    uint32_t localIndex2 = (uint32_t)EEPROM_laod16Bit(LOCAL_FLASH_INDEX_ADRESS)-LOCAL_MEASUREMENTS_OFFSET;
    uint32_t startIndex = EEPROM_laod32Bit(FLASH_START_INDEX_ADRESS);
    uint32_t FLASHindex = EEPROM_laod32Bit(FLASH_INDEX_ADRESS);
    uint32_t amountToSend = 0;

    if(startIndex == FLASHindex){amountToSend = FLASH_MAX_INDEX + localIndex2;}
    else if(startIndex > FLASHindex){amountToSend = (FLASH_MAX_INDEX - startIndex) + FLASHindex + localIndex2;}
    else{amountToSend = (FLASHindex - startIndex) + localIndex2;}

#ifdef DEBUG_PRINT_FLASH    
    sprintf(Debugbuf,"sendIndex: %lu\n", sendIndex);
    U2SendArray(Debugbuf);
    sprintf(Debugbuf,"localIndex: %lu\n", localIndex2);
    U2SendArray(Debugbuf);
    sprintf(Debugbuf,"startIndex: %lu\n", startIndex);
    U2SendArray(Debugbuf);
    sprintf(Debugbuf,"FLASHindex: %lu\n", FLASHindex);
    U2SendArray(Debugbuf);
    sprintf(Debugbuf,"amountToSend: %lu\n", amountToSend);
    U2SendArray(Debugbuf);
#endif
}