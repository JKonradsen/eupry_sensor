#include "I2C.h"

void I2Cinit(uint8_t bus){
    if(bus == 1){
        TRISCbits.RC3 = 1; //SCL TRIS = 0
        TRISCbits.RC4 = 1; //SDA TRIS = 0
        SSP1ADD = 10;
        SSP1CON1 = 0b00101000;
        SSP1CON2 = 0b00000000;
        SSP1STAT = 0b01000000;
        while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W));
    }else if(bus == 2){
        TRISBbits.RB1 = 1; //SCL TRIS = 0
        TRISBbits.RB2 = 1; //SDA TRIS = 0
        SSP2ADD = 10;
        SSP2CON1 = 0b00101000;
        SSP2CON2 = 0b00000000;
        SSP2STAT = 0b01000000;
        while ((SSP2CON2 & 0x1F) | (SSP1STATbits.R_W));
    }
}

void _I2CSendByte(uint8_t input, uint8_t bus){
    if(bus == 1){
        uint8_t temp = SSP1BUF;
        SSP1BUF = input;
        while(SSP1STATbits.BF);
        while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W));
    }else if(bus == 2){
        uint8_t temp = SSP2BUF;
        SSP2BUF = input;
        while(SSP2STATbits.BF);
        while ((SSP2CON2 & 0x1F) | (SSP2STATbits.R_W));
    }
}
uint8_t _I2CgetByte(uint8_t bus){
    uint8_t temp = 0;
    while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W));
    if(bus==1){SSP1CON2bits.RCEN = 1;}else if(bus==2){SSP2CON2bits.RCEN = 1;}
    if(bus==1){temp = SSP1STATbits.BF;}else if(bus==2){temp = SSP2STATbits.BF;}
    while(!temp){
        if(bus==1){
            temp = SSP1STATbits.BF;
        }else if(bus==2){
            temp = SSP2STATbits.BF;
        }
    }
    if(bus==1){temp = SSP1BUF;}else if(bus==2){temp = SSP2BUF;}
    return temp;
}
void _I2CSendStop(uint8_t bus){
    if(bus == 1){
        while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W));
        SSP1CON2bits.PEN = 0;
        SSP1CON2bits.PEN = 1;
        while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W));
    }else if(bus == 2){
        while ((SSP2CON2 & 0x1F) | (SSP2STATbits.R_W));
        SSP2CON2bits.PEN = 0;
        SSP2CON2bits.PEN = 1;
        while ((SSP2CON2 & 0x1F) | (SSP2STATbits.R_W));
    }
}
void _I2CSendStart(uint8_t bus){
    if(bus == 1){
        SSP1CON2bits.SEN = 0;
        SSP1CON2bits.SEN = 1;
        while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W));
    }else if(bus == 2){
        SSP2CON2bits.SEN = 0;
        SSP2CON2bits.SEN = 1;
        while ((SSP2CON2 & 0x1F) | (SSP2STATbits.R_W));
    }
}
void _I2CSendRepeatedStart(uint8_t bus){
    if(bus == 1){
        SSP1CON2bits.RSEN = 0;
        SSP1CON2bits.RSEN = 1;
        while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W));
    }else if(bus == 2){
        SSP2CON2bits.RSEN = 0;
        SSP2CON2bits.RSEN = 1;
        while ((SSP2CON2 & 0x1F) | (SSP2STATbits.R_W));
    }
}
void _I2CSendACK(uint8_t NotACK, uint8_t bus){
    if(bus == 1){
        while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W));
        SSP1CON2bits.ACKDT = NotACK;
        SSP1CON2bits.ACKEN = 0;
        SSP1CON2bits.ACKEN = 1;
        while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W));
    }else if(bus == 2){
        while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W));
        SSP2CON2bits.ACKDT = NotACK;
        SSP2CON2bits.ACKEN = 0;
        SSP2CON2bits.ACKEN = 1;
        while ((SSP2CON2 & 0x1F) | (SSP2STATbits.R_W));
    }
}
uint8_t _I2CGetACK(uint8_t bus){
    if(bus == 1){
        while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W));
        return SSP1CON2bits.ACKSTAT;
    }else if(bus == 2){
        while ((SSP2CON2 & 0x1F) | (SSP2STATbits.R_W));
        return SSP2CON2bits.ACKSTAT;
    }
    return 0;
}

uint8_t I2CWriteByte(uint8_t device, uint8_t address, uint8_t data, uint8_t bus){
    _I2CSendStart(bus);
    _I2CSendByte(device, bus);
    if(_I2CGetACK(bus) == 0){
        _I2CSendByte(address, bus);
        if(_I2CGetACK(bus) == 0){
            _I2CSendByte(data, bus);
            if(_I2CGetACK(bus) == 0){
                _I2CSendStop(bus);
                return 1;
            }else{
                _I2CSendStop(bus);
                return 0;
            }
        }else{
            _I2CSendStop(bus);
            return 0;
        }
    }else{
        _I2CSendStop(bus);
        return 0;
    }
}
uint8_t I2CReadByte(uint8_t device, uint8_t address, uint8_t delay, uint8_t bus){
    uint8_t output;
    _I2CSendStart(bus);
    _I2CSendByte(device, bus);
    if(_I2CGetACK(bus) == 0){
        _I2CSendByte(address, bus);
        if(_I2CGetACK(bus) == 0){
            _I2CSendRepeatedStart(bus);
            delay_2ms(delay);
            _I2CSendByte(device+1, bus);
            if(_I2CGetACK(bus) == 0){
                output = _I2CgetByte(bus);
                _I2CSendACK(1, bus);
                _I2CSendStop(bus);
                return output;
            }else{
                _I2CSendStop(bus);
                return 0;
            }
        }else{
            _I2CSendStop(bus);
            return 0;
        }
    }else{
        _I2CSendStop(bus);
        return 0;
    }
}
uint16_t I2CReadWord(uint8_t device, uint8_t address, uint8_t delay, uint8_t bus){
    uint16_t output;
    _I2CSendStart(bus);
    _I2CSendByte(device, bus);
    if(_I2CGetACK(bus) == 0){
        _I2CSendByte(address, bus);
        if(_I2CGetACK(bus) == 0){
            _I2CSendRepeatedStart(bus);
            delay_2ms(delay);
            _I2CSendByte(device+1, bus);
            if(_I2CGetACK(bus) == 0){
                output = _I2CgetByte(bus);
                output <<= 8;
                _I2CSendACK(0, bus);
                output |= _I2CgetByte(bus);
                _I2CSendACK(1, bus);
                _I2CSendStop(bus);
                return output;
            }else{
                _I2CSendStop(bus);
                return 0;
            }
        }else{
            _I2CSendStop(bus);
            return 0;
        }
    }else{
        _I2CSendStop(bus);
        return 0;
    }
}

//uint8_t I2CFLASHWriteByte(uint8_t device, uint32_t address, uint8_t data, uint8_t bus){
//    _I2CSendStart(bus);
//    _I2CSendByte(device, bus);
//    if(_I2CGetACK(bus) == 0){
//        _I2CSendByte((address >> 8)&0x00ff, bus);
//        if(_I2CGetACK(bus) == 0){
//            _I2CSendByte(address&0x00ff, bus);
//            if(_I2CGetACK(bus) == 0){
//                _I2CSendByte(data, bus);
//                if(_I2CGetACK(bus) == 0){
//                    _I2CSendStop(bus);
//                    return 1;
//                }else{
//                    _I2CSendStop(bus);
//                    return 0;
//                }
//            }else{
//                _I2CSendStop(bus);
//                return 0;
//            }
//        }
//    }else{
//        _I2CSendStop(bus);
//        return 0;
//    }
//}
//uint8_t I2CFLASHReadByte(uint8_t device, uint32_t address, uint8_t bus){
//    uint8_t output;
//    _I2CSendStart(bus);
//    _I2CSendByte(device, bus);
//    if(_I2CGetACK(bus) == 0){
//        _I2CSendByte((address >> 8)&0x00ff, bus);
//        if(_I2CGetACK(bus) == 0){
//            _I2CSendByte(address&0x00ff, bus);
//            if(_I2CGetACK(bus) == 0){
//                _I2CSendRepeatedStart(bus);
//                _I2CSendByte(device+1, bus);
//                if(_I2CGetACK(bus) == 0){
//                    output = _I2CgetByte(bus);
//                    _I2CSendACK(1, bus);
//                    _I2CSendStop(bus);
//                    return output;
//                }else{
//                    _I2CSendStop(bus);
//                    return 0;
//                }
//            }else{
//                _I2CSendStop(bus);
//                return 0;
//            }
//        }
//    }else{
//        _I2CSendStop(bus);
//        return 0;
//    }
//}
//uint32_t I2CSendArray(uint8_t device, uint32_t address, uint8_t *data, uint8_t bus){
//
//    int pageOffset = (PAGE_SIZE - ((address+1) % PAGE_SIZE));
//
//    _I2CSendStart(bus);
//    _I2CSendByte(device, bus);
//    if(_I2CGetACK(bus) == 0){
//        _I2CSendByte((address >> 8)&0x00ff, bus);
//        if(_I2CGetACK(bus) == 0){
//            _I2CSendByte(address&0x00ff, bus);
//            if(_I2CGetACK(bus) == 0){
//                while(*data && pageOffset >= 0){
//                    _I2CSendByte(*data++, bus);
//                    if(_I2CGetACK(bus) == 0){
//                        address++;
//                        pageOffset--;
//                    }else{
//                        _I2CSendStop(bus);
//                        return 0;
//                    }
//                }
//                _I2CGetACK(bus);
//                _I2CSendStop(bus);
//
//                if(*data != 0){
//                    delay_2ms(10);
//                    _I2CSendStart(bus);
//                    _I2CSendByte(device, bus);
//                    if(_I2CGetACK(bus) == 0){
//                        _I2CSendByte((address >> 8)&0x00ff, bus);
//                        if(_I2CGetACK(bus) == 0){
//                            _I2CSendByte(address&0x00ff, bus);
//                            if(_I2CGetACK(bus) == 0){
//                                while(*data){
//                                    _I2CSendByte(*data++, bus);
//                                    if(_I2CGetACK(bus) == 0){
//                                        address++;
//                                    }else{
//                                        _I2CSendStop(bus);
//                                        return 0;
//                                    }
//                                }
//                                _I2CGetACK(bus);
//                                _I2CSendStop(bus);
//                                return address;
//                            }else{
//                                _I2CSendStop(bus);
//                                return 0;
//                            }
//                        }
//                    }else{
//                        _I2CSendStop(bus);
//                        return 0;
//                    }
//                }
//                return address;
//            }else{
//                _I2CSendStop(bus);
//                return 0;
//            }
//        }
//    }else{
//        _I2CSendStop(bus);
//        return 0;
//    }
//}