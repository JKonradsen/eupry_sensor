#ifndef AT25DF_H
#define	AT25DF_H

#include "main.h"

#ifndef SOFTSPI
    #define SPICSPORT LATCbits.LC0
    #define SPICSTRIS TRISCbits.RC0
#endif
#ifdef SOFTSPI
#ifdef newSPI
    #define SPICSPORT LATAbits.LA5
    #define SPICSTRIS TRISAbits.RA5
#else
    #define SPICSPORT LATAbits.LA6
    #define SPICSTRIS TRISAbits.RA6
#endif
#endif

#define FLASH_WRITE_ENABLE 0x06
#define FLASH_CHIP_ERASE 0xC7
#define FLASH_SLEEP_IN 0xb9
#define FLASH_SLEEP_OUT 0xab
#define FLASH_STATUS_READ 0x05
#define FLASH_STATUS_REG 0x01

#define FLASH_PAGE_ERASE 0x81
#define FLASH_BYTE_WRITE 0x02
#define FLASH_BYTE_WRITE_DUAL_MODE 0xA2
#define FLASH_READ_ARRAY 0x03

extern uint8_t flashOverRunBit;

//Local function
//Write one byte to AT25DF
void _AT25DFWrite(uint32_t address, uint8_t data);
//Read one byte from AT25DF
uint8_t _AT25DFRead(uint32_t address);
//Send command to AT25DF
void _AT25DFCommand(uint8_t command);
//Read beazy statuse from AT25DF
uint16_t _AT25DFReadStat(uint8_t command);
//Set data at command in AT25DF
void _AT25DFCommandData(uint8_t command, uint8_t data);
//Global function
void AT25DFpageErase(uint32_t addres);
//Read and streem to UATR1 or and output to string
uint32_t AT25DFReadStreamUpToToU1(long address, long amount, uint8_t saveToString, char *outputString);
//Read and streem to UATR2
uint32_t AT25DFReadStreamUpToToU2(uint32_t address, uint32_t amount);
//Write array in to AT25DF at a slow rate (resending addres every time)
uint32_t AT25DFWriteArraySlow(uint32_t addres, char *input);
//Clear AT25DF
void AT25DFClear();

#endif