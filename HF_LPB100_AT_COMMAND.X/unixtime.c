#include "unixtime.h"

TIME globalTime;

unsigned long getUnixTime(int year, int month, int day, int hour, int minute, int second){
    unsigned long timestamp = DateToTicks(year, month, day) + TimeToTicks(hour, minute, second);
    return timestamp;
}
void getTimeFields(uint32_t unixtime){

    globalTime.sec = unixtime % 60;
    unixtime /= 60;

    globalTime.min = unixtime % 60;
    unixtime /= 60;

    globalTime.hour = unixtime % 24;
    unixtime /= 24;

    unixtime = unixtime + 719499; //Add all the day sins Jusue was borne (30/1/0)

    for (globalTime.year = 1969; unixtime > YEAR_TO_DAYS(globalTime.year + 1) + 30; globalTime.year++);

    unixtime -= YEAR_TO_DAYS(globalTime.year);

    for (globalTime.mon = 1; globalTime.mon < 12 && unixtime > 367*(globalTime.mon+1)/12; globalTime.mon++);
    unixtime -= 367*globalTime.mon/12;
    globalTime.mon += 2;
    if (globalTime.mon > 12){globalTime.mon -= 12; globalTime.year++;}
    globalTime.day = unixtime;
    
    //globalTime.year -= 2000;
    
//    sprintf(Debugbuf,"From server:%d:%d:%d %lu/%lu/%lu\n", globalTime.hour,globalTime.min,globalTime.sec,globalTime.day,globalTime.mon,globalTime.year);
//    U2SendArray(Debugbuf);
    
}

// Even with 4 years leap, drift still occurs, and must be corrected using a 100 year non-leap and 400 year leap
// See: http://en.wikipedia.org/wiki/Leap_year#Algorithm
uint8_t IsLeapYear(int year){
  if ((year % 4) == 0){ // is divible by 4
    return 2; // last year leap was year
  }  
  year += 1; //As the calculation is from 1969 and 1969 is not leap year the 
  if ((year % 4) != 0){ // is divible by 4
    return 0; // common year
  }else if ((year % 100) != 0){ // and is not divible by 100
    return 1; // leap year
  }else if((year % 400) != 0){ // and is not divible by 400
    return 0; // common year
  }
  return 1; // leap year
}
long DateToTicks(int year, int month, int day){
  if (((year >= 1) && (year <= 9999)) && ((month >= 1) && (month <= 12))){
    if(IsLeapYear(year)==1){
//        U2SendArray("Is leap!\n");
      if ((day >= 1) && (day <= (DaysToMonth366[month] - DaysToMonth366[month - 1]))){
        int previousYear = year - 1;
        int daysInPreviousYears = ((((previousYear * 365) + (previousYear / 4)) - (previousYear / 100)) + (previousYear / 400));

        int totalDays = ((daysInPreviousYears + DaysToMonth366[month - 1]) + day) - 1;
        return (totalDays * 86400L);
      }
    }else if(IsLeapYear(year)==2){
//         U2SendArray("Last year was leap!\n");
      if ((day >= 1) && (day <= (DaysToMonth365[month] - DaysToMonth365[month - 1]))){
        int previousYear = year - 1;
        int daysInPreviousYears = ((((previousYear * 365) + (previousYear / 4)) - (previousYear / 100)) + (previousYear / 400));

        int totalDays = ((daysInPreviousYears + DaysToMonth365[month - 1]) + day);
        return (totalDays * 86400L);
      } 
    }else{
//        U2SendArray("Not leap!\n");
      if ((day >= 1) && (day <= (DaysToMonth365[month] - DaysToMonth365[month - 1]))){
        int previousYear = year - 1;
        int daysInPreviousYears = ((((previousYear * 365) + (previousYear / 4)) - (previousYear / 100)) + (previousYear / 400));

        int totalDays = ((daysInPreviousYears + DaysToMonth365[month - 1]) + day) - 1;
        return (totalDays * 86400L);
      }
    }
  }
  return 0;
}

long TimeToTicks(int hour, int minute, int second){
  long totalSeconds = ((hour * 3600L) + (minute * 60L)) + second;
  return (totalSeconds * TicksInSecond);
}