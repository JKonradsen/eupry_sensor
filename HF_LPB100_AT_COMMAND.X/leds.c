#include <pic18lf26k22.h>

#include "leds.h"

void lightALED(uint16_t red, uint16_t yellow, uint16_t green, uint8_t side){
    
    PWM1set(0);
    PWM2set(0);
    GREEN_LED_LAT = 0;
    YELLOW_LED_LAT = 0;
    RED_LED_LAT = 0;
   
    LEFT_LED_TRIS = 0;
    RIGHT_LED_TRIS = 0;
    RED_LED_TRIS = 0;
    
    LEFT_LED_LAT = 0;
    RIGHT_LED_LAT = 0;
    
    PWMinit();
    PWM1set(green);
    PWM2set(yellow);
    
    RED_LED_LAT = red;
    RIGHT_LED_LAT = (side & 0x01);
    LEFT_LED_LAT = ((side & 0x02)>>1);
}

void lightLEDs(uint8_t input, uint8_t delay, uint8_t DCDC){
    if(DCDC){
        if(getBat() < LOW_VOLTAGE){
            TRISCbits.RC5 = 0;
            LATCbits.LATC5 = 1;
            delay_2ms(50);
        }
    }
    switch (input){
        case OFF_OFF:
            lightALED(0, 0, 0, 0);
            break;
        case OFF_GREEN:
            for(uint16_t y = 0; y < 1024; y++){lightALED(0, 0, y, 2);delay_10us(delay<<2);}
            for(uint16_t y = 1023; y > 0; y--){lightALED(0, 0, y, 2);delay_10us(delay<<2);}
            break;            
        case OFF_YELLOW:
            for(uint16_t y = 0; y < 1024; y++){lightALED(0, y, 0, 2);delay_10us(delay<<2);}
            for(uint16_t y = 1023; y > 0; y--){lightALED(0, y, 0, 2);delay_10us(delay<<2);}
            break;
        case OFF_RED:
            for(uint16_t y = 0; y < 1024; y++){lightALED(1, 0, 0, 2);delay_10us(delay<<2);}
            for(uint16_t y = 1023; y > 0; y--){lightALED(1, 0, 0, 2);delay_10us(delay<<2);}
            break;
        case GREEN_OFF:
            for(uint16_t y = 0; y < 1024; y++){lightALED(0, 0, y, 1);delay_10us(delay<<2);}
            for(uint16_t y = 1023; y > 0; y--){lightALED(0, 0, y, 1);delay_10us(delay<<2);}
            break;
        case GREEN_GREEN:
            for(uint16_t y = 0; y < 1024; y++){lightALED(0, 0, y, 3);delay_10us(delay<<2);}
            for(uint16_t y = 1023; y > 0; y--){lightALED(0, 0, y, 3);delay_10us(delay<<2);}
            break;
        case GREEN_YELLOW:
            for(uint16_t y = 0; y < 1024; y++){
                lightALED(0, 0, y, 1);delay_10us(delay);
                lightALED(0, y, 0, 2);delay_10us(delay);
            }
            for(uint16_t y = 1023; y > 0; y--){
                lightALED(0, 0, y, 1);delay_10us(delay);
                lightALED(0, y, 0, 2);delay_10us(delay);
            }
            break;
        case GREEN_RED:
            for(uint16_t y = 0; y < 1024; y++){
                lightALED(0, 0, y, 1);delay_10us(delay);
                lightALED(1, 0, 0, 2);delay_10us(delay);
            }
            for(uint16_t y = 1023; y > 0; y--){
                lightALED(0, 0, y, 1);delay_10us(delay);
                lightALED(1, 0, 0, 2);delay_10us(delay);
            }
            break;
        case YELLOW_OFF:
            for(uint16_t y = 0; y < 1024; y++){lightALED(0, y, 0, 1);delay_10us(delay<<2);}
            for(uint16_t y = 1023; y > 0; y--){lightALED(0, y, 0, 1);delay_10us(delay<<2);}
            break;    
        case YELLOW_GREEN:
            for(uint16_t y = 0; y < 1024; y++){
                lightALED(0, y, 0, 1);delay_10us(delay);
                lightALED(0, 0, y, 2);delay_10us(delay);
            }
            for(uint16_t y = 1023; y > 0; y--){
                lightALED(0, y, 0, 1);delay_10us(delay);
                lightALED(0, 0, y, 2);delay_10us(delay);
            }
            break;
        case YELLOW_YELLOW:
            for(uint16_t y = 0; y < 1024; y++){lightALED(0, y, 0, 3);delay_10us(delay<<2);}
            for(uint16_t y = 1023; y > 0; y--){lightALED(0, y, 0, 3);delay_10us(delay<<2);}
            break;  
        case YELLOW_RED:
            for(uint16_t y = 0; y < 1024; y++){
                lightALED(0, y, 0, 1);delay_10us(delay);
                lightALED(1, 0, 0, 2);delay_10us(delay);
            }
            for(uint16_t y = 1023; y > 0; y--){
                lightALED(0, y, 0, 1);delay_10us(delay);
                lightALED(1, 0, 0, 2);delay_10us(delay);
            }
            break; 
        case RED_OFF:
            for(uint16_t y = 0; y < 1024; y++){lightALED(1, 0, 0, 1);delay_10us(delay<<2);}
            for(uint16_t y = 0; y < 1024; y++){lightALED(1, 0, 0, 1);delay_10us(delay<<2);}
            break;
        case RED_GREEN:
            for(uint16_t y = 0; y < 1024; y++){
                lightALED(1, 0, 0, 1);delay_10us(delay);
                lightALED(0, 0, y, 2);delay_10us(delay);
            }
            for(uint16_t y = 1023; y > 0; y--){
                lightALED(1, 0, 0, 1);delay_10us(delay);
                lightALED(0, 0, y, 2);delay_10us(delay);
            }
            break; 
        case RED_YELLOW:
            for(uint16_t y = 0; y < 1024; y++){
                lightALED(1, 0, 0, 1);delay_10us(delay);
                lightALED(0, y, 0, 2);delay_10us(delay);
            }
            for(uint16_t y = 1023; y > 0; y--){
                lightALED(1, 0, 0, 1);delay_10us(delay);
                lightALED(0, y, 0, 2);delay_10us(delay);
            }
            break; 
        case RED_RED:
            for(uint16_t y = 0; y < 1024; y++){lightALED(1, 0, 0, 3);delay_10us(delay);}
            for(uint16_t y = 0; y < 1024; y++){lightALED(1, 0, 0, 3);delay_10us(delay);}
            break;
        default:
            break;
    }
    lightALED(0, 0, 0, 0);
}