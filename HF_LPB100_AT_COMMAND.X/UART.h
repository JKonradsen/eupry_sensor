#ifndef UART_H
#define	UART_H

#define U1bufferSize 256
#define U2bufferSize 257

#include "main.h"

void U1ClearErrors();
void U1init();
void U1Send(const char input);
void U1SendArray(const char *input);

void U2ClearErrors();
void U2init();
void U2Send(const char input);
void U2SendArray(const char *input);
void U2SendDebug(const char *input);

#endif

