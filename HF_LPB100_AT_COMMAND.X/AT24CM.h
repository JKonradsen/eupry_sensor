#ifndef AT24CM_H
#define	AT24CM_H

#define AT24_ADD 0xa8
#define AT24_BUS 1

#include "main.h"

//Read to UART1 and output as a string from flash
uint32_t AT24CMReadStreamUpToToU1(uint32_t address, uint32_t amount, char *outputString);
//Read to UART2 from flash
uint32_t AT24CMReadStreamUpToToU2(uint32_t address, uint32_t amount);
//Write array in to flash
uint32_t AT24CMWriteArray(uint32_t addres, char *input);

#endif	

