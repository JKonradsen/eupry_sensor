#include "UART.h"

void U1ClearErrors(){
    RCREG1 = 0; //Clear buffer
    char temp = RCSTA1; //Clear FERR
    RCSTA1 = 0;  //Clear OERR
    RCSTA1 = 0b10010000; //Back to normal
}
void U1init(){
    LATCbits.LC7 = 0;
    TRISCbits.RC7 = 1;
    TXSTA1 = 0b00100100; //Enable Async at High Speed
    RCSTA1 = 0b10010000; //Enable
    BAUDCON1 = 0b00001000; //Enable 16bit Baud Rate
    SPBRG1 = 34; //Boadrate 115200 at 16Mhz clk
    SPBRGH1 = 0;
}
void U1Send(const char input){
    TXREG1 = input;
    while((TXSTA1&0b00000010)==0); // Wait until transmit is finished
}
void U1SendArray(const char *input){
    while(*input){
       U1Send(*input++);
    }
}

void U2ClearErrors(){
    RCREG2 = 0; //Clear buffer
    char temp = RCSTA2; //Clear FERR
    RCSTA2 = 0;  //Clear OERR
    RCSTA2 = 0b10010000; //Back to normal
}
void U2init(){
    TRISBbits.RB7 = 1;
    TXSTA2 = 0b00100100; //Enable Async High Speed
    RCSTA2 = 0b10010000; //Enable
    BAUDCON2 = 0b00001000; //Enable 16bit Baud Rate
    SPBRG2 = 34; //Boadrate 115200 at 16Mhz clk
    SPBRGH2 = 0;
}
void U2dis(){
    TXSTA2 = 0; //Enable Async High Speed
    RCSTA2 = 0; //Enable
    BAUDCON2 = 0; //Enable 16bit Baud Rate
    SPBRG2 = 0; //Boadrate 115200 at 16Mhz clk
    SPBRGH2 = 0;
    TRISBbits.RB6 = 0;
    TRISBbits.RB7 = 0;
    LATBbits.LATB6 = 0;
    LATBbits.LATB7 = 0;
}
void U2disTransmiter(){
    TXSTA2 = 0b00000100; //Enable Async High Speed
}

void U2Send(const char input){
    TXREG2 = input;
    while((TXSTA2&0b00000010)==0); // Wait until transmit is finished
}
void U2SendArray(const char *input){
    U2init();
    delay_2ms(1);
    U2ClearErrors();
    while(*input){
       U2Send(*input++);
    }
    U2dis();
}
void U2SendDebug(const char *input){
    __delay_us(20);
    U2ClearErrors();
    while(*input){
       U2Send(*input++);
    }
}

