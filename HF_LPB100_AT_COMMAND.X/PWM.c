#include "PWM.h"

void PWMinit(){
    //LEDs
    PORTCbits.RC1 = 0;
    PORTCbits.RC2 = 0;
    TRISCbits.RC1 = 0; //LED0
    TRISCbits.RC2 = 0; //LED1
    PR2 = 0xFF;//15,625 kHz
    T2CON |= 0x04; //Timer 2 ON
}

void PWM1set(uint16_t input){
    if(input!=0){
        input &= 0x03ff;
        CCP1CON = 0;
        CCPR1L = (input >> 2) & 0x03fc;
        CCP1CON |= (input << 4) & 0b00110000;
        CCP1CON |= 0b00001100;
    }else{
        CCP1CON = 0;
        CCPR1L = 0;
        PORTCbits.RC2 = 0;
    }
}
void PWM2set(uint16_t input){
    if(input!=0){
        input &= 0x03ff;
        CCP2CON = 0;
        CCPR2L = (input >> 2) & 0x03fc;
        CCP2CON |= (input << 4) & 0b00110000;
        CCP2CON |= 0b00001100;
    }else{
        CCP2CON = 0;
        CCPR2L = 0;
        PORTCbits.RC1 = 0;
    }
}