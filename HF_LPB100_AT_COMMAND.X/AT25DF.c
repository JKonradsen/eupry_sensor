#include "AT25DF.h"

uint8_t flashOverRunBit = 0;

uint8_t _AT25DFRead(uint32_t address) {
    uint8_t data = 0;
    SPICSTRIS = 0;
    SPICSPORT = 0;
    address |= 0x03000000;

    SPISendByte((address >> 24)&0x000000ff);
    SPISendByte((address >> 16)&0x000000ff);
    SPISendByte((address >> 8)&0x000000ff);
    SPISendByte((address)&0x000000ff);

    data = SPISendByte(0);

    SPICSPORT = 1;
    return data;
}
void _AT25DFWrite(uint32_t address, uint8_t data) {
     while((_AT25DFReadStat(FLASH_STATUS_READ)&0x0001)!=0){
        __delay_us(10);
        U2SendArray("W\n");
    }
    _AT25DFCommand(FLASH_WRITE_ENABLE); //Remove protection
    SPICSTRIS = 0;
    SPICSPORT = 0;
    address |= 0x02000000;

    SPISendByte((address >> 24)&0x000000ff);
    SPISendByte((address >> 16)&0x000000ff);
    SPISendByte((address >> 8)&0x000000ff);
    SPISendByte((address)&0x000000ff);

    SPISendByte(data);

    SPICSPORT = 1;
    __delay_ms(1);
}
void _AT25DFCommand(uint8_t command) {
    SPICSTRIS = 0;
    SPICSPORT = 0;
    SPISendByte(command);
    SPICSPORT = 1;
}
void _AT25DFCommandData(uint8_t command, uint8_t data) {
    _AT25DFCommand(FLASH_WRITE_ENABLE); //Remove protection
    SPICSTRIS = 0;
    SPICSPORT = 0;
    SPISendByte(command);
    SPISendByte(data);
    SPICSPORT = 1;
}
uint16_t _AT25DFReadStat(uint8_t command) {
    uint16_t data;
    SPICSTRIS = 0;
    SPICSPORT = 0;
    SPISendByte(command);

    data = SPISendByte(0);
    data <<= 8;
    data |= SPISendByte(0);

    SPISendByte(0);
    SPICSPORT = 1;
    return data;
}

void AT25DFpageErase(uint32_t addres){
    _AT25DFCommand(FLASH_SLEEP_OUT); //Sleepout
    _AT25DFCommandData(FLASH_STATUS_REG, 1); //Unblock flash
    _AT25DFCommand(FLASH_WRITE_ENABLE); //Open flash

    SPICSTRIS = 0;
    SPICSPORT = 0;

    SPISendByte(FLASH_PAGE_ERASE);
    SPISendByte((addres >> 16)&0x000000ff);
    SPISendByte((addres >> 8)&0x000000ff);
    SPISendByte(0);

    SPICSPORT = 1;
    __delay_ms(20);
    __delay_ms(20);
    __delay_ms(20);

    while((_AT25DFReadStat(FLASH_STATUS_READ)&0x0001)!=0){
        U2SendArray("P\n");
        delay_2ms(5);
    }
    __delay_ms(20);
    __delay_ms(20);
}
uint32_t AT25DFReadStreamUpToToU1(long address, long amount, uint8_t saveToString, char *outputString) {
    if((amount < 0)||(address < 0)){return 0;}
    if (address > FLASH_MAX_INDEX){address = address - FLASH_MAX_INDEX;}//If the addres + startIndex is more the then flash length that say that we are in the start of the flash
    _AT25DFCommand(FLASH_SLEEP_OUT); //Sleepout
    char dataString[ENCRYPTION_LENGTH];
    uint8_t dataIndex = 0;
    uint32_t output = 0;
    uint8_t data = 0;
    SPICSTRIS = 0;
    SPICSPORT = 0;

    SPISendByte(FLASH_READ_ARRAY);
    SPISendByte((address >> 16)&0x000000ff);
    SPISendByte((address >> 8)&0x000000ff);
    SPISendByte((address)&0x000000ff);

    data = SPISendByte(0);
    U1intFlag = 0;
    while((amount > 0)&&(!U1intFlag)){
        if((data == '\n')){output = amount;}
        if(saveToString){*outputString++ = data;}
        else{
            dataString[dataIndex++] = data;
            if(dataIndex == ENCRYPTION_LENGTH){
                dataIndex = 0;
                if(USE_ENCRYPTION){encryptBlock(dataString);}//Encrypts the 16 byte data string if necessary
                for(uint8_t t = 0; t < ENCRYPTION_LENGTH; t++){U1Send(dataString[t]);}
            }
        }
        data = SPISendByte(0);
        amount--;
    }
    SPICSPORT = 1;
    *outputString = 0;
    return output;
}
uint32_t AT25DFReadStreamUpToToU2(uint32_t address, uint32_t amount){
    if(amount == 0){return 0;}
    if (address > FLASH_MAX_INDEX){address = address - FLASH_MAX_INDEX;}
    _AT25DFCommand(FLASH_SLEEP_OUT); //Sleepout
    uint32_t output = 0;
    uint8_t data = 0;
    SPICSTRIS = 0;
    SPICSPORT = 0;

    SPISendByte(FLASH_READ_ARRAY);
    SPISendByte((address >> 16)&0x000000ff);
    SPISendByte((address >> 8)&0x000000ff);
    SPISendByte((address)&0x000000ff);

    U2init();
    data = SPISendByte(0);
    while(amount > 0){
        if(data == '\n'){output = amount;}
        U2Send(data);
        data = SPISendByte(0);
        amount--;
    }
    U2dis();
    SPICSPORT = 1;
    return output;
}
uint32_t AT25DFWriteArraySlow(uint32_t addres, char *input) {

    _AT25DFCommand(FLASH_SLEEP_OUT); //Sleepout
    _AT25DFCommandData(FLASH_STATUS_REG, 1); //Unblock flash

    uint32_t currentStartIndex = EEPROM_laod32Bit(FLASH_START_INDEX_ADRESS);
    
    if(addres >= FLASH_MAX_INDEX){addres = 0;}//Test if addres is out of bounderis, if yes move it to zero 
    
    if(addres % PAGE_SIZE == 0){ //Erase new page 
        if((currentStartIndex >= addres)&&(currentStartIndex < (addres + PAGE_SIZE))){ //If start pointer in the earising page move it to the end of the page
            EEPROM_save32Bit(FLASH_START_INDEX_ADRESS, addres + PAGE_SIZE);
        }
        AT25DFpageErase(addres); //Erase the current page
    }
    
    uint8_t indexCount = PAGE_SIZE - (addres % PAGE_SIZE); //Calculate space in current page

    while(*input){

        _AT25DFCommand(FLASH_WRITE_ENABLE); //Remove protection

        SPICSTRIS = 0;
        SPICSPORT = 0;

        uint32_t tempAddress = addres;
        SPISendByte(FLASH_BYTE_WRITE_DUAL_MODE);
        SPISendByte((tempAddress >> 16)&0x000000ff);
        SPISendByte((tempAddress >> 8)&0x000000ff);
        SPISendByte((tempAddress)&0x000000ff);
        
        SPISendByteDualMode(*input++);
        indexCount--;
        addres++;

        while((indexCount != 0) && (*input)){ //As long as there is input and space in the page
            SPISendByteDualMode(*input++);
            indexCount--;
            addres++;
        }
        SPICSPORT = 1;
        delay_2ms(15);
        while((_AT25DFReadStat(FLASH_STATUS_READ)&0x0001)!=0){
            U2SendArray("F\n");
            delay_2ms(5);
        }
        if(indexCount == 0){
            if(addres >= FLASH_MAX_INDEX){addres = 0;}
            if((currentStartIndex >= addres)&&(currentStartIndex < (addres + PAGE_SIZE))){ //If start pointer in the earising page move it to the start of the next page
                EEPROM_save32Bit(FLASH_START_INDEX_ADRESS, addres + PAGE_SIZE);
                flashOverRunBit = 1;
            }
            AT25DFpageErase(addres);
        }
    }
    ClrWdt();
    return addres;
}
void AT25DFClear(){ // Reset all cells in flash memory
    _AT25DFCommand(FLASH_SLEEP_OUT); //Sleepout
    _AT25DFCommandData(FLASH_STATUS_REG, 1); //Unblock flash
    _AT25DFCommand(FLASH_WRITE_ENABLE); //Open flash
    _AT25DFCommand(FLASH_CHIP_ERASE); //Eresa flash
    ClrWdt();
    delay_2ms(5000);
    ClrWdt();
    while((_AT25DFReadStat(FLASH_STATUS_READ)&0x0001)!=0){
        U2SendArray("C\n");
        delay_2ms(500);
    }
}