#include "ONEWIRE.h"

uint8_t ROM_NO[8];
uint8_t LastDiscrepancy;
uint8_t LastFamilyDiscrepancy;
uint8_t LastDeviceFlag;

void delay480() {
    __delay_us(480);
}
uint8_t CRConebyte(uint8_t oldcrc, uint8_t newbyte) {
   // see http://pdfserv.maxim-ic.com/arpdf/AppNotes/app27.pdf
    uint8_t shift_reg, data_bit, sr_lsb, fb_bit, j;
    shift_reg=oldcrc;
    for(j=0; j<8; j++) {   // for each bit
        data_bit = (newbyte >> j) & 0x01;
        sr_lsb = shift_reg & 0x01;
        fb_bit = (data_bit ^ sr_lsb) & 0x01;
        shift_reg = shift_reg >> 1;
        if (fb_bit){shift_reg = shift_reg ^ 0x8c;}
    }
    return(shift_reg);
}
void StartConvertion(){
    int timeout = 10000;

    OneWireReset();
    OneWireWrite(ONEWIRE_SKIP_ROM); // 0xCC - address all devices
    OneWireWrite(SI70_CONVERT_TEMP); // 0x44 - start temperature conversion on all devices
      
    while(OneWireRead() == 0 && timeout > 0){timeout--;__delay_ms(1);/*clrwdt();*/} //Wait to the end of convertion

    //    OneWireReset();
//    OneWireWrite(SKIP_ROM); // skip rom
//    OneWireWrite(CONVERT_VOLT); // 0xb4 convert voltage from battery monitor
//    for(int t = 0; t < 50; t++){__delay_ms(3);//clrwdt();}
}
uint8_t OneWireReset(){
    //interupOff();//Close all interupts but UART while using one wire
    ONEWIRE_TRIS = 0; // Tris = 0 (output)
    ONEWIRE_PIN = 0; // set pin# to low (0)
    __delay_us(500); // 1 wire require time delay
    ONEWIRE_TRIS = 1; // Tris = 1 (input)
    __delay_us(100); // 1 wire require time delay

    if(ONEWIRE_PIN == 0){ // if there is a presence pluse
        //interupOn();//Turn all interup back on
        __delay_us(360);
        return 1; // return 0 ( 1-wire is present)
    }else{
        //interupOn();//Turn all interup back on
        __delay_us(360);
        return 0; // return 1 ( 1-wire is NOT present)
    }
    //interupOn();
}
void OneWireWritebit(uint8_t data){
    //interupOff();//Close all interupts but UART while using one wire
    ONEWIRE_TRIS = 0; // set pin# to output (0)
    ONEWIRE_PIN = 0; // set pin# to low (0)
    if(data != 0) {// write 1
        __delay_us(5); // 1 wire require time delay
        ONEWIRE_TRIS = 1;//ONEWIRE_PIN = 1; //
        __delay_us(60); // 1 wire require time delay
    }else{//write 0
        __delay_us(59); // 1 wire require time delay
        ONEWIRE_TRIS = 1;//ONEWIRE_PIN = 1; //
        __delay_us(9);
    }
    //interupOn();//Turn all interup back on
}
uint8_t OneWireReadbit(){
    uint8_t back;
    //interupOff();//Close all interupts but UART while using one wire
    ONEWIRE_TRIS = 0; // TRIS is output(0)
    ONEWIRE_PIN = 0; // genarate low pluse for 2us
    __delay_us(3);
    ONEWIRE_TRIS = 1; // TRIS is input(1) release the bus
    __delay_us(9);
    back = ONEWIRE_PIN;
    //interupOn();//Turn all interup back on
    __delay_us(55); // wait for recovery time
    return back;
}
void OneWireWrite (uint8_t Cmd){
    char i;
    for(i = 0; i < 8; i++){OneWireWritebit((Cmd & (1<<i)));}
}
uint8_t OneWireRead(){
    char result = 0,i;
    for(i = 0; i < 8; i++){result >>= 1;result |= (OneWireReadbit()<<(7));} //Read 8 bitss
    return result;
}
void OneWireResetSearch(){
  // reset the search state
    int i;
  LastDiscrepancy = 0;
  LastDeviceFlag = 0;
  LastFamilyDiscrepancy = 0;
  for(i = 7; ; i--) {
    ROM_NO[i] = 0;
    if ( i == 0) break;
  }
}
uint8_t OneWireSearch(uint8_t *newAddr){
  uint8_t id_bit_number;
  uint8_t last_zero, rom_byte_number, search_result;
  uint8_t id_bit, cmp_id_bit;

  int i;
  unsigned char rom_byte_mask, search_direction;

  // initialize for search
  id_bit_number = 1;
  last_zero = 0;
  rom_byte_number = 0;
  rom_byte_mask = 1;
  search_result = 0;

  //clrwdt();
  // if the last call was not the last one
  if (!LastDeviceFlag)
  {
    // 1-Wire reset
    if (!OneWireReset())
    {
      // reset the search
      LastDiscrepancy = 0;
      LastDeviceFlag = 0;
      LastFamilyDiscrepancy = 0;
      return 0;
    }
    // issue the search command
    OneWireWrite(ONEWIRE_SEARCH_ROM); // 0xF0
    // loop to do the search
    do
    {
      // read a bit and its complement
      id_bit = OneWireReadbit();
      cmp_id_bit = OneWireReadbit();

      // check for no devices on 1-wire
      if ((id_bit == 1) && (cmp_id_bit == 1))
        break;
      else
      {
        // all devices coupled have 0 or 1
        if (id_bit != cmp_id_bit)
           search_direction = id_bit;  // bit write value for search
        else
        {
          // if this discrepancy if before the Last Discrepancy
          // on a previous next then pick the same as last time
          if (id_bit_number < LastDiscrepancy)
            search_direction = ((ROM_NO[rom_byte_number] & rom_byte_mask) > 0);
          else
            // if equal to last pick 1, if not then pick 0
            search_direction = (id_bit_number == LastDiscrepancy);

          // if 0 was picked then record its position in LastZero
          if (search_direction == 0)
          {
            last_zero = id_bit_number;

            // check for Last discrepancy in family
            if (last_zero < 9)
            LastFamilyDiscrepancy = last_zero;
          }
        }

        // set or clear the bit in the ROM byte rom_byte_number
        // with mask rom_byte_mask
        if (search_direction == 1)
          ROM_NO[rom_byte_number] |= rom_byte_mask;
        else
          ROM_NO[rom_byte_number] &= ~rom_byte_mask;

        // serial number search direction write bit
        OneWireWritebit(search_direction);

        // increment the byte counter id_bit_number
        // and shift the mask rom_byte_mask
        id_bit_number++;
        rom_byte_mask <<= 1;

        // if the mask is 0 then go to new SerialNum byte rom_byte_number and reset mask
        if (rom_byte_mask == 0)
        {
          rom_byte_number++;
          rom_byte_mask = 1;
        }
      }
    }
    while(rom_byte_number < 8); // loop until through all ROM bytes 0-7

    // if the search was successful then
    if (!(id_bit_number < 65))
    {
       // search successful so set LastDiscrepancy,LastDeviceFlag,search_result
       LastDiscrepancy = last_zero;

       // check for last device
       if (LastDiscrepancy == 0)
          LastDeviceFlag = 1;

       search_result = 1;
    }
  }

  // if no device found then reset counters so next 'search' will be like a first
  if (!search_result || !ROM_NO[0])
  {
    LastDiscrepancy = 0;
    LastDeviceFlag = 0;
    LastFamilyDiscrepancy = 0;
    search_result = 0;
  }
  for (i = 0; i < 8; i++) newAddr[i] = ROM_NO[i];
  return search_result;
}
void OneWireSelect(uint8_t add[8]){
  uint8_t i;
  OneWireWrite(ONEWIRE_MATCH_ROM); // Address a single slave device
  for (i = 0; i < 8; i++){
    OneWireWrite(add[i]);
  }
}
uint8_t ReadScratchpad(uint8_t *dataout, uint8_t add[8]){
    uint8_t sucssec = 0, tries = 50, y; //Amout of read tries bofer abord
    while((sucssec == 0) && (tries > 0)){
        //clrwdt();
        tries--;
        OneWireReset(); //Reset wire            Maybe can be removed in order to save time
        OneWireSelect(add); //Call device
        OneWireWrite(ONEWIRE_READ_SCRATCHPAD); // 0xBE
        uint8_t temp = 0, lasttemp = 0; //Reset CRC cheak variabules
        for(y = 0; y < 8; y++){
            temp = OneWireRead();
            dataout[y] = temp;
            lasttemp = CRConebyte(lasttemp, temp); //Calculate CRC
        }
        dataout[8] = OneWireRead();
        if(dataout[8]==lasttemp){sucssec = 1;} //If CRC == byte 9 then read was succsecufule
#ifdef ONEWIRE_DEBUG
        sprintf(Debugbuf,"%d %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n",sucssec,dataout[0],dataout[1],dataout[2],dataout[3],dataout[4],dataout[5],dataout[6],dataout[7],dataout[8],lasttemp);
        hostSendArray(Debugbuf);
        //clrwdt();
#endif
    }
    if(sucssec){return 1;}else{return 0;}
}

/*
uint8_t addr[8], buffer[9];
        StartConvertion(); // Convert temperatures on DS18B20 sensors
        OneWireResetSearch(); //Reset one wire serch
        while(OneWireSearch(addr)!=0){ //As long as there are devices on the bus
            //sprintf(Debugbuf,"%02x %02x %02x %02x %02x %02x %02x %02x\n",addr[0],addr[1],addr[2],addr[3],addr[4],addr[5],addr[6],addr[7]);
            //U1SendArray(Debugbuf);
            ReadScratchpad(buffer, addr);
            sprintf(Debugbuf,"T: %2.2f\n",DS18B20GetTemp(((buffer[1]<<8)|buffer[0])));
            U1SendArray(Debugbuf);
            //clrwdt();
        }
*/

uint8_t getDoorStat(uint8_t addr[8]){ //Need to move to door chip
    OneWireReset();
    OneWireSelect(addr);
    OneWireWrite(0xf5); // ReadIO
    uint8_t out = OneWireRead();
    OneWireReset();
    if(out == 0x1e){
        return 1;
    }else{
        return 0;
    }
}

/*
uint8_t getBatVolt(uint8_t addr[8]){ //Need to move to bat chips
    uint8_t databuf[9];
//    OneWireReset();
//    OneWireSelect(addr);
//    OneWireWrite(0x44); // Temp convert
//    for(int t = 0; t < 100; t++){__delay_ms(1);}
    OneWireReset();
    OneWireSelect(addr);
    OneWireWrite(0xB4); // Volt convert
    for(int t = 0; t < 100; t++){__delay_ms(1);}

    OneWireReset();
    OneWireSelect(addr);
    OneWireWrite(0xB8); //Recallscratch
    OneWireWrite(0x00);

    OneWireReset();
    OneWireSelect(addr);
    OneWireWrite(0xBE); //Readscratch
    OneWireWrite(0x00);

    for(char t = 0; t < 9; t++){
        databuf[t] = OneWireRead();
    }

    Battery_Temperature = databuf[2];
    Battery_Temperature <<= 8;
    Battery_Temperature |= databuf[1];
    Battery_Temperature /= 256;
    Battery_volt = databuf[4];
    Battery_volt <<= 8;
    Battery_volt |= databuf[3];
    Battery_amp = databuf[6];
    Battery_amp <<= 8;
    Battery_amp |= databuf[5];
    Battery_amp /= (int)(4.0f * Rsens);

#ifdef EUPRY_DEBUG
    sprintf(Debugbuf,"Bat: T:%d V:%d A: %d\n",Battery_Temperature,Battery_volt,Battery_amp);
    S232Arraysend(Debugbuf);
    //clrwdt();
#endif
}*/
