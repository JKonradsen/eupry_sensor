#ifndef SPI_H
#define	SPI_H

#include "main.h"

#ifdef SOFTSPI
#ifdef newSPI
    #define MOSI LATCbits.LATC0
    #define MISO PORTAbits.RA7
    #define CLK LATAbits.LATA6

    #define MOSITris TRISCbits.RC0
    #define MISOTris TRISAbits.RA7
    #define CLKTris TRISAbits.RA6
#else
    #define MOSI LATCbits.LATC0
    #define MISO PORTAbits.RA7
    #define CLK LATAbits.LATA5

    #define MOSITris TRISCbits.RC0
    #define MISOTris TRISAbits.RA7
    #define CLKTris TRISAbits.RA5
#endif
#endif
void SPIinit();
uint8_t SPISendByte(uint8_t input);
void SPISendByteDualMode(uint8_t input);

#endif

